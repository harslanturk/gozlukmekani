<ul class="x-navigation">
    <li class="xn-logo">
        <a href="/">Site Adı</a>
        <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
      @if(Auth::user()->image)
      <a href="#" class="profile-mini">
          <img src="{{Auth::user()->image}}" alt="John Doe"/>
      </a>
      @else
        <a href="#" class="profile-mini">
            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
        </a>
      @endif
        <div class="profile">
          @if(Auth::user()->image)
            <div class="profile-image">
                <img src="{{Auth::user()->image}}" alt="John Doe"/>
            </div>
            @else
              <div class="profile-image">
                  <img src="{{('/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
              </div>
          @endif
            <div class="profile-data">
                <div class="profile-data-name">{{Auth::user()->name}}</div>
                <div class="profile-data-title">Admin Bilgisi</div>
            </div>
            <div class="profile-controls">
                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
            </div>
        </div>
    </li>
    <!-- MENÜ BAŞLANGICI -->
    <li class="xn-title text-center">MENÜ</li>
    <?php
      $ust_menus = App\Helpers\helper::getMenuUst(Auth::user()->id);
      $alt_menus = App\Helpers\helper::getMenuAlt(Auth::user()->id);

    ?>

    @foreach($ust_menus as $menu)

    <?php
    if($menu->read == 1){
    if($menu->parent == 1){
      echo '<li class="xn-openable">
                  <a href="'.$menu->url.'">
                    <span class="fa '.$menu->icon.'"></span> <span class="xn-text">'.$menu->name.'</span>
                  </a>
                  <ul>';
                  foreach ($alt_menus as $key => $alt_menu) {
                    if ($alt_menu->read == 1 && $alt_menu->parent_id == $menu->id) {
                      echo '<li><a href="'.$alt_menu->url.'"><span class="fa '.$alt_menu->icon.'"></span> '.$alt_menu->name.'</a></li>';
                    }
                  }
                  echo '</ul>
                      </li>';
          }
    else{
      echo '<li>
                <a href="'.$menu->url.'"><span class="fa '.$menu->icon.'"></span> <span class="xn-text">'.$menu->name.'</span></a>
            </li>';
    }
    }
    ?>
    @endforeach
    </ul>
