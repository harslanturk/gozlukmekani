<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
<script type="text/javascript" src="{{URL::to('js/plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/jquery/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/bootstrap/bootstrap.min.js')}}"></script>
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src="{{URL::to('js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/dropzone/dropzone.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/fileinput/fileinput.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

<script type="text/javascript" src="{{URL::to('js/plugins/morris/raphael-min.js')}}"></script>
<!-- <script type="text/javascript" src="{{URL::to('js/plugins/morris/morris.min.js')}}"></script> -->
<script type="text/javascript" src="{{URL::to('js/plugins/rickshaw/d3.v3.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/rickshaw/rickshaw.min.js')}}"></script>
<script type='text/javascript' src="{{URL::to('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script type='text/javascript' src="{{URL::to('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script type='text/javascript' src="{{URL::to('js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/owl/owl.carousel.min.js')}}"></script>
<!-- <script type="text/javascript" src="{{URL::to('js/plugins/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/fullcalendar/fullcalendar.min.js')}}"></script> -->
<script type="text/javascript" src="{{URL::to('js/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/bootstrap/bootstrap-colorpicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
<!-- END THIS PAGE PLUGINS-->
<!-- fullCalendar 2.2.5 -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/lang-all.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type='text/javascript' src="{{URL::to('js/plugins/validationengine/languages/jquery.validationEngine-en.js')}}"></script>
<script type='text/javascript' src="{{URL::to('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

<script type='text/javascript' src="{{URL::to('js/plugins/jquery-validation/jquery.validate.js')}}"></script>
<!-- START TEMPLATE -->
<script type="text/javascript" src="{{URL::to('js/settings.js')}}"></script>

<script type="text/javascript" src="{{URL::to('js/plugins.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/actions.js')}}"></script>

<!-- <script type="text/javascript" src="{{URL::to('js/demo_dashboard.js')}}"></script> -->
<script type="text/javascript" src="{{URL::to('js/demo_tables.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/plugins/summernote/summernote.js')}}"></script>
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
