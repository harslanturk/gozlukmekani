<div id="home-section" class="slider1">
  <div class="tp-banner-container">
    <div class="tp-banner" >
      <ul>
        <!-- SLIDE  -->
        <?php $sliders = App\Helpers\helper::getSlider(); ?>
        @foreach($sliders as $slider)
        <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
          <!-- MAIN IMAGE -->
          <img src="{{$slider->image}}"  alt="slidebg1" data-lazyload="{{$slider->image}}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
          <!-- LAYERS -->

          <!-- LAYER NR. 1 -->
          <div class="tp-caption small_text lft tp-resizeme rs-parallaxlevel-0"
              data-x="0"
              data-y="245"
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="1200"
              data-easing="Power3.easeInOut"
              data-splitin="none"
              data-splitout="none"
              data-elementdelay="0.1"
              data-endelementdelay="0.1"
              style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap; font-size:24px; font-weight:500; color:#fff; background-color:#702376; padding:15px 25px;">{{$slider->text_1}} </div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption finewide_medium_white lfl tp-resizeme rs-parallaxlevel-0"
              data-x="0"
              data-y="315"
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="1800"
              data-easing="Power3.easeInOut"
              data-splitin="none"
              data-splitout="none"
              data-elementdelay="0.1"
              data-endelementdelay="0.1"
              style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap; font-size:50px; font-weight:700; color:#fff;">{{$slider->text_2}}</div>

          <!-- LAYER NR. 4 -->
          <div class="tp-caption small_text lfb tp-resizeme rs-parallaxlevel-0"
              data-x="0"
              data-y="470"
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="2400"
              data-easing="Power3.easeInOut"
              data-splitin="none"
              data-splitout="none"
              data-elementdelay="0.1"
              data-endelementdelay="0.1"
              style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap; color:#fff;">{{$slider->text_3}}</div>
        </li>
        @endforeach
      </ul>
      <div class="tp-bannertimer"></div>
    </div>
  </div>
</div>
