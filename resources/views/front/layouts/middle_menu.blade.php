
<div class="container">
  <div class="row">
    <div class="col-sm-4 col-md-3 col-xs-12">
      <!-- Header Logo -->
      <div class="logo"><a title="Gözlük Mekanı" href="/"><img alt="Gözlük Mekanı" src="/front/images/logo.png"></a> </div>
      <!-- End Header Logo -->
    </div>
    <div class="col-md-9 col-sm-8 hidden-xs">
      <div class="mtmegamenu">
        <ul>
          <li class="mt-root">
            <div class="mt-root-item"><a href="/tum-urunler">
              <div class="title title_font"><span class="title-text">Tüm Ürünler</span></div>
              </a></div>
          </li>
          <!--<li class="mt-root">
            <div class="mt-root-item"><a href="#">
              <div class="title title_font"><span class="title-text">Erkek</span></div>
              </a></div>
          </li>
          <li class="mt-root">
            <div class="mt-root-item"><a href="#">
              <div class="title title_font"><span class="title-text">Bayan</span> </div>
              </a></div>
          </li>-->
          <li class="mt-root">
            <div class="mt-root-item">
              <a href="/one-cikanlar">
                <div class="title title_font"><span class="title-text">Öne Çıkanlar</span></div>
              </a></div>
          </li>
          <li class="mt-root">
            <div class="mt-root-item">
              <a href="/cok-satanlar">
                <div class="title title_font"><span class="title-text">Çok Satanlar</span></div>
              </a>
            </div>
          </li>
          <li class="mt-root">
            <div class="mt-root-item">
              <a href="/indirim">
                <div class="title title_font"><span class="title-text">İndirim</span> </div>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
