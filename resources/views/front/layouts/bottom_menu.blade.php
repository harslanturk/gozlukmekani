<nav>
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-3">
        <div class="mm-toggle-wrap">
          <div class="mm-toggle"> <i class="fa fa-align-justify"></i> </div>
          <span class="mm-label hidden-xs">Categories</span> </div>
        <div class="mega-container visible-lg visible-md visible-sm">
          <div class="navleft-container">
            <div class="mega-menu-title">
              <h3>Kategoriler</h3>
            </div>
            <div class="mega-menu-category">
              <ul class="nav">
                <?php $brands = Helper::getBrandMenu(); ?>
                @foreach($brands as $key => $brand)
                <?php
                $img_1 = '';  
                $img_2 = '';
                $img_3 = '';
                if ($brand->image_1 != '') {
                  $image = explode(',',$brand->image_1);
                  $img_1 = $image[0];
                  $img_2 = $image[1];
                  $img_3 = $image[2];
                }
                $urun_1 = '';  
                $urun_2 = '';
                $urun_3 = '';
                $urun = '';
                if ($brand->menu_urun_id) {
                  $urun = explode(',',$brand->menu_urun_id);
                  $urun_1 = Helper::getImageCover($urun[1]);
                  $urun_2 = Helper::getImageCover($urun[2]);
                  $urun_3 = Helper::getImageCover($urun[3]);
                }
                   /* echo '<pre>';
                   print_r($img_1);
                   die();*/
                 ?>
                <li> <a href="#"> {{$brand->name}}</a>
                  <div class="wrap-popup">
                    <div class="popup">
                      <div class="row">
                      <div class="col-md-4 has-sep hidden-sm">
                        <div class="custom-menu-right">
                          <div class="box-banner media">
                            <div class="add-desc">
                              <h3>Computer <br>
                                Services </h3>
                              <div class="price-sale">2017</div>
                              <a href="#">Shop Now</a> </div>
                            <div class="add-right">
                              <a href="/one-cikanlar/<?php echo str_slug($brand->name); ?>"><img src="<?php echo ($img_1)? $img_1 :''; ?>" style="width:200px;height:175px;"></a>
                            </div>
                          </div>
                          <div class="box-banner media">
                            <div class="add-desc">
                              <h3>Save up to</h3>
                              <div class="price-sale">75 <sup>%</sup><sub>off</sub></div>
                              <a href="#">Shopping Now</a> </div>
                            <div class="add-right">
                              <a href="#"><img src="<?php echo ($urun_1)? $urun_1->image :''; ?>" style="width:200px;height:175px;"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 has-sep hidden-sm">
                        <div class="custom-menu-right">
                          <div class="box-banner media">
                            <div class="add-desc">
                              <h3>Computer <br>
                                Services </h3>
                              <div class="price-sale">2017</div>
                              <a href="#">Shop Now</a> </div>
                            <div class="add-right"><a href="/cok-satanlar/<?php echo str_slug($brand->name); ?>"><img src="<?php echo ($img_2)? $img_2 :''; ?>" style="width:200px;height:175px;"></a></div>
                          </div>
                          <div class="box-banner media">
                            <div class="add-desc">
                              <h3>Save up to</h3>
                              <div class="price-sale">75 <sup>%</sup><sub>off</sub></div>
                              <a href="#">Shopping Now</a> </div>
                              <a href="#"><img src="<?php echo ($urun_2)? $urun_2->image :''; ?>" style="width:200px;height:175px;"></a>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 has-sep hidden-sm">
                        <div class="custom-menu-right">
                          <div class="box-banner media">
                            <div class="add-desc">
                              <h3>Computer <br>
                                Services </h3>
                              <div class="price-sale">2017</div>
                              <a href="#">Shop Now</a> </div>
                            <div class="add-right"><a href="/<?php echo str_slug($brand->name); ?>"><img src="<?php echo ($img_3)? $img_3 :''; ?>"  style="width:200px;height:175px;"></a></div>
                          </div>
                          <div class="box-banner media">
                            <div class="add-desc">
                              <h3>Save up to</h3>
                              <div class="price-sale">75 <sup>%</sup><sub>off</sub></div>
                              <a href="#">Shopping Now</a> </div>
                              <a href="#"><img src="<?php echo ($urun_3)? $urun_3->image :''; ?>" style="width:200px;height:175px;"></a>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-9 col-sm-6 col-md-6 hidden-xs">
        <!-- Search -->

        <div class="top-search">
          <div id="search">
            <form method="post" action="/search">
              {{csrf_field()}}
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Marka, Ürün İsmi..." name="search">
                <select class="cate-dropdown hidden-xs hidden-sm" name="category_id">
                  <option>Tüm Kategoriler</option>
                  <option>Kadın</option>
                  <option>&nbsp;&nbsp;&nbsp;Yaz </option>
                  <option>&nbsp;&nbsp;&nbsp;İlkbahar</option>
                  <option>Erkek</option>
                  <option>Unisex</option>
                </select>
                <button class="btn-search" type="submit"><i class="fa fa-search"></i></button>
              </div>
            </form>
          </div>
        </div>

        <!-- End Search -->
      </div>
      <!-- top cart -->
      <div class="col-md-3 col-xs-9 col-sm-2 top-cart">
        <div class="top-cart-contain">
          <div class="mini-cart" id="mini-cart">
            <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#">
              <div class="cart-icon"><i class="fa fa-shopping-cart"></i></div>
              <div class="shoppingcart-inner hidden-xs hidden-sm"><span class="cart-title">Sepetim</span>
                <?php
                  if (!empty(Auth::user()->id)) {
                    $my_cart = App\Helpers\helper::myCart(Auth::user()->id);
                    $my_cart_count = $my_cart->count();
                    $total = 0;
                    foreach ($my_cart as $key => $value) {
                      $total += ($value->product->out_price * $value->quantity);
                    }
                  }else {
                    $my_cart = '';
                    $my_cart_count = 0;
                    $total = 0;
                  }
                 ?>
                <span class="cart-total">
                  @if($total != 0)
                    {{$my_cart_count.' Ürün '.$total}}<i class="fa fa-try"></i>
                  @else
                    Alışverişe Başla
                  @endif
                  </span></div>
              </a></div>
            <div>
              <div class="top-cart-content">
                <div class="block-subtitle hidden-xs">Sepetinizdeki Ürünler</div>
                <ul id="cart-sidebar" class="mini-products-list">
                  @if($my_cart)
                    @foreach($my_cart as $cart)
                    <?php
                      $cover = App\Helpers\helper::getImageCover($cart->product_id);
                     ?>
                    <li class="item odd">
                      <a href="/{{$cart->product->brand->slug}}/{{$cart->product->product_number}}/{{$cart->product_id}}" title="{{$cart->product->product_number}}" class="product-image">
                        <img src="{{$cover->image}}" alt="{{$cart->product->product_number}}" width="65">
                      </a>
                      <div class="product-details">
                        <a href="#" title="Ürünü Sil" onclick="sepetSil({{$cart->id}});" class="remove-cart"><i class="icon-close"></i></a>
                        <p class="product-name"><a href="/{{$cart->product->brand->slug}}/{{$cart->product->product_number}}/{{$cart->product_id}}">
                          {{$cart->product->product_number}}</a> </p>
                        <strong>{{$cart->quantity}}</strong> x <span class="price">{{$cart->product->out_price}}</span>
                      </div>
                    </li>
                    @endforeach
                  @endif
                </ul>
                @if($total)
                  <div class="top-subtotal">Toplam: <span class="price">{{$total}} <i class="fa fa-try"></i></span></div>
                  <div class="actions">
                    <form class="" action="/myaccount/cart" method="get">
                      <button class="btn-checkout" type="submit"><i class="fa fa-check"></i><span>Satın Al</span></button>
                      <button class="view-cart" type="submit"><i class="fa fa-shopping-cart"></i> <span>Sepete Git</span></button>
                    </form>
                  </div>
                @else
                  <div class="top-subtotal" style="text-align:center;color:purple;">Sepetinizde Ürün Yok</span></div>
                  <div class="actions" style="text-align:center;">
                    <a class="btn-checkout" href="/tum-urunler"><i class="fa fa-shopping-cart"></i><span>Alışverişe Başla</span></a>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>
