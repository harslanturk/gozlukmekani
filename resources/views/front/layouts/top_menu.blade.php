<div class="header-top">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-4 hidden-xs">
        <!-- Default Welcome Message -->
        @if(Auth::user())
          <div class="welcome-msg "> <strong>Merhaba; {{Auth::user()->name.' '.Auth::user()->surname}}</strong> </div>
        @else
          <div class="welcome-msg "> <strong>Yeni Trend Gözlükler</strong> </div>
        @endif
      </div>
      <!-- top links -->
      <div class="headerlinkmenu col-lg-8 col-md-7 col-sm-8 col-xs-12">
        <div class="links">
          @if(Auth::user())
              @if(Auth::user()->admin == 1)
              <div class="myaccount">
                <a title="Hesabım" href="/admin"><i class="fa fa-user"></i>
                  <span class="hidden-xs">Panele Git</span>
                </a>
              </div>
              @else
              <div class="myaccount">
                <a title="Hesabım" href="/myaccount"><i class="fa fa-user"></i>
                  <span class="hidden-xs">Hesabım</span>
                </a>
              </div>
              <div class="wishlist"><a title="Beğendiklerim" href="/myaccount/wishlist"><i class="fa fa-heart"></i>
                <span class="hidden-xs">Beğendiklerim</span></a>
              </div>
              @endif
          @endif
          @if(!Auth::user())
          <div class="login">
            <a href="/login"><i class="fa fa-unlock-alt"></i>
              <strong> Giriş</strong>
            </a>
          </div>
          @else
          <div class="login">
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                {{ csrf_field() }}
            </form>
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <span class="hidden-xs">Çıkış</span>
                <i class="fa fa-unlock-alt"></i>
            </a>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
