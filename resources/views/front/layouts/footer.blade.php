<footer>
  <!-- our clients Slider -->
  <div class="our-clients">
    <div class="container">
      <div class="col-md-12">
        <div class="slider-items-products">
          <div id="our-clients-slider" class="product-flexslider hidden-buttons">
            <div class="slider-items slider-width-col6">
              <?php $brands = App\Helpers\helper::getBrand();  ?>
              @foreach($brands as $brand)
                @if($brand->image)
              <!-- Item -->
                <div class="item"> <a href="#"><img src="{{$brand->image}}" alt="Image"></a> </div>
              <!-- End Item -->
                @endif
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
        <div class="footer-links">
          <h3 class="links-title">Kurumsal<a class="expander visible-xs" href="#TabBlock-1">+</a></h3>
          <div class="tabBlock" id="TabBlock-1">
            <ul class="list-links list-unstyled">
              <li><a href="/hakkimizda">Hakkımızda</a></li>
              <li><a href="/iletisim">İletişim</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
        <div class="footer-links">
          <h3 class="links-title">Ürünler<a class="expander visible-xs" href="#TabBlock-3">+</a></h3>
          <div class="tabBlock" id="TabBlock-3">
            <ul class="list-links list-unstyled">
              <li> <a href="#">Erkek </a> </li>
              <li> <a href="#">Bayan</a> </li>
              <li> <a href="/one-cikanlar">Öne Çıkanlar</a> </li>
              <li> <a href="/cok-satanlar">Çok Satanlar</a> </li>
              <li> <a href="/indirim">İndirim</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-2 col-xs-12 col-lg-2 collapsed-block">
        <div class="footer-links">
          <h3 class="links-title">Müşteriler<a class="expander visible-xs" href="#TabBlock-4">+</a></h3>
          <div class="tabBlock" id="TabBlock-4">
            <ul class="list-links list-unstyled">
              <li> <a href="/login">Üye Ol</a> </li>
              <li> <a href="/login">Giriş</a> </li>
              <li> <a href="/iptal-iade-degisim">İptal,İade,Değişim</a> </li>
              <li> <a href="/uzak-mesafeli-satis-sozlesmesi">Uzak Mesafeli Satış Sözleşmesi</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">
        <h3 class="links-title">İletişim</h3>
        <div class="footer-content">
          <div class="email"> <i class="fa fa-envelope"></i>
            <p>info@gozlukmekani.com</p>
          </div>
          <div class="address"> <i class="fa fa-map-marker"></i>
          <p>Dr. Sadık Ahmet Cad.</p>
          <p>Deniz Köşler Mah.</p><i class="fa fa-map-marker"></i>
          <p>Gülden Sok No:3/8</p>
          <p>Avcılar / İSTANBUL</p>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-12 col-xs-12 col-lg-3">
        <div class="footer-links">
          <div class="footer-newsletter">
            <h3 class="links-title">Yeniliklerden Haberdar Ol</h3>
            <form id="newsletter-validate-detail" method="post" action="#">
              <div class="newsletter-inner">
                <input class="newsletter-email" name='Email' placeholder='Email Adresinizi Giriniz'/>
                <button class="button subscribe" type="submit" title="Subscribe">Subscribe</button>
              </div>
            </form>
          </div>
          <div class="social">
            <ul class="inline-mode">
              <li class="social-network fb">
                <a title="Connect us on Facebook" target="_blank" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
              </li>
              <li class="social-network googleplus">
                <a title="Connect us on Google+" target="_blank" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
              </li>
              <li class="social-network tw">
                <a title="Connect us on Twitter" target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
              </li>
              <li class="social-network rss">
                <a title="Connect us on Instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-instagram"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-coppyright">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-xs-12 coppyright"> Copyright © 2018 <a href="http://cozumlazim.com/" target="_blank"> ÇözümLazım </a>. Tüm Hakları Saklıdır. </div>
        <div class="col-sm-6 col-xs-12">
          <div class="payment">
            <ul>
              <li><a href="#"><img title="Visa" alt="Visa" src="/front/images/visa.png"></a></li>
              <li><a href="#"><img title="Master Card" alt="Master Card" src="/front/images/master-card.png"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<a href="#" class="totop" title="Yukarı Git."><i class="fa fa-arrow-up"></i></a>
<!-- End Footer -->
