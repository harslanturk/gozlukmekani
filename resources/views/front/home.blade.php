@extends('front.master')
@section('slider')
<!-- Home Slider Start -->
@include('front.layouts.slider')
<!-- End home section -->
@endsection
@section('content')
<div class="top-banner">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="jtv-banner1"><a href="/indirim"><img src="/front/images/banner1.jpg" alt=""></a>
          <div class="hover_content" style="background-color: rgba(139, 158, 252, 0.8);">
            <div class="hover_data">
              <div class="title"> Yaz Sezonu</div>
              <div class="desc-text"> Başladı </div>
              <div class="shop-now"><a href="/tum-urunler">Alışverişe Başla</a></div>
            </div>
          </div>
        </div>
        <div class="jtv-banner2"><a href="/indirim"><img src="/front/images/banner2.jpg" alt=""></a>
          <div class="hover_content">
            <div class="hover_data">
              <div class="title"> Açılışımıza Özel</div>
              <div class="desc-text"> %20'ye Varan İndirim </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="jtv-banner3">
          <div class="jtv-banner3-inner"><a href="/tom-davies"><img src="/front/images/banner3.jpg" alt=""></a>
            <div class="hover_content">
              <div class="hover_data">
                <div class="title"> yeni trend </div>
                <div class="desc-text"> Tom Davies Gözlükler</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- main container -->
<div class="main-container col1-layout">
  <div class="container">
    <div class="row">

      <!--Hot deal -->

      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="jtv-banner-top">
          <div class="jtv-banner-box">
            <div class=""> <a class="jtv-banner-inner-text" href="#">
              <div class="jtv-banner-box-image"> <img src="/front/images/image6.jpg" alt="Gözlük Mekanı"> </div>
              </a> </div>
          </div>
          <!-- End jtv-banner-box -->
        </div>
      </div>
      <!-- Home Tabs  -->
      <div class="col-sm-8 col-md-9 col-xs-12">
        <div class="home-tab">
          <ul class="nav home-nav-tabs home-product-tabs">
            <li class="active"><a href="#featured" data-toggle="tab" aria-expanded="false">Öne Çıkan Ürünler</a></li>
            <li> <a href="#top-sellers" data-toggle="tab" aria-expanded="false">Çok Satanlar</a> </li>
          </ul>
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="featured">
              <div class="featured-pro">
                <div class="slider-items-products">
                  <div id="featured-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                      @foreach($one_cikanlars as $one_cikanlar)
                      <div class="product-item">
                        <div class="item-inner">
                          <div class="product-thumb has-hover-img">
                            @if($one_cikanlar->discount > 0)
                              <div class="icon-sale-label sale-left">İndirim</div>
                            @endif
                            @if($one_cikanlar->new)
                              <div class="icon-new-label new-right"><span>Yeni</span></div>
                            @endif
                            <?php $kapak = App\Helpers\helper::getImageCover($one_cikanlar->id); ?>
                            <figure>
                              <a href="/{{$one_cikanlar->brand->slug}}/{{$one_cikanlar->product_number}}/{{$one_cikanlar->id}}"><img src="{{$kapak->image}}" alt=""></a>
                              <a class="hover-img" href="/{{$one_cikanlar->brand->slug}}/{{$one_cikanlar->product_number}}/{{$one_cikanlar->id}}"><img src="{{$kapak->image}}" alt=""></a>
                            </figure>
                            <div class="pr-info-area animated animate4">
                              <a href="#" class="quick-view" id="{{$one_cikanlar->id}}" data-toggle="modal" data-target="#Popup"><i class="fa fa-search"><span>Hızlı Görüntüle</span></i></a>
                              <a class="wishlist add-wishlist" id="{{$one_cikanlar->id}}" style="cursor:pointer;"><i class="fa fa-heart"><span>Beğen</span></i></a>
                            </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <a title="Rayban Gözlük" href="/{{$one_cikanlar->brand->slug}}/{{$one_cikanlar->product_number}}/{{$one_cikanlar->id}}">{{$one_cikanlar->brand->name}} </a>
                              </div>
                              <div class="item-title">
                                <a style="color:#777272;" href="/{{$one_cikanlar->slug}}/{{$one_cikanlar->product_number}}/{{$one_cikanlar->id}}">{{$one_cikanlar->product_number}}</a>
                              </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price"> <span class="price">{{$one_cikanlar->out_price}}</span> <i class="fa fa-try"></i> </span>
                                  </div>
                                </div>
                                <div class="pro-action">
                                  @if(Auth::user())
                                  <button type="button" class="add-to-cart-mt" onclick="sepeteEkle({{$one_cikanlar->id}});"> <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span> </button>
                                  @else
                                  <a href="/login" class="add-to-cart-mt" style="display:inline-block;">
                                    <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span>
                                  </a>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="top-sellers">
              <div class="top-sellers-pro">
                <div class="slider-items-products">
                  <div id="top-sellers-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                      @foreach($cok_satanlars as $cok_satanlar)
                      <div class="product-item">
                        <div class="item-inner">
                          <div class="product-thumb has-hover-img">
                            @if($cok_satanlar->discount > 0)
                              <div class="icon-sale-label sale-left">İndirim</div>
                            @endif
                            @if($cok_satanlar->new)
                              <div class="icon-new-label new-right"><span>Yeni</span></div>
                            @endif
                            <?php $kapak = App\Helpers\helper::getImageCover($cok_satanlar->id); ?>
                            <figure>
                              <a href="/{{$cok_satanlar->brand->slug}}/{{$cok_satanlar->product_number}}/{{$cok_satanlar->id}}"><img src="{{$kapak->image}}" alt=""></a>
                              <a class="hover-img" href="/{{$cok_satanlar->brand->slug}}/{{$cok_satanlar->product_number}}/{{$cok_satanlar->id}}"><img src="{{$kapak->image}}" alt=""></a>
                            </figure>
                            <div class="pr-info-area animated animate4">
                              <a href="#" class="quick-view" id="{{$cok_satanlar->id}}" data-toggle="modal" data-target="#Popup"><i class="fa fa-search"><span>Hızlı Görüntüle</span></i></a>
                              <a class="wishlist add-wishlist" id="{{$cok_satanlar->id}}" style="cursor:pointer;"><i class="fa fa-heart"><span>Beğen</span></i></a>
                            </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <a title="{{$cok_satanlar->brand->name}} Gözlük" href="/{{$cok_satanlar->brand->slug}}/{{$cok_satanlar->product_number}}/{{$cok_satanlar->id}}">{{$cok_satanlar->brand->name}} </a>
                              </div>
                              <div class="item-title">
                                <a style="color:#777272;" href="/{{$cok_satanlar->slug}}/{{$cok_satanlar->product_number}}/{{$cok_satanlar->id}}">{{$cok_satanlar->product_number}}</a>
                              </div>
                              <div class="item-content">
                                <div class="rating">
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star-o"></i>
                                  <i class="fa fa-star-o"></i>
                                  <i class="fa fa-star-o"></i>
                                </div>
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price"> <span class="price">{{$cok_satanlar->out_price}}</span> <i class="fa fa-try"></i> </span>
                                  </div>
                                </div>
                                <div class="pro-action">
                                  @if(Auth::user())
                                  <button type="button" class="add-to-cart-mt" onclick="sepeteEkle({{$cok_satanlar->id}});"> <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span> </button>
                                  @else
                                  <a href="/login" class="add-to-cart-mt" style="display:inline-block;">
                                    <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span>
                                  </a>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end main container -->
<!-- <div class="container">
  <div class="block-static2-inner">
    <div class="img"><a href="#"><img class="alignnone size-full" src="/front/images/banner6.jpg" alt="banner6"></a></div>
    <div class="content">
      <h3>büyük 2018 indirimi</h3>
      <h2>45% indirim</h2>
      <p>Tüm Ürünlerde İndirim Fırsatı. </p>
    </div>
    <div class="trending">
      <div class="trending-inner">
        <h3>Creative</h3>
        <h2>DEV İNDİRİM</h2>
        <p>Yeni Ürünler </p>
      </div>
    </div>
  </div>
</div>-->

<!--special-products-->

<!--<div class="container">
  <div class="special-products">
    <div class="page-header">
      <h2>özel ürünler</h2>
    </div>
    <div class="special-products-pro">
      <div class="slider-items-products">
        <div id="special-products-slider" class="product-flexslider hidden-buttons">
          <div class="slider-items slider-width-col4">
            @foreach($ozel_urunlers as $ozel_urunler)
            <div class="product-item">
              <div class="item-inner">
                <div class="product-thumb has-hover-img">
                  @if($ozel_urunler->discount > 0)
                    <div class="icon-sale-label sale-left">İndirim</div>
                  @endif
                  @if($ozel_urunler->new)
                    <div class="icon-new-label new-right"><span>Yeni</span></div>
                  @endif
                  <?php $kapak = App\Helpers\helper::getImageCover($ozel_urunler->id); ?>
                  <figure>
                    <img src="{{$kapak->image}}" alt="">
                    <img class="hover-img" src="{{$kapak->image}}" alt="">
                  </figure>
                  </a>
                  <div class="pr-info-area animated animate4">
                    <a href="#" class="quick-view" id="{{$ozel_urunler->id}}" data-toggle="modal" data-target="#Popup"><i class="fa fa-search"><span>Hızlı Görüntüle</span></i></a>
                    <a class="wishlist add-wishlist" id="{{$ozel_urunler->id}}" style="cursor:pointer;"><i class="fa fa-heart"><span>Wishlist</span></i></a>
                   </div>
                </div>
                <div class="item-info">
                  <div class="info-inner">
                    <div class="item-title">
                      <a title="{{$ozel_urunler->brand->name}} Gözlük" href="/{{$ozel_urunler->brand->slug}}/{{$ozel_urunler->product_number}}/{{$ozel_urunler->id}}">{{$ozel_urunler->brand->name.' '.$ozel_urunler->product_number}} </a>
                    </div>
                    <div class="item-content">
                      <div class="rating">
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                      </div>
                      <div class="item-price">
                        <div class="price-box">
                          <span class="regular-price"> <span class="price">{{$ozel_urunler->out_price}}</span>
                          <i class="fa fa-try"></i> </span>
                        </div>
                      </div>
                      <div class="pro-action">
                        @if(Auth::user())
                        <button type="button" class="add-to-cart-mt" onclick="sepeteEkle({{$ozel_urunler->id}});"> <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span> </button>
                        @else
                        <a href="/login" class="add-to-cart-mt" style="display:inline-block;">
                          <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span>
                        </a>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>-->

<!-- category area start -->
<div class="jtv-category-area">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-12">
        <div class="jtv-single-cat">
          <div class="page-header">
            <h2>Haftanın Çok Satanları</h2>
          </div>
          @foreach($haftanin_urunlers as $key => $haftanin_urunler)
          <div class="jtv-product <?php echo ($key>1) ? 'jtv-cat-margin':'' ?> ">
            <div class="product-img">
              <?php $kapak = App\Helpers\helper::getImageCover($haftanin_urunler->id); ?>
              <a href="/{{$haftanin_urunler->brand->slug}}/{{$haftanin_urunler->product_number}}/{{$haftanin_urunler->id}}">
                <img src="{{$kapak->image}}" alt="">
                <img class="secondary-img" src="{{$kapak->image}}" alt="">
              </a>
            </div>
            <div class="jtv-product-content">
              <h3><a href="/{{$haftanin_urunler->brand->slug}}/{{$haftanin_urunler->product_number}}/{{$haftanin_urunler->id}}">{{$haftanin_urunler->brand->name}}</a>
              <div class="item-title">
                <a style="color:#777272;" href="/{{$haftanin_urunler->slug}}/{{$haftanin_urunler->product_number}}/{{$haftanin_urunler->id}}">{{$haftanin_urunler->product_number}}</a>
              </div>
              </h3>
              <div class="price-box"> <span class="regular-price">
                <span class="price">{{$haftanin_urunler->out_price}} <i class="fa fa-try"></i> </span> </span>
              </div>
              <div class="jtv-product-action">
                <div class="jtv-extra-link">
                  <div class="button-cart">
                    @if(Auth::user())
                    <button type="button" onclick="sepeteEkle({{$haftanin_urunler->id}});"> <i class="fa fa-shopping-cart"></i></button>
                    @else
                    <a href="/login" style="display:inline-block;">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                    @endif
                  </div>
                    <a href="#" class="quick-view" id="{{$haftanin_urunler->id}}" data-toggle="modal" data-target="#Popup"><i class="fa fa-search"></i></a>
                    <a class="wishlist add-wishlist" id="{{$haftanin_urunler->id}}" style="cursor:pointer;"><i class="fa fa-heart"></i></a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>

      <!-- banner -->
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="jtv-banner">
          <div class="upper">
            <div class="jtv-subbanner1"><a href="/gyesse"><img class="img-respo" alt="jtv-subbanner1" src="/front/images/banner4.jpg"></a>
              <div class="text-block">
                <div class="text1"><a href="/gyesse"> Gyesse</a></div>
              </div>
            </div>
            <div class="jtv-subbanner2"><a href="/tom-davies"><img class="img-respo" alt="jtv-subbanner2" src="/front/images/banner5.jpg"></a>
              <div class="text-block">
                <div class="text1"><a href="/tom-davies">Tom Davies</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- category-area end -->

<!-- service section -->

<div class="jtv-service-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-truck flip-horizontal"></i> </div>
          <div class="service-content">
            <h5>Ücretsiz Kargo </h5>
            <p>100.00 <i class="fa fa-try"></i> üzeri siparişlerinize</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-mail-forward"></i> </div>
          <div class="service-content">
            <h5>Garanti </h5>
            <p>Tün Ürünlerde 2 Yıl Garanti</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-comments flip-horizontal"></i> </div>
          <div class="service-content">
            <h5>24/7 Müşteri Desteği </h5>
            <p>Günde 24 Saat Destek</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- MODALS -->
<!-- <div class="modal" id="quick_view" tabindex="-1" role="dialog" aria-hidden="true">

</div> -->

<div class="modal fade" id="Popup" role="dialog" aria-labelledby="myModalLabel">

</div>
@endsection
@section('jscode')

<!-- Slider Js -->
<script type="text/javascript" src="/front/js/revolution-slider.js"></script>

<!-- bxslider js -->
<script type="text/javascript" src="/front/js/jquery.bxslider.js"></script>

<script type="text/javascript">
function sepeteEkle(product_id) {
  //console.log(product_id);
  $.ajax({
      url: '/sepete-ekle',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      data: {product_id: product_id},
      success: function(data){
        sepetYenile();
        //$.notify("Ürün Sepete Eklendi.",'success');
        alertify.notify('Ürün Sepete Eklendi.', 'success', 5, function(){  console.log('ok'); });
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetYenile() {
  $.ajax({
      url: '/sepet-yenile',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      success: function(data){
        $('#mini-cart').empty();
        document.getElementById('mini-cart').innerHTML = data;
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetSil(id) {
    console.log(id);
    $.ajax({
        url: '/sepet-urun-sil',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id: id},
        success: function(data){
          sepetYenile();
          //$.notify("Ürün Sepetden Silindi.",'error');
          alertify.notify('Ürün Sepetden Silindi', 'error', 5, function(){  console.log('ok'); });
        },
        error: function(jqXHR, textStatus, err){}
    });
  }

$('.quick-view').click(function(){
  var id = $(this).attr('id');
      $.ajax({
          url: '/quick_view/'+id,
          type: 'POST',
          beforeSend: function (xhr) {
              var token = $('meta[name="csrf_token"]').attr('content');

              if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
              }
          },
          cache: false,
          data: {id:id},
          success: function(data){
              document.getElementById('Popup').innerHTML = data;
          },
          error: function(jqXHR, textStatus, err){}
      });
    });

$('.add-wishlist').click(function () {
    var product_id = $(this).attr('id');
    console.log(product_id);
  $.ajax({
      url: '/product/my-wishlist/add',
      type: 'POST',
      beforeSend: function (xhr) {
        var token = $('meta[name="csrf_token"]').attr('content');

        if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      cache: false,
      data: {product_id: product_id},
      success: function(data){

        if (data == 1) {
          $.notify("Ürün Beğeni Listenize Eklendi.",'success');
        }else if(data == 2) {
          $.notify("Ürün Beğeni Listenizden Silindi.",'error');
        }
      },
      error: function(jqXHR, textStatus, err){}
  });
});

jQuery(document).ready(function() {

	jQuery('.tp-banner').show().revolution(
	{
		dottedOverlay:"none",
		delay:10000,
		startwidth:960,
		startheight:715,
		hideThumbs:200,

		thumbWidth:100,
		thumbHeight:50,
		thumbAmount:5,

		navigationType:"bullet",
		navigationArrows:"on",

		touchenabled:"on",
		onHoverStop:"off",

		swipe_velocity: 0.7,
		swipe_min_touches: 1,
		swipe_max_touches: 1,
		drag_block_vertical: false,

								parallax:"mouse",
		parallaxBgFreeze:"on",
		parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

		keyboardNavigation:"off",

		navigationHAlign:"center",
		navigationVAlign:"bottom",
		navigationHOffset:0,
		navigationVOffset:60,

		shadow:0,

		spinner:"spinner4",

		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,

		shuffle:"off",

		autoHeight:"off",
		forceFullWidth:"off",



		hideThumbsOnMobile:"off",
		hideNavDelayOnMobile:1500,
		hideBulletsOnMobile:"off",
		hideArrowsOnMobile:"off",
		hideThumbsUnderResolution:0,

		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		startWithSlide:0,
		fullScreenOffsetContainer: ".header"
	});

});	//ready
	</script>
@endsection
