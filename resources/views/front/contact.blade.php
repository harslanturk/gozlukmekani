@extends('front.master')
@section('content')
   <!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="/">Anasayfa</a><span>&raquo;</span></li>

          <li><strong>İletişim</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
    <div class="row">
      <section class="col-main col-sm-12">
        <div id="contact" class="page-content page-contact">
          <div class="page-title">
            <h2>İletişim</h2>
          </div>
          <div class="row">
            <div class="col-sm-8">
              <h3 class="page-subheading"><strong style="color:red;font-size:20px;">*</strong> İşaretli Alanları Doldurunuz</h3>
              <div class="contact-form-box">
                <div class="form-selector">
                  <label>İsim <span>*</span></label>
                  <input type="text" class="form-control input-sm" placeholder="İsim " id="name" />
                </div>
                <div class="form-selector">
                  <label>Soyisim</label>
                  <input type="text" class="form-control input-sm" placeholder="Soyisim" id="name" />
                </div>
                <div class="form-selector">
                  <label>E-mail Adres <span>*</span></label>
                  <input type="text" class="form-control input-sm" placeholder="E-mail Adresiniz" id="email" />
                </div>
                <div class="form-selector">
                  <label>Mesajınız <span>*</span></label>
                  <textarea class="form-control input-sm" rows="20" placeholder="Mesajınız" id="message"></textarea>
                </div>
                <div class="form-selector">
                  <button class="button"><i class="fa fa-send"></i>&nbsp; <span>Mesajı Gönder</span></button>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4" id="contact_form_map">
              <h3 class="page-subheading">İletişim Bilgilerimiz</h3>
              <div class="article-container style-1">
                <h5>Şirket Adresi</h5>
                Dr. Sadık Ahmet Cad.<br>
                Deniz Köşler Mah.<br>
                Gülden Sok No:3/8<br>
                Avcılar / İSTANBUL<br><br>
                <h5>Telefon & E-mail</h5>
                <p>Email: <a href="mailto:destek@gozlukmekani.com">destek@gozlukmekani.com</a><br>
                  Telefon: 0531 214 60 71</p>
              </div>
              <br/>
              <div class="share-box">
                <div class="title">Sosyal Medya </div>
                <div class="socials-box">
                  <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                  <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                  <a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                  <a href="#" title="Youtube"><i class="fa fa-youtube"></i></a>
                  <a href="#" title="Facebook"><i class="fa fa-instagram"></i></a> </div>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</section>
<!-- Main Container End -->
<!-- service section -->

<div class="jtv-service-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-truck flip-horizontal"></i> </div>
          <div class="service-content">
            <h5>Ücretsiz Kargo </h5>
            <p>100.00 <i class="fa fa-try"></i> üzeri siparişlerinize</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-mail-forward"></i> </div>
          <div class="service-content">
            <h5>Garanti </h5>
            <p>Tün Ürünlerde 2 Yıl Garanti</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-comments flip-horizontal"></i> </div>
          <div class="service-content">
            <h5>24/7 Müşteri Desteği </h5>
            <p>Günde 24 Saat Destek</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer -->
@endsection
