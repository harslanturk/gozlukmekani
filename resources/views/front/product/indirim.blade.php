@extends('front.master')
@section('content')
<!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">

        <ul>
          <li class="home"> <a title="Go to Home Page" href="/">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>İndirim</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<div class="main-container col2-left-layout">
  <div class="container">
    <div class="row">
      <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">
        <div class="shop-inner">
          <div class="toolbar">
            <div class="view-mode">
              <ul>
                <li class="active"> <a href="#"> <i class="fa fa-th"></i> </a> </li>
              </ul>
            </div>
            <div class="sorter">
              <div class="short-by">
                <label>Sıralama:</label>
                <select id="list_value">
                  @if(!empty($list))
                  <option value="name" <?php echo ($list=='name') ? 'selected' : ''  ?>>İsme</option>
                  <option value="max_min" <?php echo ($list=='max_min') ? 'selected' : ''  ?>>Pahalıdan Ucuza</option>
                  <option value="min_max" <?php echo ($list=='min_max') ? 'selected' : ''  ?>>Ucuzdan Pahalıya</option>
                  <option value="new" <?php echo ($list=='new') ? 'selected' : ''  ?>>En Yeniler</option>
                  @else
                  <option value="name">İsme</option>
                  <option value="max_min">Pahalıdan Ucuza</option>
                  <option value="min_max">Ucuzdan Pahalıya</option>
                  <option value="new">En Yeniler</option>
                  @endif
                </select>
              </div>
            </div>
          </div>
          <div class="product-grid-area">
            <ul class="products-grid">
              @foreach($cat_products as $cat_product)
              <?php
                if ($cat_product->modek_id != 0) {
                  $model_name = $cat_product->modek_name;
                }else {
                  $model_name = '';
                }
               ?>
              <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">
                <div class="product-item">
                  <div class="item-inner">
                    <div class="product-thumb has-hover-img">
                      @if($cat_product->discount > 0)
                        <div class="icon-sale-label sale-left">İndirim</div>
                      @endif
                      @if($cat_product->new)
                        <div class="icon-new-label new-right"><span>Yeni</span></div>
                      @endif
                      <?php $kapak = App\Helpers\helper::getImageCover($cat_product->id); ?>
                      <figure>
                        <a href="/{{$cat_product->slug.'/'.$cat_product->product_number}}/{{$cat_product->id}}"><img src="{{$kapak->image}}" alt=""></a>
                        <a class="hover-img" href="/{{$cat_product->slug.'/'.$cat_product->product_number}}/{{$cat_product->id}}"><img src="{{$kapak->image}}" alt=""></a>
                      </figure>
                      <div class="pr-info-area animated animate4">
                        <a href="#" class="quick-view" id="{{$cat_product->id}}" data-toggle="modal" data-target="#Popup"><i class="fa fa-search"></i></a>
                        <a class="wishlist add-wishlist" id="{{$cat_product->id}}" style="cursor:pointer;"><i class="fa fa-heart"></i></a>
                      </div>
                    </div>
                    <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title">
                          <a href="/{{$cat_product->slug}}/{{$cat_product->product_number}}/{{$cat_product->id}}">{{$cat_product->brand_name}}</a>
                        </div>
                          <div class="item-title">
                            <a style="color:#777272;" href="/{{$cat_product->slug}}/{{$cat_product->product_number}}/{{$cat_product->id}}">{{$cat_product->product_number}}</a>
                          </div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="item-price">
                            <div class="price-box"> <span class="regular-price"> <span class="price">{{$cat_product->out_price}}</span> <i class="fa fa-try"></i> </span>
                            </div>
                          </div>
                          <div class="pro-action">
                            @if(Auth::user())
                            <button type="button" class="add-to-cart-mt" onclick="sepeteEkle({{$cat_product->id}});"> <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span> </button>
                            @else
                            <a href="/login" class="add-to-cart-mt" style="display:inline-block;">
                              <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span>
                            </a>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
          </div>
          <div style="display: inline-block;width: 100%;text-align: center;">
          {{$cat_products->links()}}
          </div>
        </div>
      </div>
      <!-- Sol Filtreleme Menüsü -->
      <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">
        <div class="block category-sidebar">
          <div class="sidebar-title">
            <h3>Categories</h3>
          </div>
          <ul class="product-categories">
            <li class="cat-item current-cat cat-parent"><a href= "shop_grid.html">Women</a>
              <ul class="children">
                <li class="cat-item cat-parent"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Accessories</a>
                  <ul class="children">
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Dresses</a></li>
                    <li class="cat-item cat-parent"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Handbags</a>
                      <ul style="display: none;" class="children">
                        <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Beaded Handbags</a></li>
                        <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Sling bag</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li class="cat-item cat-parent"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Handbags</a>
                  <ul class="children">
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; backpack</a></li>
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Beaded Handbags</a></li>
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Fabric Handbags</a></li>
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Sling bag</a></li>
                  </ul>
                </li>
                <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Jewellery</a> </li>
                <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Swimwear</a> </li>
              </ul>
            </li>
            <li class="cat-item cat-parent"><a href="shop_grid.html">Men</a>
              <ul class="children">
                <li class="cat-item cat-parent"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Dresses</a>
                  <ul class="children">
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Casual</a></li>
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Designer</a></li>
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Evening</a></li>
                    <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Hoodies</a></li>
                  </ul>
                </li>
                <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Jackets</a> </li>
                <li class="cat-item"><a href="shop_grid.html"><i class="fa fa-angle-right"></i>&nbsp; Shoes</a> </li>
              </ul>
            </li>
            <li class="cat-item"><a href="shop_grid.html">Electronics</a></li>
            <li class="cat-item"><a href="shop_grid.html">Furniture</a></li>
            <li class="cat-item"><a href="shop_grid.html">KItchen</a></li>
          </ul>
        </div>
        <div class="block shop-by-side">
          <div class="sidebar-bar-title">
            <h3>Filtre</h3>
          </div>
          <div class="block-content">
            <div class="layered-Category">
              <h2 class="saider-bar-title">Markalar</h2>
              <div class="layered-content">
                <ul class="check-box-list">
                  @foreach($brands as $key => $brand)
                  <li>
                    <input type="checkbox" id="jtv{{$key}}" name="jtvc" class="jtvc" value="{{$brand->slug}}">
                    <label for="jtv{{$key}}"> <span class="button"></span>  {{$brand->name}}<span class="count">({{$brand->brand_count}})</span> </label>
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="single-img-add sidebar-add-slider ">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner" role="listbox">
              <div class="item active"> <img src="/front/images/add-slide1.jpg" alt="slide1">
                <div class="carousel-caption">
                  <h3><a href="single_product.html" title=" Sample Product">Sale Up to 50% off</a></h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <a href="#" class="info">shopping Now</a> </div>
              </div>
              <div class="item"> <img src="/front/images/add-slide1.jpg" alt="slide2">
                <div class="carousel-caption">
                  <h3><a href="single_product.html" title=" Sample Product">Smartwatch Collection</a></h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <a href="#" class="info">All Collection</a> </div>
              </div>
              <div class="item"> <img src="/front/images/add-slide1.jpg" alt="slide3">
                <div class="carousel-caption">
                  <h3><a href="single_product.html" title=" Sample Product">Summer Sale</a></h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
              </div>
            </div>

            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
        </div>-->
        <div class="block special-product">
          <div class="sidebar-bar-title">
            <h3>Özel Ürünler</h3>
          </div>
          <div class="block-content">
            <ul>
              @foreach($ozel_urunlers as $ozel_urunler)
              <li class="item">
                <div class="products-block-left">
                  <a href="/{{$ozel_urunler->brand->slug.'/'.$ozel_urunler->product_number}}/{{$ozel_urunler->id}}" title="Sample Product" class="product-image">
                    <?php $kapak = App\Helpers\helper::getImageCover($ozel_urunler->id); ?>
                    <img src="{{$kapak->image}}" alt="{{$ozel_urunler->product_number}}">
                  </a>
                </div>
                <div class="products-block-right">
                  <p class="product-name">
                    <a href="/{{$ozel_urunler->brand->slug.'/'.$ozel_urunler->product_number}}/{{$ozel_urunler->id}}">{{$ozel_urunler->brand->name}}</a>
                  </p>
                  <span>{{$ozel_urunler->product_number}}</span>
                  <span class="price">{{$ozel_urunler->out_price}}  <i class="fa fa-try"></i> </span>
                  <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o"></i>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
        </div>
      </aside>
    </div>
  </div>
</div>
@if(isset($data))
<input type="text" id="search_data" value="{{$data}}">
@endif
<!-- Main Container End -->
<div class="modal fade" id="Popup" role="dialog" aria-labelledby="myModalLabel">

</div>
@endsection
@section('jscode')
<script type="text/javascript">
  $('.pagination>li.active>span').attr('style','background-color:#702376;border-color:#702376');
  $('.pagination>li>a').attr('style','color:#702376');
console.log(window.location.pathname.split('/'));
$('.jtvc').click(function(){
  $('.jtvc').removeAttr('checked');
  var product = $(this).val();
  var url = window.location.pathname.split('/');
  console.log(url[1]+'/'+product);

  if (url.length == 2) {
    location.href = url[1] +'/'+product;
  }else {
    location.href = product;
  }
});

$('#list_value').change(function(){
  var url = window.location.pathname.split('/');
  var list_value = $('#list_value').val();
  console.log(url);
  if (url.length == 2) {
     location.href = url[1]+'/'+list_value;
  }else {
     location.href =list_value;
  }
});

function sepeteEkle(product_id) {
  //console.log(product_id);
  $.ajax({
      url: '/sepete-ekle',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      data: {product_id: product_id},
      success: function(data){
        sepetYenile();
        alertify.notify('Ürün Sepete Eklendi.', 'success', 5, function(){  console.log('ok'); });
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetYenile() {
  $.ajax({
      url: '/sepet-yenile',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      success: function(data){
        $('#mini-cart').empty();
        document.getElementById('mini-cart').innerHTML = data;
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetSil(id) {
    console.log(id);
    $.ajax({
        url: '/sepet-urun-sil',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id: id},
        success: function(data){
          sepetYenile();
          alertify.notify('Ürün Sepetden Silindi', 'error', 5, function(){  console.log('ok'); });
        },
        error: function(jqXHR, textStatus, err){}
    });
  }

$('.add-wishlist').click(function () {
    var product_id = $(this).attr('id');
    console.log(product_id);
  $.ajax({
      url: '/product/my-wishlist/add',
      type: 'POST',
      beforeSend: function (xhr) {
        var token = $('meta[name="csrf_token"]').attr('content');

        if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      cache: false,
      data: {product_id: product_id},
      success: function(data){

        if (data == 1) {
          $.notify("Ürün Beğeni Listenize Eklendi.",'success');
        }else if(data == 2) {
          $.notify("Ürün Beğeni Listenizden Silindi.",'error');
        }
      },
      error: function(jqXHR, textStatus, err){}
  });
});
$('.quick-view').click(function(){
  var id = $(this).attr('id');
      $.ajax({
          url: '/quick_view/'+id,
          type: 'POST',
          beforeSend: function (xhr) {
              var token = $('meta[name="csrf_token"]').attr('content');

              if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
              }
          },
          cache: false,
          data: {id:id},
          success: function(data){
              document.getElementById('Popup').innerHTML = data;
          },
          error: function(jqXHR, textStatus, err){}
      });
    });
</script>
@endsection
