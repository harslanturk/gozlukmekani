<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form action="" method="post">
      {{csrf_field()}}
  <div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="product-view-area">
        <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
          @if($product->discount > 0)
            <div class="icon-sale-label sale-left">İndirim</div>
          @endif
          <div class="large-image">
            <a href="{{$images[0]->image}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="{{$images[0]->image}}" alt="products"> </a>
          </div>
          <div class="flexslider flexslider-thumb" style="padding:15px 9px;">
            <ul class="previews-list slides">
              @foreach($images as $image)
              <li>
                <a href='{{$image->image}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$image->image}}' ">
                  <img src="{{$image->image}}" alt = "Thumbnail 1"/>
                </a>
              </li>
              @endforeach
            </ul>
          </div>

          <!-- end: more-images -->

        </div>
        <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
          <div class="product-details-area">
            <div class="product-name">
              <h1>{{$product->product_number}}</h1>
            </div>
            <div class="price-box">
              <?php
                $discount_price = $product->out_price - (($product->out_price * $product->discount)/100)
               ?>
              <p class="special-price"> <span class="price-label">Special Price</span>
                <span class="price">  {{$discount_price}}<i class="fa fa-try"></i> </span> </p>
                @if($product->discount)
                  <p class="old-price"> <span class="price-label">Regular Price:</span>
                  <span class="price"> {{$product->out_price}} <i class="fa fa-try"></i> </span> </p>
                @endif
            </div>
            <div class="ratings">
              <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
              </div>
              <p class="rating-links">
                <a href="#reviews" data-toggle="tab" class="focusla">{{$comments->count()}} Yorum</a>
              </p>
              @if($product->stock >0)
                <p class="availability in-stock pull-right">Stok Durumu: <span>Stokda Var</span></p>
              @else
                <p class="availability out-of-stock pull-right">Stok Durumu: <span>Stokda Yok</span></p>
              @endif
            </div>
            <div class="short-description">
              <h2>Hızlı Bakış</h2>
              <p>{{$product->quick_content}}</p>
              <br><br><br><br>
            </div>
            <div class="product-variation">
              <form action="#" method="post">
                <div class="cart-plus-minus">
                  <label for="qty">Adet:</label>
                  <div class="numbers-row">
                    <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                    <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                    <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                  </div>
                </div>
                @if($product->stock >0)
                  <button class="button pro-add-to-cart" title="Sepete Ekle" type="button" onclick="sepeteEkle({{$product->id}});">
                    <span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span>
                  </button>
                @else
                  <button class="button pro-add-to-cart" title="Sepete Ekle" type="button"><span><i class="fa fa-shopping-cart"></i> Tükendi</span></button>
                @endif
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="modal-footer" style="border-top:0px;">
  </div>
</form>
</div>
@section('jscode')
<!-- flexslider js -->
<script type="text/javascript" src="/front/js/jquery.flexslider.js"></script>
<!--cloud-zoom js -->
<script type="text/javascript" src="/front/js/cloud-zoom.js"></script>
@endsection
