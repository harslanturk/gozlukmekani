@extends('front.master')

@section('content')

<!-- Breadcrumbs -->
<input type="hidden" id="product_id" value="{{$product->id}}">
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <?php
          $url = App\Helpers\helper::getBreadcrumbs(url()->current());
         ?>
        <ul>
          <li class="home"> <a title="Go to Home Page" href="/">Anasayfa</a><span>&raquo;</span></li>
          {!! $url !!}
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<div class="main-container col1-layout">
  <div class="container">
    <div class="row">
      <div class="col-main">
        <div class="product-view-area">
          <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
            @if($product->discount > 0)
              <div class="icon-sale-label sale-left">İndirim</div>
            @endif
            <div class="large-image">
              <a href="{{$images[0]->image}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="{{$images[0]->image}}" alt="products"> </a>
            </div>
            <div class="flexslider flexslider-thumb">
              <ul class="previews-list slides">
                @foreach($images as $image)
                <li>
                  <a href='{{$image->image}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$image->image}}' ">
                    <img src="{{$image->image}}" alt = "Thumbnail 1"/>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>

            <!-- end: more-images -->

          </div>
          <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
            <div class="product-name">
              <h1>{{$product->product_number}}</h1>
            </div>
            <div class="price-box">
              <?php
                $discount_price = $product->out_price - (($product->out_price * $product->discount)/100)
               ?>
              <p class="special-price"> <span class="price-label">Special Price</span>
                <span class="price">  {{$discount_price}}<i class="fa fa-try"></i> </span> </p>
                @if($product->discount)
                  <p class="old-price"> <span class="price-label">Regular Price:</span>
                  <span class="price"> {{$product->out_price}} <i class="fa fa-try"></i> </span> </p>
                @endif
            </div>
            <div class="ratings">
              <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
              </div>
              <p class="rating-links">
                <a href="#reviews" data-toggle="tab" class="focusla">{{$comments->count()}} Yorum</a>
                <span class="separator">|</span>
                <a href="#reviews" data-toggle="tab" class="focusla">Yorum Ekle</a>
              </p>
              @if($product->stock >0)
                <p class="availability in-stock pull-right">Stok Durumu: <span>Stokda Var</span></p>
              @else
                <p class="availability out-of-stock pull-right">Stok Durumu: <span>Stokda Yok</span></p>
              @endif
            </div>
            <div class="short-description">
              <h2>Hızlı Bakış</h2>
              <p>{{$product->quick_content}}</p>
              <br><br><br><br>
            </div>
            <div class="product-variation">
              <form action="#" method="post">
                <div class="cart-plus-minus">
                  <label for="qty">Adet:</label>
                  <div class="numbers-row">
                    <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                    <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                    <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                  </div>
                </div>
                @if($product->stock >0)
                  @if(Auth::user())
                    <button type="button" class="button pro-add-to-cart" onclick="sepeteEkle({{$product->id}});"> <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span> </button>
                  @else
                    <button class="button pro-add-to-cart goToLogin" title="Sepete Ekle" type="button"><span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span></button>
                  @endif
                @else
                  <button class="button pro-add-to-cart" title="Sepete Ekle" type="button"><span><i class="fa fa-shopping-cart"></i> Tükendi</span></button>
                @endif
              </form>
            </div>
            <div class="product-cart-option">
              <ul>
                <li>
                  <?php
                  if (!empty(Auth::user()->id)) {
                    $wishlist = App\Helpers\helper::getWishlist(Auth::user()->id,$product->id);
                  }else {
                    $wishlist = 2;
                  }
                   ?>
                  @if($wishlist && $wishlist != 2)
                    <a class="add-wishlist" id="{{$product->id}}" style="cursor:pointer;"><i class="fa fa-heart" style="color:#702376;"></i>
                      <span style="color:#702376;">Beğeni Listemden Çıkar</span>
                    </a>
                  @elseif($wishlist == 2)
                    <a style="cursor:pointer;"><i class="fa fa-heart"></i>
                      <span>Beğeni Listesine Ekle</span>
                    </a>
                  @else
                    <a class="add-wishlist" id="{{$product->id}}" style="cursor:pointer;"><i class="fa fa-heart"></i>
                      <span>Beğeni Listesine Ekle</span>
                    </a>
                  @endif
                </li>
                <li><a href="#"><i class="fa fa-envelope"></i><span>Arkadaşına Gönder</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="product-overview-tab">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="product-tab-inner">
                <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                  <li class="active"> <a href="#description" data-toggle="tab"> Açıklama </a> </li>
                  <li> <a href="#reviews" data-toggle="tab">Yorum</a> </li>
                </ul>
                <div id="productTabContent" class="tab-content">
                  <div class="tab-pane fade in active" id="description">
                    <div class="std">
                      {!! $product->content !!}
                    </div>
                  </div>
                  <div id="reviews" class="tab-pane fade">
                    <?php
                    if (!empty(Auth::user()->id)) {
                      $comment_control = App\Helpers\helper::getComment($product->id,Auth::user()->id);
                    }else {
                      $comment_control = '';
                    }
                     ?>
                     @if(!$comment_control)
                      <div class="col-sm-5 col-lg-5 col-md-5">
                      @else
                       <div class="col-sm-12 col-lg-12 col-md-12">
                      @endif
                      <div class="reviews-content-left">
                        <h2>Kullanıcı Yorumları</h2>
                        @foreach($comments as $comment)
                        <div class="review-ratting">
                          <p>{{$comment->title}}</p>
                          <span>{{$comment->content}}</span>
                          <table>
                            <tbody>
                              <tr>
                                <th>Puan</th>
                                <td>
                                  <div class="rating">
                                    @if($comment->stars == 5)
                                      @for($i=0;$i<$comment->stars;$i++)
                                        <i class="fa fa-star"></i>
                                      @endfor
                                    @elseif($comment->stars == 4)
                                      @for($i=0;$i<$comment->stars;$i++)
                                        <i class="fa fa-star"></i>
                                      @endfor
                                        <i class="fa fa-star-o"></i>
                                    @elseif($comment->stars == 3)
                                      @for($i=0;$i<$comment->stars;$i++)
                                        <i class="fa fa-star"></i>
                                      @endfor
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    @elseif($comment->stars == 2)
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                      @for($i=0;$i<($comment->stars+1);$i++)
                                        <i class="fa fa-star-o"></i>
                                      @endfor
                                    @elseif($comment->stars == 1)
                                      <i class="fa fa-star"></i>
                                      @for($i=0;$i<($comment->stars+3);$i++)
                                        <i class="fa fa-star-o"></i>
                                      @endfor
                                    @endif
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <p class="author"> {{$comment->user->name.' '.$comment->user->surname}}<small> {{$comment->created_at}} </small> </p>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    @if(!$comment_control)
                    <div class="col-sm-7 col-lg-7 col-md-7">
                      <div class="reviews-content-right">
                          <label>Puanla <em>*</em></label>
                          <div class="table-responsive reviews-table">
                            <table>
                              <ul class="rate-area">
                                <input type="radio" id="5-star" name="star" value="5" class="star"/>
                                <label for="5-star" title="Süper">5 Yıldız</label>
                                <input type="radio" id="4-star" name="star" value="4" class="star"/>
                                <label for="4-star" title="İyi">4 Yıldız</label>
                                <input type="radio" id="3-star" name="star" value="3" class="star"/>
                                <label for="3-star" title="Ne İyi Ne Kötü">3 Yıldız</label>
                                <input type="radio" id="2-star" name="star" value="2" class="star"/>
                                <label for="2-star" title="İyi Değil">2 Yıldız</label>
                                <input type="radio" id="1-star" name="star" value="1" class="star"/>
                                <label for="1-star" title="Kötü">1 Yıldız</label>
                              </ul>
                            </table>
                          </div>
                          <div class="form-area">
                            <div class="form-element">
                              <label>Başlık <em>*</em></label>
                              <input type="text" name="title" id="title" class="form-control" required>
                            </div>
                            <div class="form-element">
                              <label>Yorum <em>*</em></label>
                              <textarea class="form-control" id="content" style="resize:none;" required></textarea>
                            </div>
                            @if(Auth::user())
                            <div class="buttons-set">
                                <button class="button submit" title="Yorum Yap" type="button" id="comment_button"><span><i class="fa fa-thumbs-up"></i> Yorum Yap</span></button>
                            </div>
                            @else
                            <div class="buttons-set">
                                <button class="button submit goToLogin" title="Yorum Yap" type="button"><span><i class="fa fa-thumbs-up"></i> Giriş Yapın</span></button>
                            </div>
                            @endif
                          </div>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container End -->
@endsection
@section('jscode')
<!-- flexslider js -->
<script type="text/javascript" src="/front/js/jquery.flexslider.js"></script>
<!--cloud-zoom js -->
<script type="text/javascript" src="/front/js/cloud-zoom.js"></script>
<script type="text/javascript">
function sepeteEkle(product_id) {
  //console.log(product_id);
  var quantity = $('#qty').val();
  console.log(quantity);
  $.ajax({
      url: '/sepete-ekle',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      data: {
        product_id: product_id,
        quantity:quantity
      },
      success: function(data){
        sepetYenile();
        alertify.notify('Ürün Sepete Eklendi.', 'success', 5, function(){  console.log('ok'); });
        //$.notify("Ürün Sepete Eklendi.",'success');
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetYenile() {
  $.ajax({
      url: '/sepet-yenile',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      success: function(data){
        $('#mini-cart').empty();
        document.getElementById('mini-cart').innerHTML = data;
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetSil(id) {
    console.log(id);
    $.ajax({
        url: '/sepet-urun-sil',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id: id},
        success: function(data){
          sepetYenile();
          //$.notify("Ürün Sepetden Silindi.",'error');
          alertify.notify('Ürün Sepetden Silindi', 'error', 5, function(){  console.log('ok'); });
        },
        error: function(jqXHR, textStatus, err){}
    });
  }
$(document).ready(function () {

  $('.goToLogin').click(function(){
    location.href='/login';
  });
  $('.focusla').click(function(){
      $('html, body').animate({
        scrollTop: $("#reviews").offset().top
      }, 1000);

      $('#product-detail-tab li').eq(0).removeAttr('class');
      $('#product-detail-tab li').eq(1).attr('class','active');
    });
});
$('#comment_button').click(function(){
  var stars        = $('.star:checked').val();
  var product_id  = $('#product_id').val();
  var title       = $('#title').val();
  var content     = $('#content').val();
  console.log(stars);
  if (stars) {
    $.ajax({
        url: '/product/comment/new',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {
          stars:stars,
          product_id:product_id,
          title:title,
          content:content
        },
        success: function(data){
          if (data == 1) {
            alert('Başarılı');
          }
        },
        error: function(jqXHR, textStatus, err){}
    });
  }else {
    alertify.alert('Lütfen Puanlama Yapınız.').set({'closableByDimmer': true});
  }
});
$('.add-wishlist').click(function () {
    var product_id = $(this).attr('id');
    console.log(product_id);
    $.ajax({
        url: '/product/my-wishlist/add',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {product_id: product_id},
        success: function(data){

          if (data == 1) {
            $('.product-cart-option li .add-wishlist i').attr('style','color:#702376');
            $('.product-cart-option li .add-wishlist span').attr('style','color:#702376');
            $('.add-wishlist span')[0].innerHTML = 'Beğeni Listemden Çıkar';
          }else if(data == 2) {
            $('.product-cart-option li .add-wishlist i').removeAttr('style');
            $('.product-cart-option li .add-wishlist span').removeAttr('style');
            $('.add-wishlist span')[0].innerHTML = 'Beğeni Listesine Ekle';
          }
        },
        error: function(jqXHR, textStatus, err){}
    });
});
alertify.defaults = {
        // dialogs defaults
        autoReset:true,
        basic:false,
        closable:true,
        closableByDimmer:true,
        frameless:false,
        maintainFocus:true, // <== global default not per instance, applies to all dialogs
        maximizable:true,
        modal:true,
        movable:true,
        moveBounded:false,
        overflow:true,
        padding: true,
        pinnable:true,
        pinned:true,
        preventBodyShift:false, // <== global default not per instance, applies to all dialogs
        resizable:true,
        startMaximized:false,
        transition:'pulse',

        // notifier defaults
        notifier:{
            // auto-dismiss wait time (in seconds)
            delay:5,
            // default position
            position:'bottom-right',
            // adds a close button to notifier messages
            closeButton: false
        },

        // language resources
        glossary:{
            // dialogs default title
            title:'Gözlük Mekanı',
            // ok button text
            ok: 'Tamam',
            // cancel button text
            cancel: 'İptal'
        },

        // theme settings
        theme:{
            // class name attached to prompt dialog input textbox.
            input:'ajs-input',
            // class name attached to ok button
            ok:'ajs-ok',
            // class name attached to cancel button
            cancel:'ajs-cancel'
        }
    };
</script>
@endsection
