<!DOCTYPE html>
<html lang="tr">
<head>
<!-- Basic page needs -->
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<!--[if IE]>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <![endif]-->
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>GÖZLÜK MEKANI</title>
<meta name="description" content="">
<!-- Ajax için token üretme -->
<meta name="csrf_token" content="{{ csrf_token() }}" />
<!-- Mobile specific metas  -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="{{URL::to('favicon.ico')}}">
<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- CSS Style -->
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/front/css/bootstrap.min.css">
<!-- font-awesome & simple line icons CSS -->
<link rel="stylesheet" type="text/css" href="/front/css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="/front/css/simple-line-icons.css" media="all">
<!-- owl.carousel CSS -->
<link rel="stylesheet" type="text/css" href="/front/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/front/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="/front/css/owl.transitions.css">
<!-- animate CSS  -->
<link rel="stylesheet" type="text/css" href="/front/css/animate.css" media="all">
<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="/front/css/flexslider.css" >
<!-- jquery-ui.min CSS  -->
<link rel="stylesheet" type="text/css" href="/front/css/jquery-ui.css">
<!-- Revolution Slider CSS -->
<link href="/front/css/revolution-slider.css" rel="stylesheet">
<!-- style CSS -->
<link rel="stylesheet" type="text/css" href="/front/css/style.css" media="all">
<!-- Gozluk Puanlama CSS -->
<link rel="stylesheet" type="text/css" href="/front/css/gozluk.css" media="all">
<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/semantic.min.css"/>
<link rel="stylesheet" type="text/css" href="/front/css/duzenleme.css">
</head>

<body class="cms-index-index cms-home-page">
<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

<!-- mobile menu -->
@include('front.layouts.mobile_menu')
<!-- end mobile menu -->
<div id="page">

  <!-- Header -->
  <header>
    <div class="header-container">
      @include('front.layouts.top_menu')
      @include('front.layouts.middle_menu')
    </div>
  </header>

  <!-- end header -->

  <!-- Navbar -->
  @include('front.layouts.bottom_menu')
  <!-- end nav -->
  @yield('slider')

  @yield('content')

  <!-- Footer -->
@include('front.layouts.footer')
  <!--Newsletter Popup Start-->
  <div id="myModal" class="modal fade">
    <div class="modal-dialog newsletter-popup">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="modal-body">
          <h4 class="modal-title">Haberdar Ol</h4>
          <form id="newsletter-form" method="post" action="#">
            <div class="content-subscribe">
              <div class="form-subscribe-header">
                <label>Email yazıp %10 indirimden hemen faydalan.</label>
              </div>
              <div class="input-box">
                <input type="text" class="input-text newsletter-subscribe" title="Sign up for our newsletter" name="email" placeholder="Email Adresinizi Giriniz">
              </div>
              <div class="actions">
                <button class="button-subscribe" title="Subscribe" type="submit">Subscribe</button>
              </div>
            </div>
          </form>
          <div class="subscribe-bottom">
            <input name="notshowpopup" id="notshowpopup" type="checkbox">
            Tekrar Gösterme </div>
        </div>
      </div>
    </div>
  </div>
  <!--End of Newsletter Popup-->

</div>

<!-- JS -->

<!-- JS -->

<!-- jquery js -->
<script type="text/javascript" src="/front/js/jquery.min.js"></script>

<!-- bootstrap js -->
<script type="text/javascript" src="/front/js/bootstrap.min.js"></script>

<!-- owl.carousel.min js -->
<script type="text/javascript" src="/front/js/owl.carousel.min.js"></script>

<!-- bxslider js -->
<script type="text/javascript" src="/front/js/jquery.bxslider.js"></script>
<!-- megamenu js -->
<script type="text/javascript" src="/front/js/megamenu.js"></script>
<script type="text/javascript">
  /* <![CDATA[ */
  var mega_menu = '0';

  /* ]]> */
  </script>

<!-- jquery.mobile-menu js -->
<script type="text/javascript" src="/front/js/mobile-menu.js"></script>

<!--jquery-ui.min js -->
<script type="text/javascript" src="/front/js/jquery-ui.js"></script>

<!-- main js -->
<script type="text/javascript" src="/front/js/main.js"></script>

<!-- countdown js -->
<script type="text/javascript" src="/front/js/countdown.js"></script>
<!-- Maskeleme js -->
<script type="text/javascript" src="/front/js/jquery.mask.min.js"></script>
<!-- Notify js -->
<script type="text/javascript" src="/front/js/notify.min.js"></script>
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
<!-- Revolution Slider -->
<!-- Revolution slider -->

@yield('jscode')
</html>
