@extends('front.master')
@section('content')


<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
    <div class="col-main">
      <div class="cart">

        <div class="page-content page-order"><div class="page-title">
          <h2 style="color:#702376;">Teslimat Bilgileri</h2>
        </div>
          <div class="order-detail-content">
            <div class="table-responsive">
              @if($adress->count())
              <table class="table table-bordered cart_summary" id="sepet">
                <thead>
                  <tr>
                    <th class="text-center">Teslimat Adresi</th>
                    <th class="text-center">Fatura Adresi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <select class="select2" name="teslimat_adresi" id="teslimat_adresi" style="width: 100%;">
                          <option value="0">Lütfen Seçiniz</option>
                        @foreach($adress as $adres)
                          <option value="{{$adres->adres}}">{{$adres->name.' -'.$adres->adres}}</option>
                        @endforeach
                      </select>
                    </td>
                    <td>
                      <select class="select2" name="fatura_adresi" id="fatura_adresi" style="width: 100%;">
                          <option value="0">Lütfen Seçiniz</option>
                        @foreach($adress as $adres)
                          <option value="{{$adres->adres}}">{{$adres->name.' -'.$adres->adres}}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">
                      <input type="checkbox" name="sozlesme_onay" id="sozlesme_onay"> 
                      &nbsp;<a href="#" class="onBilgilendirme" data-toggle="modal" data-target="#onBilgilendirme"><strong>Ön Bilgilendirme</strong></a> ve
                      &nbsp;<a href="#" class="mesafeliSatis" data-toggle="modal" data-target="#mesafeliSatis"><strong>Mesafali Satış Sözleşmesi</strong></a>
                      'ni Okudum Onaylıyorum.
                    </td>
                  </tr>
                </tfoot>
              </table>
              @else
            <div class="alert alert-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert"></button>
                <strong>Adres Oluşturmak İçin <a href="/myaccount/adres">Tıklayın </a>!</strong>
            </div>
              @endif
            </div>
          <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert"></button>
              <strong>KARGO ÜCRETSİZDİR.</strong>
          </div>
            <div class="cart_navigation">
              <a class="continue-btn" href="/myaccount/cart"><i class="fa fa-arrow-left"> </i>&nbsp; Sepetime Dön</a>
              <a class="checkout-btn" onclick="odemeGonder();" href="#"><i class="fa fa-arrow-right"></i> Ödemeye Geç</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <!-- service section -->
 <?php 
  $sUser = Auth::user();
  $customer = App\Customer::where('user_id',$sUser->id)->first();
  $ref = time().$sUser->id;
  $bugun = date("Y-m-d h:i:s");
  $my_cart = App\Helpers\helper::myCart($sUser->id);
  $total = 0;
  $secretKey="N8J!T4@1|n+h1|(~l#W9";
  /*$input = "8GZLUKMEK20N8J!T4@1|n+h1|(~l#W9"."192018-06-11 15:51:3519MacBook Air 13 inch9iPhone 4S5MBA134IP4S27Extended Warranty - 5 Years0420006500.5011122242242503EUR21010București10București2RO8CCVISAMC5GROSS3NET4TRUE";
$output = hash_hmac('md5', $input, $secretKey);
  echo $ref;*/
  /*$arParams = array(

   "MERCHANT" => "GZLUKMEK",
   "ORDER_REF" =>  $ref,
   "ORDER_DATE" => $bugun,
   "ORDER_PNAME[0]" => "DVD Player",
   "ORDER_PCODE[0]" => "CDPLY",
   "ORDER_PINFO[0]" => "Incarcator inclus",
   "ORDER_PRICE[0]" => "3.50",
   "ORDER_QTY[0]" => "1",
   "ORDER_VAT[0]"=>"24",
   "ORDER_SHIPPING"=>"0",
   "PRICES_CURRENCY" => "TRY",
   "DISCOUNT" => "0",
   "DELIVERY_CITY" => "Radauti",
   "DELIVERY_STATE" => "Suceava",
   "DELIVERY_COUNTRYCODE" => "RO",
   "PAY_METHOD" => "CCVISAMC",
   "ORDER_PRICE_TYPE[0]"=>"GROSS",
);*/
      $arParams = array();
      $pName = array();
      $pCode = array();
      $pInfo = array();
      $pPrice = array();
      $pQTY = array();
      $pVat = array();
      $pType = array();
      $user = Auth::user();
      $carts = App\Cart::where('user_id',$user->id)->get();
      foreach($carts as $key => $value)
      {
        $pName = array_merge($pName, array('ORDER_PNAME['.$key.']' => $value->product->product_number));
        $pCode = array_merge($pCode, array('ORDER_PCODE['.$key.']' => $value->product_id));
        $pInfo = array_merge($pInfo, array('ORDER_PINFO['.$key.']' => "Gözlük Mekanı icerik"));
        $pPrice = array_merge($pPrice, array('ORDER_PRICE['.$key.']' => $value->product->out_price));
        $pQTY = array_merge($pQTY, array('ORDER_QTY['.$key.']' => $value->quantity));
        $pVat = array_merge($pVat, array('ORDER_VAT['.$key.']' => "24"));
        $pType = array_merge($pType, array('ORDER_PRICE_TYPE['.$key.']' => "GROSS"));
      }
      $topParams = array(
         "MERCHANT" => "GZLUKMEK",
         "ORDER_REF" =>  $ref,
         "ORDER_DATE" => $bugun,        
      );
      $midParams = array(
         "ORDER_SHIPPING"=>"0",
         "PRICES_CURRENCY" => "TRY",
         "DISCOUNT" => "0",
         "DELIVERY_CITY" => "Radauti",
         "DELIVERY_STATE" => "Suceava",
         "DELIVERY_COUNTRYCODE" => "RO",
         "PAY_METHOD" => "CCVISAMC",
      );
      $arParams = array_merge($arParams, $topParams, $pName, $pCode, $pInfo, $pPrice, $pQTY, $pVat, $midParams, $pType);
     /* echo "<pre>";
      print_r($arParams);
      die();*/

//ksort($arParams);
$hashString = "";
foreach ($arParams as $key => $val) {
    $hashString .= strlen($val) . $val;
}
$hashString .= 0;
$arParams["ORDER_HASH"] = hash_hmac("md5", $hashString, $secretKey);
/*echo "<pre>";
echo $hashString."<br>";
echo $arParams["ORDER_HASH"]."<br>";
die();*/
 ?>
<form id="liveUpdate" action="https://secure.payu.com.tr/order/lu.php" method="post" style="display:none;">
        <div><input id="sat_gonder" value="LiveUpdate" type="submit">&nbsp;&nbsp;<input id="generate" value="HASH IT" type="button">&nbsp;&nbsp;<input id="formGenerator" value="FORM ME" type="button"><br><br></div>
        <input class="HASH" id="POST_URL" value="https://secure.payu.com.tr/order/lu.php" type="text">
        <input class="HASH" name="MERCHANT" value="GZLUKMEK" id="MERCHANT" type="text">
        <input class="HASH" name="ORDER_HASH" value="<?php echo $arParams["ORDER_HASH"]; ?>" id="ORDER_HASH" type="text">
        <input class="HASH" id="SECRET_KEY" name="" value="N8J!T4@1|n+h1|(~l#W9" type="text">
        <input class="HASH" id="BACK_REF" name="BACK_REF" value="" type="text">
        <select class="HASH" name="TESTORDER">
			<option value="TRUE">TRUE</option>
			<option value="FALSE" selected="selected">FALSE</option>
		</select>
        <select class="HASH" name="AUTOMODE">
			<option value="1" selected="selected">TRUE</option>
			<option value="0">FALSE</option>
		</select>
            <input name="LANGUAGE" value="TR" id="LANGUAGE" type="text">
            <input name="ORDER_REF" value="<?php echo $ref; ?>" id="ORDER_REF" type="text">
            <input name="ORDER_DATE" value="<?php echo $bugun; ?>" id="ORDER_DATE" type="text">
            <input name="DESTINATION_CITY" value="Radauti" id="DESTINATION_CITY" type="text">
            <input name="DESTINATION_STATE" value="Suceava" id="DESTINATION_STATE" type="text">
            <input name="DESTINATION_COUNTRY" value="RO" id="DESTINATION_COUNTRY" type="text">
            <input name="BILL_FNAME" value="<?php echo $sUser->name; ?>" id="BILL_FNAME" type="text">
            <input name="BILL_LNAME" value="<?php echo $sUser->surname; ?>" id="BILL_LNAME" type="text">
            <input name="BILL_CISERIAL" value="EP" id="BILL_CISERIAL" type="text">
            <input name="BILL_CINUMBER" value="123456" id="BILL_CINUMBER" type="text">
            <input name="BILL_CIISSUER" id="BILL_CIISSUER" type="text">
            <input name="BILL_CNP" id="BILL_CNP" type="text">
            <input name="BILL_COMPANY" id="BILL_COMPANY" type="text">
            <input name="BILL_FISCALCODE" id="BILL_FISCALCODE" type="text">
            <input name="BILL_REGNUMBER" id="BILL_REGNUMBER" type="text">
            <input name="BILL_BANK" id="BILL_BANK" type="text">
            <input name="BILL_BANKACCOUNT" id="BILL_BANKACCOUNT" type="text">
            <input name="BILL_EMAIL" value="<?php echo $sUser->email; ?>" id="BILL_EMAIL" type="text">
            <input name="BILL_PHONE" value="<?php echo $customer->gsm; ?>" id="BILL_PHONE" type="text">
            <input name="BILL_FAX" value="" id="BILL_FAX" type="text">
            <input name="BILL_ADDRESS" value="" id="BILL_ADDRESS" type="text">
            <input name="BILL_CITY" value="" id="BILL_CITY" type="text">
            <input name="BILL_STATE" value="" id="BILL_STATE" type="text">
            <input name="BILL_COUNTRYCODE" value="TR" id="BILL_COUNTRYCODE" type="text">
            <input name="DELIVERY_FNAME" value="<?php echo $sUser->name; ?>" id="DELIVERY_FNAME" type="text">
            <input name="DELIVERY_LNAME" value="<?php echo $sUser->surname; ?>" id="DELIVERY_LNAME" type="text">
            <input name="DELIVERY_COMPANY" value="Example. INC" id="DELIVERY_COMPANY" type="text">
            <input name="DELIVERY_PHONE" value="<?php echo $customer->gsm; ?>" id="DELIVERY_PHONE" type="text">
            <input name="DELIVERY_ADDRESS" value="" id="DELIVERY_ADDRESS" type="text">
            <input name="DELIVERY_CITY" value="" id="DELIVERY_CITY" type="text">
            <input name="DELIVERY_STATE" value="" id="DELIVERY_STATE" type="text">
            <input name="DELIVERY_COUNTRYCODE" value="" id="DELIVERY_COUNTRYCODE" type="text">
            <input name="LANGUAGE" value="TR" id="LANGUAGE" type="text">
			<input name="INSTALLMENT_OPTIONS" id="INSTALLMENT_OPTIONS" type="text">
			<select name="PRICES_CURRENCY" id="PRICES_CURRENCY">
					<option selected="selected" value="TRY">TRY</option>
                    <option value="RON">RON</option>
                    <option value="EUR">EUR</option>
                    <option value="USD">USD</option>
                    <option value="GBP">GBP</option>
                    <option value="RUB">RUB</option>
                    <option value="HUF">HUF</option>
                </select>
			<select name="PAY_METHOD" id="PAY_METHOD">
                    <option value="">NONE</option>
                    <option selected="selected" value="CCVISAMC">CCVISAMC</option>
                    <option value="CCAMEX">CCAMEX</option>
                    <option value="CCDINERS">CCDINERS</option>
                    <option value="CCJCB">CCJCB</option>
                    <option value="WIRE">WIRE</option>
                    <option value="PAYPAL">PAYPAL</option>
                    <option value="CASH">CASH</option>
                    <option value="MOBILIS">MOBILIS</option>
                    <option value="QIWI">QIWI</option>
                    <option value="YANDEX">YANDEX</option>
                    <option value="WEBMONEY">WEBMONEY</option>
                    <option value="MAILRU">MAILRU</option>
                    <option value="RAIFFEISEN_RU">RAIFFEISEN_RU</option>
                    <option value="PRIVATBANK">PRIVATBANK</option>
                </select>
                @foreach($carts as $cart)
      			<input name="ORDER_SHIPPING" value="0" id="ORDER_SHIPPING" type="text">
            <input name="DISCOUNT" value="0" id="DISCOUNT" type="text">
            <input id="ORDER_PNAME" value="{{$cart->product->product_number}}" name="ORDER_PNAME[]" type="text">
            <input id="ORDER_PCODE" value="{{$cart->product_id}}" name="ORDER_PCODE[]" type="text">
            <input id="ORDER_PINFO" value="Gözlük Mekanı icerik" name="ORDER_PINFO[]" type="text">
            <input id="ORDER_PRICE" value="{{$cart->product->out_price}}" name="ORDER_PRICE[]" type="text">
            <select id="ORDER_PRICE_TYPE" name="ORDER_PRICE_TYPE[]">
      				<option selected="selected">GROSS</option>
      				<option>NET</option>
      			</select>
            <input id="ORDER_QTY" value="{{$cart->quantity}}" name="ORDER_QTY[]" type="text">
            <input id="ORDER_VAT" value="24" name="ORDER_VAT[]" type="text">
            @endforeach
</form>
 <?php 
    /*$hash=$clientid . $oid . $amount . $okurl . $failurl . $trtype . $taksit . $rnd . $storekey;
    $hash=base64_encode(pack('H*',sha1($hash)));*/
    /*echo $ref."<br>".$hashString."<br>".$arParams["ORDER_HASH"];*/
    ?>

<!-- Modal -->
<div class="modal fade" id="onBilgilendirme" role="dialog" aria-labelledby="myModalLabel">

</div>
<!-- Modal -->
<div class="modal fade" id="mesafeliSatis" role="dialog" aria-labelledby="myModalLabel">

</div>
@endsection
@section('jscode')
<script type="text/javascript">
  $(document).ready(function(){
    $('.onBilgilendirme').click(function(){
      $.ajax({
          url: '/myaccount/on-bilgilendirme',
          type: 'POST',
          beforeSend: function (xhr) {
              var token = $('meta[name="csrf_token"]').attr('content');

              if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
              }
          },
          cache: false,
          data: {},
          success: function(data){
              document.getElementById('onBilgilendirme').innerHTML = data;
          },
          error: function(jqXHR, textStatus, err){}
      });
    });
    $('.mesafeliSatis').click(function(){
      $.ajax({
          url: '/myaccount/mesafeli-satis',
          type: 'POST',
          beforeSend: function (xhr) {
              var token = $('meta[name="csrf_token"]').attr('content');

              if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
              }
          },
          cache: false,
          data: {},
          success: function(data){
              document.getElementById('mesafeliSatis').innerHTML = data;
          },
          error: function(jqXHR, textStatus, err){}
      });
    });
  });
  function odemeGonder() {
    var teslimat_adresi = $('#teslimat_adresi').val();
    $('#DELIVERY_ADDRESS').val(teslimat_adresi);
    var fatura_adresi = $('#fatura_adresi').val();
    $('#BILL_ADDRESS').val(fatura_adresi);
    var sozlesme_onay = document.getElementById('sozlesme_onay').checked;
    if (sozlesme_onay == false) {
        alert('Lütfen  "Ön Bilgilendirme ve  Mesafali Satış Sözleşmesini Okudum Onaylıyorum." alanını seçiniz.');
      }
    else{
      $("#sat_gonder").click();
    }
  }
</script>
@endsection
