@extends('front.master')
@section('content')


<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
    <div class="col-main">
      <div class="cart">

        <div class="page-content page-order"><div class="page-title">
          <h2>Sepetim</h2>
        </div>
          <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Kapat</span></button>
              <strong>İndirimi Olan Ürünleriniz Genel Toplamdan Otomatik Olarak Düşülmüştür.</strong>
          </div>
          <div class="order-detail-content">
            <div class="table-responsive">
              <table class="table table-bordered cart_summary" id="sepet">
                <thead>
                  <tr>
                    <th class="cart_product">Ürün Resmi</th>
                    <th>Ürün Adı</th>
                    <th class="col-sm-2">Birim Fiyat</th>
                    <th class="col-sm-3">Adet</th>
                    <th class="col-sm-2">Toplam</th>
                    <th  class="action col-sm-1"><i class="fa fa-trash-o"></i></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $total = 0;
                  $total_dis = 0;
                  $discount = 0;
                   ?>
                  @foreach($carts  as $cart)
                  <?php
                    $cover = App\Helpers\helper::getImageCover($cart->product_id);
                    $total += ($cart->quantity * $cart->product->out_price);
                    $discount = $cart->product->discount;
                    if ($discount > 0) {
                      $total_dis += ((($cart->quantity * $cart->product->out_price)*$cart->product->discount)/100);
                    }
                   ?>
                  <tr>
                    <td class="cart_product"><a href="#"><img src="{{$cover->image}}" alt="{{$cart->product_number}}"></a></td>
                    <td class="cart_description"><p class="product-name"><a href="#">{{$cart->product->product_number}}</a></p>
                    <td class="price"><span>{{$cart->product->out_price}} <i class="fa fa-try"></i> </span></td>
                    <td>
                      <div class="cart-plus-minus">
                        <div class="numbers-row">
                          <div onclick="deincrement({{$cart->id}});" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                            <input type="text" class="qty" title="Adet" value="{{$cart->quantity}}" maxlength="12" name="qty" readonly>
                          <div onclick="increment({{$cart->id}});" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                        </div>
                      </div>
                    </td>
                    <td class="price"><span class="{{$cart->id}} urun_toplam">{{($cart->quantity * $cart->product->out_price)}} <i class="fa fa-try"></i></span></td>
                    <td class="action"><a href="/sepet-urun-sil/{{$cart->id}}"><i class="icon-close"></i></a></td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <!-- <tr>
                    <td colspan="2" rowspan="3"></td>
                    <td colspan="2">KDV Toplam</td>
                    <td colspan="2">$237.88 </td>
                  </tr>
                  <tr>
                    <td colspan="2"><strong >Ürün İndirimleri</strong></td>
                    <td colspan="2"><strong> {{$total_dis}} <i class="fa fa-try"></i></strong></td>
                  </tr> -->
                  <tr>
                    <td colspan="4" class="text-right"><strong style="color:#702376;">Genel Toplam</strong></td>
                    <td colspan="2" class="text-center"><strong style="color:#702376;" id="total"> {{($total-$total_dis)}} <i class="fa fa-try"></i></strong></td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="cart_navigation">
              <a class="continue-btn" href="/"><i class="fa fa-arrow-left"> </i>&nbsp; Alıverişe Devam Et</a>
              <a class="checkout-btn" href="/myaccount/teslimat-bilgileri"><i class="fa fa-check"></i> Ödemeye Geç</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <!-- service section -->
@endsection
@section('jscode')
<script type="text/javascript">
  function increment(cart_id) {
    var increment = 1;
    console.log(cart_id);
    $.ajax({
        url: '/sepet-adet-guncelle',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {
          cart_id: cart_id,
          increment:increment
        },
        success: function(data){
          sepetYenile();
        },
        error: function(jqXHR, textStatus, err){}
    });
  }

  function deincrement(cart_id) {
    console.log(cart_id);
      var increment = 0;
    $.ajax({
        url: '/sepet-adet-guncelle',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {
          cart_id: cart_id,
          increment:increment
        },
        success: function(data){
          sepetYenile();
        },
        error: function(jqXHR, textStatus, err){}
    });
  }
  function sepetYenile() {
    $.ajax({
        url: '/sepet-adet-yenile',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        success: function(data){
          $('#sepet').empty();
          document.getElementById('sepet').innerHTML = data;
        },
        error: function(jqXHR, textStatus, err){}
    });
  }
  // $('.quantity').keyup(function(){
  //   var birim_fiyat = $(this).attr('id');
  //   var indirim   = $(this).attr('name');
  //   var adet      = this.value;
  //   var id        = $(this).attr('role');
  //   var urun_toplam = birim_fiyat*adet;
  //   var genel_toplam = 0;
  //   var satir_toplam = 0;
  //   if (indirim > 0) {
  //     urun_toplam = (urun_toplam - ((urun_toplam * indirim)/100));
  //   }
  //   console.log('Fiyat:'+birim_fiyat+' İndirim:'+indirim+' Adet:'+adet+' CartId:'+id+'Toplam:'+urun_toplam);
  //   $('.'+id)[0].innerHTML = urun_toplam+' <i class="fa fa-try"></i>';
  //   //$('#total')[0].innerHTML =
  //   for (var i = 0; i < $('.urun_toplam').length; i++) {
  //     toplam =$('.urun_toplam')[i].innerText.trim();
  //     genel_toplam += (+toplam);
  //   }
  //   $('#total')[0].innerHTML = genel_toplam+' <i class="fa fa-try"></i>';
  //   console.log(genel_toplam);
  // });
</script>
@endsection
