@extends('front.master')

@section('content')

<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>Hesabım</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col2-right-layout">
  <div class="main container">
    <div class="row">
      <div class="col-main col-sm-9 col-xs-12">
        <div class="my-account">
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
            @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>{{Session::get('success')}}</strong>
                </div>
            @endif
          <div class="page-title" style="margin-bottom:15px;">
            <h2>Hesap Ayarlarım</h2>
          </div>
          <div class="row">
            <form action="/myaccount/save" method="post">
              {{csrf_field()}}
              <div class="col-sm-6">
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="form-group">
                  <label>Adınız</label>
                  <input type="text" class="form-control input" name="name" value="{{$user->name}}" required>
                </div>
                <div class="form-group">
                  <label>Soyadınız</label>
                  <input type="text" class="form-control input" name="surname" value="{{$user->surname}}" required>
                </div>
                <div class="form-group">
                  <label>Şifreniz</label>
                  <input type="password" class="form-control input" name="password">
                </div>
                <div class="form-group">
                  <label>E-Mail Adresiniz</label>
                  <input type="email" class="form-control input" name="email" value="{{$user->email}}" required>
                </div>
                <div class="form-group">
                  <button type="submit" class="button" name="button"> <i class="fa fa-save"></i> Güncelle</button>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Cep Telefonunuz</label>
                  <input type="text" class="form-control input" name="gsm" value="{{$customer->gsm}}">
                </div>
                <div class="form-group">
                  <label>T.C No</label>
                  <input type="number" class="form-control input" data-mask="00000000000" name="tc_no" value="{{$customer->tc_no}}" maxlength="11">
                </div>
                <div class="form-group">
                  <label>Doğum Tarihiniz</label>
                  <input type="date" class="form-control input"  name="birthdate" value="{{$customer->birthdate}}">
                </div>
                <div class="form-group">
                  <label>Cinsiyetiniz</label><br>
                  <label style="margin:5px;">
                    <input type="radio" name="gender" value="1" <?php echo ($customer->gender == 1)?'checked':''; ?> required> Erkek
                  </label>
                  <label style="margin:5px;">
                    <input type="radio" name="gender" value="2" <?php echo ($customer->gender == 2)?'checked':''; ?>> Bayan
                  </label>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <aside class="right sidebar col-sm-3 col-xs-12">
        <div class="sidebar-account block">
          <div class="sidebar-bar-title">
            <h3>Hesabım</h3>
          </div>
          <div class="block-content">
            <ul>
              <li class="current"><a>Hesap Ayarlarım</a></li>
              <li><a href="#">Siparişlerim</a></li>
              <li><a href="/myaccount/wishlist">Beğendiklerim</a></li>
              <li class="last"><a href="/myaccount/adres">Adreslerim</a></li>
            </ul>
          </div>
        </div>
      </aside>
    </div>
  </div>
</section>
<!-- service section -->
@endsection
@section('jscode')
<script type="text/javascript">

</script>
@endsection
