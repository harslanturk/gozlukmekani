@extends('front.master')

@section('content')

<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>Beğendiklerim</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col2-right-layout">
  <div class="main container">
    <div class="row">
      <div class="col-main col-sm-9 col-xs-12">
        <div class="my-account">
          <div class="page-title">
            <h2>Beğendiklerim</h2>
          </div>
          <div class="wishlist-item table-responsive">
          @if($wishlists->count())
            <table class="col-md-12">
              <thead>
                <tr>
                  <th class="th-delate">Sil</th>
                  <th class="th-product">Resim</th>
                  <th class="th-details">Ürün Adı</th>
                  <th class="th-price">Fiyatı</th>
                  <th class="th-total th-add-to-cart">Sepete Ekle </th>
                </tr>
              </thead>
              <tbody>
                @foreach($wishlists as $wishlist)
                <?php $kapak = App\Helpers\helper::getImageCover($wishlist->product_id); ?>
                <tr>
                  <td class="th-delate"><a href="/myaccount/wishlist/delete/{{$wishlist->id}}">X</a></td>
                  <td class="th-product"><a href="#"><img src="{{$kapak->image}}" alt="cart"></a></td>
                  <td class="text-center"><a href="#">{{$wishlist->product->product_number}}</a></td>
                  <td class="th-price">{{$wishlist->product->out_price}} <i class="fa fa-try"></i> </td>
                  <th class="td-add-to-cart">
                    <button type="button" class="button pro-add-to-cart" onclick="sepeteEkle({{$wishlist->product->id}});"> <i class="fa fa-shopping-cart"></i><span> Sepete Ekle</span> </button>
                  </th>
                </tr>
                @endforeach
              </tbody>
            </table>
            @else
            <table class="col-md-12">
              <thead>
                <tr>
                  <th class="th-delate">Beğendiğiniz Ürün Bulunmamakta.</th>
                </tr>
              </thead>
            </table>
          @endif
        </div>
        </div>
      </div>
      <aside class="right sidebar col-sm-3 col-xs-12">
        <div class="sidebar-account block">
          <div class="sidebar-bar-title">
            <h3>Hesabım</h3>
          </div>
          <div class="block-content">
            <ul>
              <li><a href="/myaccount">Hesap Ayarlarım</a></li>
              <li><a href="#">Siparişlerim</a></li>
              <li class="current"><a href="#">Beğendiklerim</a></li>
              <li class="last"><a href="/myaccount/adres">Adreslerim</a></li>
            </ul>
          </div>
        </div>
      </aside>
    </div>
  </div>
</section>
<!-- service section -->
@endsection
@section('jscode')
<!-- flexslider js -->
<script type="text/javascript" src="/front/js/jquery.flexslider.js"></script>
<!--cloud-zoom js -->
<script type="text/javascript" src="/front/js/cloud-zoom.js"></script>
<script type="text/javascript">

$(document).ready(function () {
  $('.focusla').click(function(){
      $('html, body').animate({
        scrollTop: $("#reviews").offset().top
      }, 1000);

      $('#product-detail-tab li').eq(0).removeAttr('class');
      $('#product-detail-tab li').eq(1).attr('class','active');
    });
});
function sepeteEkle(product_id) {
  //console.log(product_id);
  $.ajax({
      url: '/sepete-ekle',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      data: {product_id: product_id},
      success: function(data){
        sepetYenile();
        //$.notify("Ürün Sepete Eklendi.",'success');
        alertify.notify('Ürün Sepete Eklendi.', 'success', 5, function(){  console.log('ok'); });
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetYenile() {
  $.ajax({
      url: '/sepet-yenile',
      type: 'POST',
      beforeSend: function (xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
      },
      cache: false,
      success: function(data){
        $('#mini-cart').empty();
        document.getElementById('mini-cart').innerHTML = data;
      },
      error: function(jqXHR, textStatus, err){}
  });
}

function sepetSil(id) {
    console.log(id);
    $.ajax({
        url: '/sepet-urun-sil',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id: id},
        success: function(data){
          sepetYenile();
          //$.notify("Ürün Sepetden Silindi.",'error');
          alertify.notify('Ürün Sepetden Silindi', 'error', 5, function(){  console.log('ok'); });
        },
        error: function(jqXHR, textStatus, err){}
    });
  }
$('.add-wishlist').click(function () {
    var product_id = $(this).attr('id');
    console.log(product_id);
    $.ajax({
        url: '/product/my-wishlist/add',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {product_id: product_id},
        success: function(data){

          if (data == 1) {
            $('.product-cart-option li .add-wishlist i').attr('style','color:#702376');
            $('.product-cart-option li .add-wishlist span').attr('style','color:#702376');
            $('.add-wishlist span')[0].innerHTML = 'Beğeni Listemden Çıkar';
          }else if(data == 2) {
            $('.product-cart-option li .add-wishlist i').removeAttr('style');
            $('.product-cart-option li .add-wishlist span').removeAttr('style');
            $('.add-wishlist span')[0].innerHTML = 'Beğeni Listesine Ekle';
          }
        },
        error: function(jqXHR, textStatus, err){}
    });
});
</script>
@endsection
