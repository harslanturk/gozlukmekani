<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form action="#" method="post">
      {{csrf_field()}}
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">MESAFELİ SATIŞ SÖZLEŞMESİ</h4>
  </div>
  <div class="modal-body">
    <input type="hidden" name="customer_id" value="{{Auth::user()->id}}">
    <div class="form-group text-center">
      <h4>Mesafeli Satış Sözleşmesi</h4>
    </div>
    <div class="form-group">
      <label>Satıcı Bilgileri</label>
    </div>
    <div class="form-group">
      <label>Satıcı İsim/Ünvan : </label>
      <span>Gözlük Mekanı</span><br>
      <label>Satıcı'nın Açık Adresi : </label>
      <span>Dr. Sadık Ahmet Cad. Deniz Köşler Mah. Gülden Sok No:3/8 Avcılar / İSTANBUL</span><br>
      <label>Satıcı’nın Telefonu : </label>
      <span>0212 123 45 56</span><br>
      <label>Satıcı Mersis No : </label>
      <span> </span><br>
      <label>Satıcı E-Posta Adres : </label>
      <span>info@gozlukmekani.com</span><br>
      <label>Alıcı'nın İade Halnde Malı Satıcı'ya Göndereceği Kargo Şirketi : </label>
      <span>Yurtiçi</span><br>
    </div>
    <div class="form-group">
      <label>Alıcı Bilgileri</label>
    </div>
    <div class="form-group">
      <label>Alıcı Adı Soyadı : </label>
      <span>{{Auth::user()->name.' '.Auth::user()->surname}}</span><br>
      <label>Alıcı Adresi : </label>
      <span>{{$adres->adres.' '.$adres->city->name}}</span><br>
      <label>Alıcı Telefonu : </label>
      <span>{{$customer->gsm}}</span><br>
      <label>Alıcı E-Posta Adres : </label>
      <span>{{Auth::user()->email}}</span><br>
    </div>
    <div class="form-group text-center">
      <h4>SÖZLEŞME KONUSU VE KAPSAMI</h4>
    </div>
    <div class="form-group">
      <span>
        İşbu Mesafeli Satış Sözleşmesi (“Sözleşme”) 6502 Sayılı Tüketicinin Korunması Hakkında Kanun (“Kanun”) ve Mesafeli Sözleşmeler Yönetmeliği'ne uygun olarak düzenlenmiştir. İşbu Sözleşme'nin tarafları işbu Sözleşme tahtında Kanun'dan ve Mesafeli Sözleşmeler Yönetmeliği'nden kaynaklanan yükümlülük ve sorumluluklarını bildiklerini ve anladıklarını kabul ve beyan ederler. 
      </span>
      <br>
      <br>
      <span>
        İşbu Sözleşme'nin konusunu; Alıcı'nın, Gözlük Mekanı'na ait www.gozlukmekani.com alan adlı web stesnden (“Webstesi”), Satıcı'ya ait mal veya hizmetin satın alınmasına yönelik elektronik olarak sipariş verdiği, Sözleşme'de belirtilen niteliklere sahip mal veya hizmetin satışı ve teslim ile ilgili olarak Kanun ve Mesafeli Sözleşmeler Yönetmeliği hükümleri gereğince tarafların hak ve yükümlülüklernin belirlenmesi oluşturur.
      </span>
      <br>
      <br>
      <span>
        İşbu Sözleşme'nin akdedilmesi tarafların ayrı ayrı Gözlük Mekanı ile akdetmiş oldukları Websitesi Üyelik Sözleşmeler'nin hükümlernin ifasını engellemeyecek olup taraflar işbu Sözleşme konusu mal veya hizmetin satışında Gözlük Mekanı'nın herhangi bir şekilde taraf olmadığını ve Sözleşme kapsamında tarafların yükümlülüklerin yerine getirmeleri ile ilgili herhangi bir sorumluluğu ve taahhüdü bulunmadığını kabul ve beyan ederler.
      </span>
    </div>
    <div class="form-group text-center">
      <h4>SÖZLEŞME KONUSU MAL VE HİZMETİN TEMEL NİTELİKLERİ VE FİYATI (KDV DAHİL)</h4>
    </div>
    <div class="form-group">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td>Ürün Kodu Ve Adı</td>
            <td>Adet</td>
            <td>Satıcı Unvanı</td>
            <td>Birim Fyatı</td>
            <td>Birim İndirimi</td>
            <td>Kupon</td>
            <td>Puan</td>
            <td>Toplam Satış Tutarı</td>
            <td>Vade Farkı</td>
            <td>KDV Dahil Toplam Tutar</td>
          </tr>
        </thead>
        <?php 
            $genel_toplam = 0;
         ?>
        <tbody>
          @foreach($carts as $key => $cart)
          <?php 
            $birim_fiyat = $cart->product->out_price;
            $indirim = $cart->product->discount;
            $adet = $cart->quantity;
            if ($indirim > 0) {
              $toplam = (($birim_fiyat * $adet) * $indirim) / 100;
            }else{
              $toplam = $birim_fiyat * $adet;
            }
           ?>
          <tr>
            <td>{{$cart->product->product_number.' '.$cart->product->brand->name}}</td>
            <td>{{$adet}}</td>
            <td>Gözlük Mekanı</td>
            <td>{{$birim_fiyat}}</td>
            <td>{{$indirim}}</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>{{$toplam}}</td>
            <td>0.00</td>
            <td>{{$toplam}}</td>
          </tr>
          <?php 
            $genel_toplam += $toplam;
           ?>
          @endforeach
          <tr>
            <td colspan="3">Kargo Ücret - Yurtiçi</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="form-group text-center">
      <h4>ÖDEME VE TESLİM ŞARTLARI</h4>
    </div>
    <div class="form-group">
      <label>Kargo hariç toplam ürün bedeli : </label>
      <span>{{$genel_toplam}} TL </span>
    </div>
    <div class="form-group">
      <label>Kargo Ücreti : </label>
      <span>0.00 TL </span>
    </div>
    <div class="form-group">
      <label>Kargo Dahil Toplam Bedeli : </label>
      <span>{{$genel_toplam}} TL </span>
    </div>
    <div class="form-group">
      <label>Ödeme Şekli ve Planı : </label>
      <span> kredi kartı le 1 ay {{$genel_toplam}} TL aylık ödeme / KDV Dahil {{$genel_toplam}} TL </span>
    </div>
    <div class="form-group">
      <label>Alınan Vade Farkı : </label>
      <span>0.00 TL </span>
    </div>
    <div class="form-group">
      <label>Vade Farkı hesabında kullanılan faiz oranı : </label>
      <span>%0.00 TL </span>
    </div>
    <div class="form-group">
      <label>Teslim Şartları ürün sayfasında belirtildiği şekilde uygulanacaktır</label>
    </div>
    <div class="form-group text-center">
      <label>Teslim Şartları</label>
    </div>
    <div class="form-group">
      <span> Ürün/Ürünler'in sevkiyatına Alıcı tarafından mal veya hizmetin sipariş edilmesini takiben 1 iş günü içinde başlanacaktır. </span>
    </div>
    <div class="form-group">
      <span>Teslimat Adresi :</span>
      <span>{{$adres->adres}}</span>
    </div>
    <div class="form-group">
      <span>Teslim Edilecek Kişi :</span>
      <span>{{Auth::user()->name.' '.Auth::user()->surname}}</span>
    </div>
    <div class="form-group text-center">
      <h4>MALIN TESLİMİ VE TESLİM ŞEKLİ</h4>
    </div>
    <div class="form-group">
      <span>
        Sözleşme Alıcı tarafından elektronik ortamda onaylanmakla yürürlüğe girmiş olup, Alıcı'nın Satıcı'dan satın almış olduğu mal veya hizmetin Alıcı'ya teslm edilmesiyle ifa edilmiş olur. Mal, Alıcı'nın sipariş formunda ve işbu Sözleşme'de belirtmiş olduğu adrese ve beilrtilen yetkili kişi/kişilere teslim edilecektir. 
      </span>
    </div>
    <div class="form-group text-center">
      <h4>TESLİMAT MASRAFLARI VE İFASI </h4>
    </div>
    <div class="form-group">
      <span>
        Malın teslimat masrafları aksine bir hüküm yoksa Alıcı'ya aittir. Satıcı, Webstesi'nde teslimat ücretnin kendisince karşılanacağını beyan etmişse teslimat masrafları Satıcı'ya ait olacaktır.
      </span>
        <br>
        <br>
      <span>
        Malın teslimatı; ödemenin gerçekleşmesinden sonra taahhüt edilen sürede yapılır. Satıcı, sipariş konusu mal veya hizmetin ediminin yerine getirilmesinin imkansızlaştığı haller saklı kalmak kaydıyla, mal veya hizmet, Alıcı tarafından mal veya hizmetin sipariş edilmesinden itibaren 30 (otuz) gün içinde teslim eder.
      </span>
        <br>
        <br>
      <span>
        Herhangi bir nedenle Alıcı tarafından mal veya hizmetin bedel ödenmez veya yapılan ödeme banka kayıtlarında iptal edilir ise, Satıcı mal veya hizmetin teslim yükümlülüğünden kurtulmuş kabul edilir.
      </span>
        <br>
        <br>
      <span>
        Malın Satıcı tarafından kargoya verilmesinden sonra ve fakat Alıcı tarafından teslim alınmasından önce Alıcı tarafından yapılan sipariş iptallerinde kargo bedelinden Alıcı sorumludur.
      </span>
        <br>
        <br>
      <span>
        Sipariş konusu mal ya da hizmet ediminin yerine getirilmesinin imkansızlaştığı hallerde Satıcı bu durumu öğrendiği tarihten itibaren 3 (üç) gün içinde Alıcı'yı bilgilendirecek ve varsa teslimat masrafları da dâhil olmak üzere tahsil edilen tüm ödemeler bildirim tarihinden itibaren en geç 14 (on dört) gün içinde iade edecektir. 
      </span>
    </div>
    <div class="form-group text-center">
      <h4>ALICI'NIN BEYAN VE TAAHHÜTLERİ</h4>
    </div>
    <div class="form-group">
      <span>
        Alıcı, Websitesi'nde yer alan Sözleşme konusu mal veya hizmetin temel nitelikler, satış fyatı ve ödeme şekli ile teslimat ve kargo bedeline ilişkin olarak Satıcı tarafından yüklenen ön bilgileri okuyup bilgi sahibi olduğunu ve elektronik ortamda gerekli teyidi verdiğini beyan eder.
      </span>
        <br>
        <br>
      <span>
        Alıcılar, tüketci sıfatıyla talep ve şikayetlerin yukarıda yer alan Satıcı iletişim bilgilerini kullanarak ve/veya Websitesi'nde yer alan Hesabım>Ürün Sorularım üzerinden ulaştırabilirler.
      </span>
        <br>
        <br>
      <span>
        Alıcı, işbu Sözleşme'yi ve Ön Bilgilendirme Formu'nu elektronik ortamda teyit etmekle, mesafeli sözleşmelerin akdinden önce Satıcı tarafından Alıcı'ya verilmesi gereken adres, sipariş verilen mal veya hizmete ait temel özellikler, mal veya hizmetin vergiler dahil fiyatı, ödeme ve teslimat ile teslimat fiyatı bilgilerini de doğru ve eksiksiz olarak edindiğini teyit etmiş olur. Alıcı'nın, teslim edildiği esnada tahrip olmuş, kırık, ambalajı yırtılmış vb. hasarlı ve ayıplı olduğu açıkça belli olan Sözleşme konusu mal veya hizmet kargo şirketinden teslm alması halinde sorumluluk tamamen kendisine aittir.
      </span>
        <br>
        <br>
      <span>
        Mal veya hizmetin tesliminden sonra Alıcı'ya ait kredi kartının Alıcı'nın kusurundan kaynaklanmayan bir şekilde yetkisiz kişilerce haksız veya hukuka aykırı olarak kullanılması neden ile ilgili banka veya finans kuruluşunun mal veya hizmetin bedelini Satıcı'ya ödememesi halinde, Alıcı kendisine teslm edilmiş olması kaydıyla mal veya hizmet 3 (üç) gün içinde Satıcı'ya iade etmekle yükümlüdür. Bu halde teslimat giderler Alıcı'ya aittir.
      </span>
    </div>
    <div class="form-group text-center">
      <h4>SATICI'NIN BEYAN VE TAAHHÜTLERİ</h4>
    </div>
    <div class="form-group">
      <span>
        Satıcı, Sözleşme konusu mal veya hizmetin tüketci mevzuatına uygun olarak, sağlam, eksiksiz, siparişte belirtilen niteliklere uygun ve varsa garanti belgeleri ve kullanım kılavuzları ile Alıcı'ya teslim edilmesinden sorumludur.
      </span>
        <br>
        <br>
      <span>
        Satıcı, mücbir sebepler veya nakliyeyi engelleyen olağanüstü durumlar nedeni ile sözleşme konusu mal veya hizmet süresi içinde teslim edemez ise, durumu öğrendiği tarihten itibaren 3 (üç) gün içinde Alıcı'ya bildirmekle yükümlüdür.
      </span>
        <br>
        <br>
      <span>
        Sözleşme konusu mal veya hizmet, Alıcı'dan başka bir kişiye teslim edilecek ise, teslim edilecek kişinin teslmatı kabul etmemesinden Satıcı sorumlu tutulamaz.
      </span>
    </div>
    <div class="form-group text-center">
      <h4>CAYMA HAKKI</h4>
    </div>
    <div class="form-group">
      <span>
        Alıcı, hiçbir hukuk ve ceza sorumluluk üstlenmeksizin ve hiçbir gerekçe göstermekszn, mal satışına ilişkin işlemlerde teslimat tarihinden itibaren, hizmet satışına ilişkin işlemlerde satın alma tarihinden itibaren 14 (on dört) gün içerisinde cayma hakkını kullanabilir. Alıcı, malın teslimine kadar olan süre içinde de cayma hakkını kullanabilir.
      </span><br>
        <br><span>
          Alıcı, cayma hakkını gozlukmekanı.com’a giriş yaparak gozlukmekanı.com'da yer alan Hesabım>Siparişlerim üzerinden "İade Et" bağlantısına tıklayarak kullanabilir. Alıcı'nın ilgili sayfada yer alan iade talep formunu doldurup satıcının iade adres bilgilerini alarak, cayma hakkını kullandığı tarihten itibaren 10 (on) gün içinde malı geri göndermesi gerekmektedir. Mal ile beraber faturasının, malın kutusunun, ambalajının, varsa standart aksesuarları, mal ile birlikte hediye edilen diğer ürünlerin de eksiksiz ve hasarsız olarak iade edilmesi gerekmektedir. Alıcı, cayma süresi içinde malı, işleyişine, teknik özelliklerine ve kullanım talimatlarına uygun bir şekilde kullandığı takdirde meydana gelen değişiklik ve bozulmalardan sorumlu değildir.
        </span>
        <br>
        <br>
        <span>
          Alıcı iade edeceği malı Satıcı’ya Ön Bilgilendirme Formu'nda belirtilen Satıcı’nın iade için öngördüğü anlaşmalı kargo şirket le gönderdiği sürece, iade kargo bedel Satıcı’ya aittir. Alıcı’nın iade edeceği malı Ön Bilglendirme Formu'nda belirtilen Satıcı’nın anlaşmalı kargo şirket dışında bir kargo şirket le göndermesi halinde, iade kargo bedeli ve malın kargo sürecinde uğrayacağı hasardan Satıcı sorumlu değildir.  
        </span>
        <br>
        <br>
        <span>
          Alıcı'nın cayma hakkını kullanmasından itibaren 14 (on dört) gün içerisinde (malın Satıcı’nın iade için belirttiği taşıyıcı aracılığıyla ger gönderilmesi kaydıyla), Alıcı'nın ilgili mal veya hizmete ilişkin Satıcı veya Aracı Hizmet Sağlayıcı’ya yaptığı tüm ödemeler Alıcı'ya satın alırken kullandığı ödeme aracına uygun bir şekilde ve tüketiciye herhangi bir masraf veya yükümlülük getirmeden ve tek seferde iade edilecektr.  
        </span>
    </div>
    <div class="form-group text-center">
      <h4>CAYMA HAKKININ KULLANILAMAYACAĞI HALLER</h4>
    </div>
    <div class="form-group">
      <span>
        Alıcı, hiçbir hukuk ve ceza sorumluluk üstlenmeksizin ve hiçbir gerekçe göstermekszn, mal satışına ilişkin işlemlerde teslimat tarihinden itibaren, hizmet satışına ilişkin işlemlerde satın alma tarihinden itibaren 14 (on dört) gün içerisinde cayma hakkını kullanabilir. Alıcı, malın teslimine kadar olan süre içinde de cayma hakkını kullanabilir.
      </span>
      <br>
      <br>
      <span>
        Mevzuat uyarınca Alıcı aşağıdaki hallerde cayma hakkını kullanamaz: Fiyatı finansal piyasalardaki dalgalanmalara bağlı olarak değişen ve Satıcı’nın kontrolünde olmayan mal veya hizmetlere ilişkin sözleşmelerde (örn. ziynet, altın ve gümüş kategorisindeki ürünler); Alıcı'nın istekler veya açıkça onun kişisel ihtyaçları doğrultusunda hazırlanan, niteliği itibariyle geri gönderilmeye elverişli olmayan ve çabuk bozulma tehlikesi olan veya son kullanma tarihi geçme ihtimali olan malların teslimine ilişkin sözleşmelerde; 
      </span>
      <br>
      <br>
      <span>
        Tesliminden sonra ambalaj, bant, mühür, paket gibi koruyucu unsurları açılmış olan mallardan; adesi sağlık ve hijyen açısından uygun olmayanların teslimine ilişkin sözleşmelerde;  
      </span>
      <br>
      <br>
      <span>
        Tesliminden sonra başka ürünlerle karışan ve doğası gereği ayrıştırılması mümkün olmayan mallara ilişkin sözleşmelerde; Alıcı tarafından ambalaj, bant, mühür, paket gibi koruyucu unsurları açılmış olması şartıyla maddi ortamda sunulan kitap, ses veya görüntü kayıtlarına, yazılım programlarına ve bilgisayar sarf malzemelerine ilişkin sözleşmelerde; Abonelik sözleşmesi kapsamında sağlananlar dışında gazete, dergi gibi süreli yayınların teslimine ilişkin sözleşmelerde; Belirli bir tarihte veya dönemde yapılması gereken, konaklama, eşya taşıma, araba kiralama, yiyecek-içecek tedarik ve eğlence veya dinlenme amacıyla yapılan boş zamanın değerlendrilmesine ilişkin sözleşmelerde; Bahis ve piyangoya ilişkin hizmetlerin ifasına ilişkin sözleşmelerde;  
      </span>
      <br>
      <br>
      <span>
        Cayma hakkı süresi sona ermeden önce, tüketicinin onayı ile ifasına başlanan hizmetlere ilişkin sözleşmelerde; ve Elektronik ortamda anında ifa edilen hizmetler ile tüketiciye anında teslim edilen gayri maddi mallara ilişkin sözleşmelerde (hediye kartı, hediye çeki, para yerine geçen kupon ve benzer). 
      </span>
      <br>
      <br>
      <span>
        Mesafeli Sözleşmeler Yönetmeliği’nin kapsamı dışında bırakılmış olan mal veya hizmetler (Satıcı’nın düzenli teslimatları ile Alıcı'nın meskenine teslim edilen gıda maddelerinin, içeceklerin ya da diğer günlük tüketim maddeler ile seyahat, konaklama, lokantacılık, eğlence sektörü gibi alanlarda hizmetler) bakımından cayma hakkı kullanılamayacaktır. Tatil kategorisinde satışa sunulan bu tür mal ve hizmetlerin iptal ve iade şartları her Satıcı’nın uygulama ve kurallarına tabidir.  
      </span>
    </div>
    <div class="form-group text-center">
      <h4>UYUŞMAZLIKLARIN ÇÖZÜMÜ</h4>
    </div>
    <div class="form-group">
      <span>
        6502 sayılı Tüketicinin Korunması Hakkında Kanun ve Mesafeli Sözleşmeler Yönetmeliği kapsamında satılan mal veya hzimete ilişkin sorumluluk bizzat Satıcı'ya aittir. Bununla birlikte Alıcılar, satın aldıkları mal ve hizmetlerle ilgili şikâyetlerin Satıcılar'a doğrudan veya Gözlük Mekanı üzerinden iletebilirler. Şikayetin Gözlük Mekanı'na iletilmesi halinde Gözlük Mekanı sorunun çözülmesi için mümkün olan tüm desteği sağlayacaktır 
      </span>
      <br>
      <br>
      <span>
        İş bu Mesafeli Satış Sözleşme ile ilgili çıkacak ihtilaflarda; her yıl Gümrük ve Ticaret Bakanlığı tarafından ilan edilen değere kadar Alıcı'nın yerleşim yerindeki ürünü satın aldığı veya ikametgâhının bulunduğu yerdeki İl veya İlçe Tüketci Sorunları Hakem Heyetler, söz konusu değerin üzerindeki ihtilaflarda ise Tüketci Mahkemeleri yetkilidir. 
      </span>
    </div>
    <div class="form-group text-center">
      <h4>MAL/HİZMETİN FİYATI</h4>
    </div>
    <div class="form-group">
      <span>
        Malın peşin veya vadeli satış fiyatı, ispariş formunda yer almakla birlikte, sipariş sonu gönderilen bilgilendirme e-postası ve ürün ile birlikte müşteriye gönderilen fatura içeriğinde mevcut olan fiyattır. Satıcı veya Gözlük Mekanı tarafından yapılan indirimler, kuponlar, kargo ücreti ve sair uygulamalar satış fiyatına yansıtılır. 
      </span>
    </div>
    <div class="form-group text-center">
      <h4>TEMERRÜT HALİ VE HUKUKİ SONUÇLARI </h4>
    </div>
    <div class="form-group">
      <span>
        Alıcı'nın, kredi kartı ile yapmış olduğu işlemlerde temerrüde düşmesi halinde kart sahibi bankanın kendisi ile yapmış olduğu kredi kartı sözleşmesi çerçevesinde faiz ödeyecek ve bankaya karşı sorumlu olacaktır. Bu durumda ilgili banka hukuk yollara başvurabilir; doğacak masrafları ve vekâlet ücretini Alıcı'dan talep edebilir ve her koşulda Alıcı'nın borcundan dolayı temerrüde düşmesi halinde, Alıcı'nın borcu gecikmeli ifasından dolayı Satıcı'nın uğradığı zarar ve ziyandan Alıcı sorumlu olacaktır.
      </span>
    </div>
    <div class="form-group text-center">
      <h4>BİLDİRİMLER VE DELİL SÖZLEŞMESİ</h4>
    </div>
    <div class="form-group">
      <span>
        İşbu Sözleşme tahtında taraflar arasında yapılacak her türlü yazışma, mevzuatta sayılan zorunlu haller dışında, Websitesi'nde yer alan Hesabım>Ürün Sorularım veya elektronik posta aracılığıyla yapılacaktır. Alıcı, işbu Sözleşme'den doğabilecek ihtilaflarda Satıcı'nın ve Gözlük Mekanı'nın resmi defter ve ticari kayıtlarıyla, kendi veritabanında, sunucularında tuttuğu elektronk bilgilerin ve bilgisayar kayıtlarının, bağlayıcı, kesin ve münhasır delil teşkil edeceğini, bu maddenin Hukuki Muhakemeler Kanunu'nun 193. maddes anlamında delil sözleşmesi niteliğinde olduğunu kabul, beyan ve taahhüt eder. 
      </span>
    </div>
    <div class="form-group text-center">
      <h4>YÜRÜRLÜK</h4>
    </div>
    <div class="form-group">
      <span>
        14 (on dört) maddeden ibaret bu Sözleşme, taraflarca okunarak, {{\Carbon\Carbon::now()->format('d/m/Y')}} tarihinde, Alıcı tarafından elektronik ortamda onaylanmak suretiyle akdedilmiş ve yürürlüğe girmiştir.
      </span>
    </div>
    <div class="form-group">
      <div class="pull-left">
        SATICI <br>
        <strong>Gözlük Mekanı</strong>
      </div>
      <div class="pull-right">
        ALICI <br>
        <strong>{{Auth::user()->name.' '.Auth::user()->surname}}</strong>
      </div>
      <br>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
  </div>
</form>
</div>
