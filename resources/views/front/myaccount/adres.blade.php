@extends('front.master')

@section('content')

<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>Adreslerim</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col2-right-layout">
  <div class="main container">
    <div class="row">
      <div class="col-main col-sm-9 col-xs-12">
        <div class="my-account">
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
            @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>{{Session::get('success')}}</strong>
                </div>
            @endif
          <div class="page-title" style="margin-bottom:15px;">
            <h2>Adreslerim</h2>
          </div>
            <div class="jumbotron col-sm-3" style="padding:60px 8px 60px 8px;margin:5px;border:1px solid #702376;">
              <a href="#" class="createAdres" data-toggle="modal" data-target="#createAdres">
                <i class="fa fa-plus"></i> Yeni Teslimat Adresi Ekle</a>
            </div>
          @if($adress)
            @foreach($adress as $adres)
                <div class="jumbotron col-sm-4 pull-right" style="padding:11px 8px 11px 8px;margin:5px;border:1px solid #702376;height:250px;">
                  <span> <strong>{{$adres->name}}</strong> </span><br><br>
                  @if($adres->company)
                  <span>{{$adres->company}}</span><br>
                  <span>{{$adres->vergi_no}}</span><br>
                  @endif
                  <span>{{$adres->zip_code}}</span><br><br>
                  <span>{{$adres->adres}}</span><br><br>
                  <span>{{$adres->city->name}}</span><br>
                  <div class="text-center">
                    <a onclick="adresSil({{$adres->id}});" class="btn btn-danger">Sil</a>
                    <a href="#" class="btn btn-purple editAdres" id="{{$adres->id}}" data-toggle="modal" data-target="#editAdres">Düzenle</a>
                  </div>
                </div>
              @endforeach
          @endif
        </div>
      </div>
      <aside class="right sidebar col-sm-3 col-xs-12">
        <div class="sidebar-account block">
          <div class="sidebar-bar-title">
            <h3>Hesabım</h3>
          </div>
          <div class="block-content">
            <ul>
              <li><a href="/myaccount">Hesap Ayarlarım</a></li>
              <li><a href="#">Siparişlerim</a></li>
              <li><a href="/myaccount/wishlist">Beğendiklerim</a></li>
              <li class="last current"><a>Adreslerim</a></li>
            </ul>
          </div>
        </div>
      </aside>
    </div>
  </div>
</section>
<!-- service section -->
<!-- Modal -->
<div class="modal fade" id="createAdres" role="dialog" aria-labelledby="myModalLabel">

</div>
<!-- Modal -->
<div class="modal fade" id="editAdres" role="dialog" aria-labelledby="myModalLabel">

</div>
</div>
@endsection
@section('jscode')
<script type="text/javascript">
function adresSil(id) {
  var r = confirm('Silmek İstediğinize Emin Misiniz ?');
  if (r == true) {
    location.href = '/myaccount/adres/delete/'+id;
  }

}

  $('.createAdres').click(function(){
    $.ajax({
        url: '/myaccount/adres/createAdres',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {},
        success: function(data){
            document.getElementById('createAdres').innerHTML = data;
            $('.tip').click(function(){
              var tip = $(this).val();
              console.log(tip);
              if (tip == 1) {
                $('#firma').removeAttr('style');
              }else{
                $('#firma').attr('style','display:none');
              }
            });
        },
        error: function(jqXHR, textStatus, err){}
    });
  });

  $('.editAdres').click(function(){
    var id = $(this).attr('id');
    console.log(id);
    $.ajax({
        url: '/myaccount/adres/editAdres',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id:id},
        success: function(data){
            document.getElementById('editAdres').innerHTML=data;
            var check = document.querySelector('input[type="radio"]:checked').value;
            if (check == 1) {
              $('#firma2').removeAttr('style');
            }else {
              $('#firma2').attr('style','display:none');
            }
            $('.tip2').click(function(){
              var tip = $(this).val();
              console.log(tip);
              if (tip == 1) {
                $('#firma2').removeAttr('style');
              }else{
                $('#firma2').attr('style','display:none');
              }
            });
        },
        error: function(jqXHR, textStatus, err){}
    });
  });

</script>
@endsection
