<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form action="#" method="post">
      {{csrf_field()}}
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">ÖN BİLGİLENDİRME</h4>
  </div>
  <div class="modal-body">
    <input type="hidden" name="customer_id" value="{{Auth::user()->id}}">
    <div class="form-group text-center">
      <h4>Ön Bilgilendirme Formu</h4>
    </div>
    <div class="form-group">
      <label>Satıcı Bilgileri</label>
    </div>
    <div class="form-group">
      <label>Satıcı İsim/Ünvan : </label>
      <span>Gözlük Mekanı</span><br>
      <label>Satıcı'nın Açık Adresi : </label>
      <span>Dr. Sadık Ahmet Cad. Deniz Köşler Mah. Gülden Sok No:3/8 Avcılar / İSTANBUL</span><br>
      <label>Satıcı’nın Telefonu : </label>
      <span>0212 123 45 56</span><br>
      <label>Satıcı Mersis No : </label>
      <span> </span><br>
      <label>Satıcı E-Posta Adres : </label>
      <span>info@gozlukmekani.com</span><br>
      <label>Alıcı'nın İade Halnde Malı Satıcı'ya Göndereceği Kargo Şirketi : </label>
      <span>Yurtiçi</span><br>
    </div>
    <div class="form-group text-center">
      <h4>SÖZLEŞME KONUSU MAL VE HİZMETİN TEMEL NİTELİKLERİ VE FİYATI (KDV DAHİL)</h4>
    </div>
    <div class="form-group">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td>Ürün Kodu Ve Adı</td>
            <td>Adet</td>
            <td>Satıcı Unvanı</td>
            <td>Birim Fyatı</td>
            <td>Birim İndirimi</td>
            <td>Kupon</td>
            <td>Puan</td>
            <td>Toplam Satış Tutarı</td>
            <td>Vade Farkı</td>
            <td>KDV Dahil Toplam Tutar</td>
          </tr>
        </thead>
        <?php 
            $genel_toplam = 0;
         ?>
        <tbody>
          @foreach($carts as $key => $cart)
          <?php 
            $birim_fiyat = $cart->product->out_price;
            $indirim = $cart->product->discount;
            $adet = $cart->quantity;
            if ($indirim > 0) {
              $toplam = (($birim_fiyat * $adet) * $indirim) / 100;
            }else{
              $toplam = $birim_fiyat * $adet;
            }
           ?>
          <tr>
            <td>{{$cart->product->product_number.' '.$cart->product->brand->name}}</td>
            <td>{{$adet}}</td>
            <td>Gözlük Mekanı</td>
            <td>{{$birim_fiyat}}</td>
            <td>{{$indirim}}</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>{{$toplam}}</td>
            <td>0.00</td>
            <td>{{$toplam}}</td>
          </tr>
          <?php 
            $genel_toplam += $toplam;
           ?>
          @endforeach
          <tr>
            <td colspan="3">Kargo Ücret - Yurtiçi</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="form-group text-center">
      <h4>ÖDEME VE TESLİM ŞARTLARI</h4>
    </div>
    <div class="form-group">
      <label>Kargo hariç toplam ürün bedeli : </label>
      <span>{{$genel_toplam}} TL </span>
    </div>
    <div class="form-group">
      <label>Kargo Ücreti : </label>
      <span>0.00 TL </span>
    </div>
    <div class="form-group">
      <label>Kargo Dahil Toplam Bedeli : </label>
      <span>{{$genel_toplam}} TL </span>
    </div>
    <div class="form-group">
      <label>Ödeme Şekli ve Planı : </label>
      <span> kredi kartı le 1 ay {{$genel_toplam}} TL aylık ödeme / KDV Dahil {{$genel_toplam}} TL </span>
    </div>
    <div class="form-group">
      <label>Alınan Vade Farkı : </label>
      <span>0.00 TL </span>
    </div>
    <div class="form-group">
      <label>Vade Farkı hesabında kullanılan faiz oranı : </label>
      <span>%0.00 TL </span>
    </div>
    <div class="form-group">
      <label>Teslim Şartları ürün sayfasında belirtildiği şekilde uygulanacaktır</label>
    </div>
    <div class="form-group text-center">
      <label>Teslim Şartları</label>
    </div>
    <div class="form-group">
      <span> Ürün/Ürünler'in sevkiyatına Alıcı tarafından mal veya hizmetin sipariş edilmesini takiben 1 iş günü içinde başlanacaktır. </span>
    </div>
    <div class="form-group text-center">
      <h4>CAYMA HAKKI</h4>
    </div>
    <div class="form-group">
      <span>
        Alıcı, hiçbir hukuk ve ceza sorumluluk üstlenmeksizin ve hiçbir gerekçe göstermekszn, mal satışına ilişkin işlemlerde teslimat tarihinden itibaren, hizmet satışına ilişkin işlemlerde satın alma tarihinden itibaren 14 (on dört) gün içerisinde cayma hakkını kullanabilir. Alıcı, malın teslimine kadar olan süre içinde de cayma hakkını kullanabilir.
      </span><br>
        <br><span>
          Alıcı, cayma hakkını n11.com'da yer alan Hesabım>Mevcut Sipariş>Siparişlerim üzerinden "İade Et" bağlantısına tıklayarak kullanabilir. Alıcı'nın ilgili sayfada yer alan iade talep formunu doldurup satıcının iade adres bilgilerini alarak, cayma hakkını kullandığı tarihten itibaren 10 (on) gün içinde malı geri göndermesi gerekmektedir. Mal le beraber faturasının, malın kutusunun, ambalajının, varsa standart aksesuarları, mal le birlikte hediye edilen diğer ürünlerin de eksiksiz ve hasarsız olarak iade edilmesi gerekmektedir. Alıcı, cayma süresi içinde malı, işleyişine, teknik özelliklerine ve kullanım talimatlarına uygun bir şekilde kullanmalıdır.
        </span>
        <br>
        <br>
        <span>
          Alıcı iade edeceği malı Satıcı’ya Ön Bilgilendirme Formu'nda belirtilen Satıcı’nın iade için öngördüğü anlaşmalı kargo şirket le gönderdiği sürece, iade kargo bedel Satıcı’ya aittir. Alıcı’nın iade edeceği malı Ön Bilglendirme Formu'nda belirtilen Satıcı’nın anlaşmalı kargo şirket dışında bir kargo şirket le göndermesi halinde, iade kargo bedeli ve malın kargo sürecinde uğrayacağı hasardan Satıcı sorumlu değildir.  
        </span>
        <br>
        <br>
        <span>
          Alıcı'nın cayma hakkını kullanmasından itibaren 14 (on dört) gün içerisinde (malın Satıcı’nın iade için belirttiği taşıyıcı aracılığıyla ger gönderilmesi kaydıyla), Alıcı'nın ilgili mal veya hizmete ilişkin Satıcı veya Aracı Hizmet Sağlayıcı’ya yaptığı tüm ödemeler Alıcı'ya satın alırken kullandığı ödeme aracına uygun bir şekilde ve tüketiciye herhangi bir masraf veya yükümlülük getirmeden ve tek seferde iade edilecektr.  
        </span>
        <br>
        <br>
        <span>
          Malın iade alınamaması için haklı sebeplerin varlığı halinde Satıcı, sözleşmedeki ifa süresi dolmadan Alıcı’ya eşit kalite ve fiyatta mal/hizmet tedarik edebilir. Satıcı mal/hizmetin ifasının imkânsızlaştığını düşünüyorsa, bu durumu öğrendiği tarihten itibaren 3 (üç) gün içinde Alıcı’ya bildirir. Bu durumda Satıcı, ödenen bedel ve varsa belgeler 14 (on dört) gün içinde Alıcı’ya iade eder. Alıcı iade edeceği mal/hizmet ön bilgilendirme formunda belirtilen Satıcı’nın anlaşmalı kargo şirket le Satıcı’ya gönderdiği sürece, iade kargo bedel Satıcı’ya aittir. Alıcı’nın iade edeceği malı ön bilgilendirme formunda belirtilen Satıcı’nın iade için ön gördüğü anlaşmalı kargo şirket dışında bir kargo şirket le göndermesi halinde, iade kargo bedel ve malın kargo sürecinde uğrayacağı hasardan Satıcı sorumlu değildir. 
        </span>
    </div>
    <div class="form-group text-center">
      <h4>CAYMA HAKKININ KULLANILAMAYACAĞI HALLER</h4>
    </div>
    <div class="form-group">
      <span>
        Alıcı, hiçbir hukuk ve ceza sorumluluk üstlenmeksizin ve hiçbir gerekçe göstermekszn, mal satışına ilişkin işlemlerde teslimat tarihinden itibaren, hizmet satışına ilişkin işlemlerde satın alma tarihinden itibaren 14 (on dört) gün içerisinde cayma hakkını kullanabilir. Alıcı, malın teslimine kadar olan süre içinde de cayma hakkını kullanabilir.
      </span>
      <br>
      <br>
      <span>
        Mevzuat uyarınca Alıcı aşağıdaki hallerde cayma hakkını kullanamaz: Fiyatı finansal piyasalardaki dalgalanmalara bağlı olarak değişen ve Satıcı’nın kontrolünde olmayan mal veya hizmetlere ilişkin sözleşmelerde (örn. ziynet, altın ve gümüş kategorisindeki ürünler); Alıcı'nın istekler veya açıkça onun kişisel ihtyaçları doğrultusunda hazırlanan, niteliği itibariyle geri gönderilmeye elverişli olmayan ve çabuk bozulma tehlikesi olan veya son kullanma tarihi geçme ihtimali olan malların teslimine ilişkin sözleşmelerde; 
      </span>
      <br>
      <br>
      <span>
        Tesliminden sonra ambalaj, bant, mühür, paket gibi koruyucu unsurları açılmış olan mallardan; adesi sağlık ve hijyen açısından uygun olmayanların teslimine ilişkin sözleşmelerde;  
      </span>
      <br>
      <br>
      <span>
        Tesliminden sonra başka ürünlerle karışan ve doğası gereği ayrıştırılması mümkün olmayan mallara ilişkin sözleşmelerde; Alıcı tarafından ambalaj, bant, mühür, paket gibi koruyucu unsurları açılmış olması şartıyla maddi ortamda sunulan kitap, ses veya görüntü kayıtlarına, yazılım programlarına ve bilgisayar sarf malzemelerine ilişkin sözleşmelerde; Abonelik sözleşmesi kapsamında sağlananlar dışında gazete, dergi gibi süreli yayınların teslimine ilişkin sözleşmelerde; Belirli bir tarihte veya dönemde yapılması gereken, konaklama, eşya taşıma, araba kiralama, yiyecek-içecek tedarik ve eğlence veya dinlenme amacıyla yapılan boş zamanın değerlendrilmesine ilişkin sözleşmelerde; Bahis ve piyangoya ilişkin hizmetlerin ifasına ilişkin sözleşmelerde;  
      </span>
      <br>
      <br>
      <span>
        Cayma hakkı süresi sona ermeden önce, tüketicinin onayı ile ifasına başlanan hizmetlere ilişkin sözleşmelerde; ve Elektronik ortamda anında ifa edilen hizmetler ile tüketiciye anında teslim edilen gayri maddi mallara ilişkin sözleşmelerde (hediye kartı, hediye çeki, para yerine geçen kupon ve benzer). 
      </span>
      <br>
      <br>
      <span>
        Mesafeli Sözleşmeler Yönetmeliği’nin kapsamı dışında bırakılmış olan mal veya hizmetler (Satıcı’nın düzenli teslimatları ile Alıcı'nın meskenine teslim edilen gıda maddelerinin, içeceklerin ya da diğer günlük tüketim maddeler ile seyahat, konaklama, lokantacılık, eğlence sektörü gibi alanlarda hizmetler) bakımından cayma hakkı kullanılamayacaktır. Tatil kategorisinde satışa sunulan bu tür mal ve hizmetlerin iptal ve iade şartları her Satıcı’nın uygulama ve kurallarına tabidir.  
      </span>
    </div>
    <div class="form-group text-center">
      <h4>UYUŞMAZLIKLARIN ÇÖZÜMÜ</h4>
    </div>
    <div class="form-group">
      <span>
        6502 sayılı Tüketicinin Korunması Hakkında Kanun ve Mesafeli Sözleşmeler Yönetmeliği kapsamında satılan mal veya hzimete ilişkin sorumluluk bizzat Satıcı'ya aittir. Bununla birlikte Alıcılar, satın aldıkları mal ve hizmetlerle ilgili şikâyetlerin Satıcılar'a doğrudan veya Gözlük Mekanı üzerinden iletebilirler. Şikayetin Gözlük Mekanı'na iletilmesi halinde Gözlük Mekanı sorunun çözülmesi için mümkün olan tüm desteği sağlayacaktır 
      </span>
      <br>
      <br>
      <span>
        İş bu Sözleşme ile ilgili çıkacak ihtilaflarda; her yıl Gümrük ve Ticaret Bakanlığı tarafından ilan edilen değere kadar Alıcı'nın yerleşim yerindeki ürünü satın aldığı veya ikametgâhının bulunduğu yerdeki İl veya İlçe Tüketci Sorunları Hakem Heyetler, söz konusu değerin üzerindeki ihtilaflarda ise Tüketci Mahkemeleri yetkilidir. 
      </span>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
  </div>
</form>
</div>
