<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form action="/myaccount/adres/update" method="post">
      {{csrf_field()}}
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Adresi Güncelle</h4>
  </div>
  <div class="modal-body">
    <input type="hidden" name="id" value="{{$adres->id}}">
    <div class="form-group">
      <label>Adres Tipi</label><br>
      <label style="margin:5px;">
        <input type="radio" class="tip2" name="address_type" value="1" <?php echo ($adres->address_type == 1) ? 'checked':'' ?> required> Kurumsal
      </label>
      <label style="margin:5px;">
        <input type="radio" class="tip2" name="address_type" value="2"  <?php echo ($adres->address_type == 2) ? 'checked':'' ?>> Bireysel
      </label>
    </div>
    <div class="form-group">
      <label>Adres Adı</label>
      <input type="text" class="form-control input" name="name" value="{{$adres->name}}" required>
    </div>
    <div class="form-group" id="firma2" style="display:none;">
      <div class="form-group">
        <label>Firma</label>
        <input type="text" class="form-control input" name="company" value="{{$adres->company}}">
      </div>
      <div class="form-group">
        <label>Vergi No</label>
        <input type="text" class="form-control input" name="vergi_no" value="{{$adres->vergi_no}}">
      </div>
    </div>
    <div class="form-group">
      <label>İl</label>
      <select class="select2 form-control" name="city_id" data-live-search="true">
        @foreach($citys as $city)
          @if($city->id == $adres->city_id)
            <option value="{{$city->id}}" selected>{{$city->name}}</option>
          @else
            <option value="{{$city->id}}">{{$city->name}}</option>
          @endif
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Posta Kodu</label>
      <input type="number" class="form-control input" name="zip_code" value="{{$adres->zip_code}}">
    </div>
    <div class="form-group">
      <label>Adres</label>
      <textarea style="resize:none;" class="form-control" name="adres" rows="8" cols="80">{{$adres->adres}}</textarea>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
    <button type="submit" class="btn btn-success">Güncelle</button>
  </div>
</form>
</div>
