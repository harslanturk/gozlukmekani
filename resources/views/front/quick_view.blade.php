@extends('front.master')
@section('content')
<div id="quick_view_popup-overlay"></div>
<div style="display: block;" id="quick_view_popup-wrap">
  <div id="quick_view_popup-outer">
    <div id="quick_view_popup-content">
      <div style="width:auto;height:auto;overflow: auto;position:relative;">
        <div class="product-view-area">
          <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
            @if($product->discount > 0)
              <div class="icon-sale-label sale-left">İndirim</div>
            @endif
            <div class="large-image">
              <a href="{{$images[0]->image}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="{{$images[0]->image}}" alt="products"> </a>
            </div>
            <div class="flexslider flexslider-thumb">
              <ul class="previews-list slides">
                @foreach($images as $image)
                <li>
                  <a href='{{$image->image}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{$image->image}}' ">
                    <img src="{{$image->image}}" alt = "Thumbnail 1"/>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>

            <!-- end: more-images -->

          </div>
          <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
            <div class="product-details-area">
              <div class="product-name">
                <h1>{{$product->product_number}}</h1>
              </div>
              <div class="price-box">
                <?php
                  $discount_price = $product->out_price - (($product->out_price * $product->discount)/100)
                 ?>
                <p class="special-price"> <span class="price-label">Special Price</span>
                  <span class="price">  {{$discount_price}}<i class="fa fa-try"></i> </span> </p>
                  @if($product->discount)
                    <p class="old-price"> <span class="price-label">Regular Price:</span>
                    <span class="price"> {{$product->out_price}} <i class="fa fa-try"></i> </span> </p>
                  @endif
              </div>
              <div class="ratings">
                <div class="rating">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                  <i class="fa fa-star-o"></i>
                </div>
                <p class="rating-links">
                  <a href="#reviews" data-toggle="tab" class="focusla">{{$comments->count()}} Yorum</a>
                </p>
                @if($product->stock >0)
                  <p class="availability in-stock pull-right">Stok Durumu: <span>Stokda Var</span></p>
                @else
                  <p class="availability out-of-stock pull-right">Stok Durumu: <span>Stokda Yok</span></p>
                @endif
              </div>
              <div class="short-description">
                <h2>Hızlı Bakış</h2>
                <p>{{$product->quick_content}}</p>
                <br><br><br><br>
              </div>
              <div class="product-variation">
                <form action="#" method="post">
                  <div class="cart-plus-minus">
                    <label for="qty">Adet:</label>
                    <div class="numbers-row">
                      <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                      <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                      <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                    </div>
                  </div>
                  @if($product->stock >0)
                    <button class="button pro-add-to-cart" title="Sepete Ekle" type="button"><span><i class="fa fa-shopping-cart"></i> Sepete Ekle</span></button>
                  @else
                    <button class="button pro-add-to-cart" title="Sepete Ekle" type="button"><span><i class="fa fa-shopping-cart"></i> Tükendi</span></button>
                  @endif
                </form>
              </div>
              <div class="product-cart-option">
                <ul>
                  <li>
                    <?php
                    if (!empty(Auth::user()->id)) {
                      $wishlist = App\Helpers\helper::getWishlist(Auth::user()->id,$product->id);
                    }else {
                      $wishlist = '';
                    }
                     ?>
                    @if($wishlist)
                      <a href="#" class="add-wishlist" id="{{$product->id}}"><i class="fa fa-heart" style="color:#702376;"></i>
                        <span style="color:#702376;">Beğeni Listemden Çıkar</span>
                      </a>
                    @elseif(empty($wishlist))
                      <a href="#"><i class="fa fa-heart"></i>
                        <span>Beğeni Listesine Ekle</span>
                      </a>
                    @else
                      <a href="#" class="add-wishlist" id="{{$product->id}}"><i class="fa fa-heart"></i>
                        <span>Beğeni Listesine Ekle</span>
                      </a>
                    @endif
                  </li>
                  <li><a href="#"><i class="fa fa-envelope"></i><span>Arkadaşına Gönder</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!--product-view-->

      </div>
    </div>
    <a style="display: inline;" id="quick_view_popup-close" href="/"><i class="fa fa-times-circle"></i></a> </div>
</div>
@endsection
@section('jscode')
<!-- flexslider js -->
<script type="text/javascript" src="/front/js/jquery.flexslider.js"></script>
<!--cloud-zoom js -->
<script type="text/javascript" src="/front/js/cloud-zoom.js"></script>
<script type="text/javascript">

$('.add-wishlist').click(function () {
    var product_id = $(this).attr('id');
    console.log(product_id);
    $.ajax({
        url: '/product/my-wishlist/add',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {product_id: product_id},
        success: function(data){

          if (data == 1) {
            $('.product-cart-option li .add-wishlist i').attr('style','color:#702376');
            $('.product-cart-option li .add-wishlist span').attr('style','color:#702376');
            $('.add-wishlist span')[0].innerHTML = 'Beğeni Listemden Çıkar';
          }else if(data == 2) {
            $('.product-cart-option li .add-wishlist i').removeAttr('style');
            $('.product-cart-option li .add-wishlist span').removeAttr('style');
            $('.add-wishlist span')[0].innerHTML = 'Beğeni Listesine Ekle';
          }
        },
        error: function(jqXHR, textStatus, err){}
    });
});
</script>
@endsection
