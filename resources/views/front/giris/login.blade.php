@extends('front.master')
@section('content')
<!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>Giriş</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
      <div class="page-content">
          <div class="account-login">
            @if(Session::has('error'))
             <div class="alert alert-danger" role="alert">
               <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
               <strong>{{Session::get('error')}}</strong>
             </div>
             @elseif(Session::has('success'))
               <div class="alert alert-success" role="alert">
                 <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                 <strong>{{Session::get('success')}}</strong>
               </div>
             @elseif(Session::has('email-error'))
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('email-error')}}</strong>
              </div>
              @elseif(Session::has('email-ok'))
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <strong>{{Session::get('email-ok')}}</strong>
                </div>
             @endif
            <form class="" action="/login" method="post">
              {{csrf_field()}}
                <div class="box-authentication">
                  <h4>Giriş Yap</h4>
                 <p class="before-login-text">Hoş Geldiniz. Hesabınıza Giriş Yapınız.</p>
                  <label for="emmail_login">Email Adresiniz<span class="required">*</span></label>
                  <input id="emmail_login" name="email" type="text" class="form-control">
                  <label for="password_login">Şifreniz<span class="required">*</span></label>
                  <input id="password_login" type="password" name="password" class="form-control" required>
                  <p class="forgot-pass"><a href="#">Şifremi Unuttum?</a></p>
                  <button class="button"><i class="fa fa-lock"></i>&nbsp; <span>Giriş</span></button>
                </div>
            </form>
              <form class="" action="/front/register" method="post">
                {{csrf_field()}}
                <div class="box-authentication">
                  <h4>Hızlı Kayıt Ol</h4><p>Yeni Hesap Oluştur</p>
                  <label for="name">Adınız<span class="required" req>*</span></label>
                  <input id="name" name="name" type="text" class="form-control" required>

                  <label for="surname">Soyadınız<span class="required">*</span></label>
                  <input id="surname" name="surname" type="text" class="form-control" required>

                  <label for="email">Email Adresiniz<span class="required">*</span></label>
                  <input id="email" type="email" name="email" class="form-control" required>

                  <label for="password">Şifreniz<span class="required">*</span></label>
                  <input id="password" name="password" type="password" class="form-control" required>

                  <button class="button"><i class="fa fa-user"></i>&nbsp; <span>Kayıt Ol</span></button>
                </div>
              </form>
      </div>
    </div>
  </div>
</section>
<!-- Main Container End -->
@endsection
