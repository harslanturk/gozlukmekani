@extends('front.master')
@section('content')
<!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>İptal, İade, Değişim</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
      <div class="page-content">
          <h2>İADE VE DEĞİŞİM SÜRECİ</h2>
          <p><b>Ürünümü Ne Kadar Sürede İade Edebilirim?</b><br>
          Siparişinizi teslim aldığınız tarihten itibaren 15 gün içerisinde iade edebilirsiniz </p>

          <br><p><b>Ürünümü Nasıl İade Edebilirim?</b></p>  
          gozlukmekani.com hesabınıza giriş yaparak > hesabım > siparişlerim bölümüne giriniz.<br>
          Karşınıza vermiş olduğunuz siparişlerin listesi geldikten sonra talep açmak istediğiniz siparişin sağında bulunan "artı (+)" işaretine tıklayınız.<br>
          Sipariş detayları ekranınıza geldikten sonra ekranın altındaki turuncu renkli "İade ve değişim talebi oluştur" butonuna tıklayınız.<br>
          Ürününüzü fatura veya irsaliye makbuzunu ekleyerek anlaşmalı olduğumuz Yurt içi Kargo ile aşağıda belirtilen adresimize ücretsiz olarak gönderiniz.<br>

          <br><p><b>İade Ücretimi Ne Zaman Geri Alabilirim?</b></p>
          Ücret iadesini talep ettiğiniz ürününüzün, tarafımıza ulaştıktan sonra inceleme birimi tarafından iade şartlarına uygunluğu kontrol edilir.<br>
          İade talebiniz faturanız veya irsaliye makbuzunuz ile birlikte onaylandıktan sonra kartınıza/hesabınıza 7 iş günü içinde transfer edilir. <br>
          İadeniz; ödemenizi gerçekleştirdiğiniz ödeme türünde yapılır. <br>
          Taksitli ödemelerinizde ise; geri ödemeler bankalar tarafından taksitlendirilerek yapılmaktadır.<br>

          <br><p><b>İademi Hangi Adrese Göndereceğim?</b></p>
          Gönderici Adresimiz;<br>
          Alıcı Adı: www.gozlukmekani.com Adres: Dr. Sadık Ahmet Cad.Denizköşkler Mah.Gülden Sok No:3/8 Avcılar / İSTANBUL<br>

          <br><p><b>İade Kargo Masraflarını Ben Mi Ödeyeceğim?</b></p>
          İade gönderilerinizin kargo masrafları Gözlük Mekanı’na aittir. <br>
          İade etmek istediğiniz ürün veya ürünleri orjinal kutusu zarar görmeyecek şekilde anlaşmalı olduğumuz Yurt içi kargo ile ücretsiz gönderebilirsiniz.<br>
          Yurt içi kargo haricinde gönderdiğiniz kargolar tarafımızca kabul edilememektedir.<br>

          <br><p><b>Hangi Ürünleri İade Edemiyorum?</b></p>
          Tek kullanımlık ürünlerimizin iadesi kabul edilmemektedir. <br>

          <p><b>Ürünümü Ne Kadar Sürede Değiştirebilirim?</b></p>
          Siparişinizi teslim aldığınız tarihten itibaren 7 gün içerisinde değiştirebilirsiniz. <br>

          <br><p><b>Ürünümü Nasıl Değiştirebilirim?</b></p>
          gozlukmekani.com hesabınıza giriş yaparak > hesabımı görüntüle > siparişlerim bölümüne giriniz.<br>
          Karşınıza vermiş olduğunuz siparişlerin listesi geldikten sonra talep açmak istediğiniz siparişin sağında bulunan "artı (+)" işaretine tıklayınız.<br>
          Sipariş detayları ekranınıza geldikten sonra ekranın altındaki turuncu renkli "İade ve değişim talebi oluştur" butonuna tıklayınız.<br>
          Ürününüzü fatura veya irsaliye makbuzunu ekleyerek anlaşmalı olduğumuz Yurt içi kargo ile aşağıda belirtilen adresimize ücretsiz olarak gönderiniz.<br>

          <br><p><b>Yeni Siparişim Ne Zaman Hazırlanmaya Başlar?</b></p>
          Değişim talep ettiğiniz ürününüzün; tarafımıza ulaştıktan sonra inceleme birimi tarafından değişim şartlarına uygunluğu kontrol edilir. <br>
          Talebiniz 7 iş günü içerisinde, faturanız veya irsaliye makbuzunuz ile birlikte onaylandıktan sonra işleme alınır. <br>

          <br><p><b>Değişmesini Istediğim Ürünleri Hangi Adrese Göndereceğim?</b></p>
          Gönderici Adresimiz;<br>
          Alıcı Adı: www.gozlukmekani.com Adres: Dr. Sadık Ahmet Cad.Denizköşkler Mah.Vildan Sok No:3/8Avcılar / İSTANBUL<br>

          <br><p><b>Değişim Ürünlerini Gönderirken Kargo Masraflarını Ben Mi Ödeyeceğim?</b></p>
          Değişim gönderilerinizin kargo masrafları gözlulmekani’na aittir.<br>
          Değişimini istediğiniz ürün veya ürünleri orjinal kutusu zarar görmeyecek şekilde anlaşmalı olduğumuz Yurt içi Kargo ile ücretsiz gönderebilirsiniz. <br>
          Yurt içi kargo haricinde gönderdiğiniz kargolar tarafımızca kabul edilememektedir. <br>

          <br><p><b>Hangi Ürünleri Değiştiremiyorum?</b></p>
          Tek kullanımlık ürünlerin değişimi kabul edilmemektedir.<br>
          Bu ürünlerin dışında gozlukmekani.com’dan aldığınız tüm ürünlerimizde yalnızca beden değişikliği yapılabilmektedir, renk yada farklı model değişikliği yapılamamaktadır. <br>
          Farklı renk yada model değişikliği talep edildiğinde ücret iadesi gerçekleştirilerek yeniden sipariş vermeniz talep edilmektedir. <br>

          <br><p><b>Hatırlatma!</b></p>
          İade veya değişim taleplerinizin geçerli ve eksiksiz olabilmesi için ürünleriniz ile birlikte mutlaka faturanız yada irsaliye makbuzunuzun olması gerekmektedir. <br>
        </p>
      </div>
    </div>
  </div>
</section>
<!-- Main Container End -->
@endsection
