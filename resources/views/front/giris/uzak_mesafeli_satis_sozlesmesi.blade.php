@extends('front.master')
@section('content')
<!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>İptal, İade, Değişim</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
      <div class="page-content">
          <h2>MESAFELİ SATIŞ SÖZLEŞMESİ</h2>
          <p><b>MADDE 1 - TARAFLAR</b></p>
          SATICI <br>
          Ticari Ünvanı : <br>
          Adresi : <br>
          Telefon : <br>
          Satıcı Mersis No : <br>
          Satıcı E-Posta Adresi : <br>
          Alıcının İade Halinde Malı Satıcıya Göndereceği Kargo Şirketi :<br>
          gozlukmekani.com Çağrı Merkezi: <br>
          ALICI <br>
          Adı – soyadı : <br>
          Adresi : <br>
          Telefon : <br>
          E-Posta:<br> 

          <br><p><b>MADDE 2-SÖZLEŞMENİN KONUSU ve KAPSAMI</b></p>
          <p>İşbu Mesafeli Satış Sözleşmesi (“Sözleşme”) 6502 Sayılı Tüketicinin Korunması Hakkında Kanun ve Mesafeli Sözleşmeler Yönetmeliği'ne uygun olarak düzenlenmiştir. İşbu Sözleşme'nin tarafları işbu Sözleşme tahtında 6502 Sayılı Tüketicinin Korunması Hakkında Kanun ve Mesafeli Sözleşmeler Yönetmeliği'den kaynaklanan yükümlülük ve sorumluluklarını bildiklerini ve anladıklarını kabul ve beyan ederler. İşbu Sözleşmenin konusunu; Alıcı'nın, Gözlük Mekanı’na ait [www.gozlukmekani.com] alan adlı web sitesinden (“Websitesi”), Satıcı'ya ait Mal/Hizmetin satın alınmasına yönelik elektronik olarak sipariş verdiği, Sözleşmede belirtilen niteliklere sahip Mal/Hizmetin satışı ve teslimi ile ilgili olarak 6502 Sayılı Tüketicinin Korunması Hakkındaki Kanun ve Mesafeli Sözleşmeler Yönetmeliği hükümleri gereğince tarafların hak ve yükümlülüklerinin saptanması oluşturur. İşbu Sözleşmenin akdedilmesi tarafların ayrı ayrı Gözlük Mekanı ile akdetmiş oldukları websitesi üyelik sözleşmelerinin hükümlerinin ifasını engellemeyecek olup taraflar işbu Sözleşme konusu Mal/Hizmetin satışında Gözlük Mekanı’nın herhangi bir şekilde taraf olmadığını ve Sözleşme kapsamında tarafların yükümlülüklerini yerine getirmeleri ile ilgili herhangi bir sorumluluğu ve taahhüdü bulunmadığını kabul ve beyan ederler.</p>

          <br><p><b>MADDE 3 –SÖZLEŞME KONUSU MAL VE HİZMETİN TEMEL NİTELİKLERİ ve FİYATI (KDV DAHİL)</b></p>
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>Ürün Kodu Ve Adı</td>
                <td>Adet</td>
                <td>Satici Unvani</td>
                <td>Birim Fiyatı</td>
                <td>Birim İndirimi</td>
                <td>Kupon</td>
                <td>Puan</td>
                <td>Toplam Satış Tutarı</td>,
                <td>Vade Farkı</td>
                <td>KDV Dahil Toplam Tutar</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
          Kargo - Yurtiçi - Ücretsiz Kargo<br>
          İlan edilen fiyatlar ve vaatler güncelleme yapılana ve değiştirilene kadar geçerlidir. Süreli olarak ilan edilen fiyatlar ise belirtilen süre sonuna kadar geçerlidir.<br>
          Kargo hariç toplam ürün bedeli: <br>
          Kargo Ücreti: <br>
          Kargo Dahil Toplam Bedeli: <br>
          Ödeme Şekli ve Planı: <br>
          Alınan Vade Farkı: <br>
          Vade Farkı hesabında kullanılan faiz oranı:<br>
          Teslim Şartları:<br>
          Teslimat Adresi: <br>
          Teslim Edilecek Kişi(ler):<br>
          SÖZ KONUSU ÜRÜN BEDELİ, ÖDEME KORUMA SİSTEMİ KAPSAMINDA SATICI ADINA, GÖZLÜK MEKANI TARAFINDAN ALICI'DAN TAHSİL EDİLMEKTEDİR. ALICI MALIN BEDELİNİ GÖZLÜK MEKANI’NA ÖDEMEKLE, ÜRÜN BEDELİNİ SATICIYA ÖDEMİŞ SAYILACAKTIR.<br>

          <br><p><b>MADDE 4 - MALIN TESLİMİ VE TESLİM ŞEKLİ</b></p>
          <p>Sözleşme Alıcı tarafından elektronik ortamda onaylanmakla yürürlüğe girmiş olup Alıcı'nın Satıcı'dan satın almış olduğu Malın/Hizmetin Alıcı'ya teslim edilmesiyle ifa edilmiş olur. Mal/Hizmet, Alıcı'nın sipariş formunda ve işbu Sözleşmede belirtmiş olduğu adrese ve belirtilen yetkili kişi/kişilere teslim edilecektir.0</p>

          <br><p><b>MADDE 5 - TESLİMAT MASRAFLARI VE İFASI</b></p>
          <p>Malın teslimat masrafları aksine bir hüküm yoksa Alıcı'ya aittir. Satıcı, Web sitesinde teslimat ücretinin kendisince karşılanacağını beyan etmişse teslimat masrafları Satıcı'ya ait olacaktır. Malın teslimatı; Satıcı'nın stokunun müsait olması halinde ve ödemenin gerçekleşmesinden sonra taahhüt edilen sürede yapılır. Satıcı, sipariş konusu Mal/Hizmet ediminin yerine getirilmesinin imkansızlaştığı haller saklı kalmak kaydıyla, Mal/Hizmet'i, Alıcı tarafından Mal/Hizmet'in sipariş edilmesinden itibaren 30 (otuz) gün içinde teslim eder. Herhangi bir nedenle Alıcı tarafından Mal/Hizmet bedeli ödenmez veya yapılan ödeme banka kayıtlarında iptal edilir ise, Satıcı Mal/Hizmet'in teslimi yükümlülüğünden kurtulmuş kabul edilir.</p>
          <p>Malın Satıcı tarafından kargoya verilmesinden sonra ve fakat Alıcı tarafından teslim alınmasından önce Alıcı tarafından yapılan sipariş iptallerinde kargo bedelinden Alıcı sorumludur.</p>

          <br><p><b>MADDE 6 - ALICININ BEYAN VE TAAHHÜTLERİ</b></p>
          <p>Alıcı, Web sitesinde yer alan Sözleşme konusu Malın/Hizmetin temel nitelikleri, satış fiyatı ve ödeme şekli ile teslimat ve kargo bedeline ilişkin olarak Satıcı tarafından yüklenen ön bilgileri okuyup bilgi sahibi olduğunu ve elektronik ortamda gerekli teyidi verdiğini beyan eder. Alıcılar, Tüketici sıfatıyla talep ve şikayetlerini yukarıda yer alan Satıcı iletişim bilgilerine ve/veya Web sitesinin sağladığı kanallarla ulaştırabilirler. Alıcı, işbu Sözleşme'yi ve Ön Bilgilendirme Formunu elektronik ortamda teyit etmekle, mesafeli sözleşmelerin akdinden önce Satıcı tarafından Alıcıya verilmesi gereken adres, siparişi verilen Mal/Hizmet'e ait temel özellikler, Mal/Hizmet'in vergiler dahil fiyatı, ödeme ve teslimat ile teslimat fiyatı bilgilerini de doğru ve eksiksiz olarak edindiğini teyit etmiş olur. Alıcı'nın, Sözleşme konusu Mal/Hizmet'i teslim almadan önce muayene etmeksizin; tahrip olmuş, kırık, ambalajı yırtılmış vb. hasarlı ve ayıplı Mal/Hizmeti kargo şirketinden teslim alması halinde sorumluluk tamamen kendisine aittir. Alıcı tarafından kargo şirketi görevlisinden teslim alınan Mal/Hizmet'in hasarsız ve sağlam olduğu kabul edilecektir. Teslimden sonra Mal/Hizmet'in sorumluluğu ve hasarlar Alıcı'ya aittir. Mal/Hizmet'in tesliminden sonra Alıcı'ya ait kredi kartının Alıcı'nın kusurundan kaynaklanmayan bir şekilde yetkisiz kişilerce haksız veya hukuka aykırı olarak kullanılması nedeni ile ilgili banka veya finans kuruluşunun Mal/Hizmet bedelini Satıcı'ya ödememesi halinde, Alıcı kendisine teslim edilmiş olması kaydıyla Mal/Hizmet'i 3 (üç) gün içinde Satıcı'ya iade etmekle yükümlüdür. Bu halde teslimat giderleri Alıcı'ya aittir.</p>

          <br><p><b>MADDE 7 - SATICININ BEYAN VE TAAHHÜTLERİ</b></p>
          <p>Satıcı, Sözleşme konusu Mal/Hizmet'in Tüketici Mevzuatına uygun olarak, sağlam, eksiksiz, siparişte belirtilen niteliklere uygun ve varsa garanti belgeleri ve kullanım kılavuzları ile Alıcı'ya teslim edilmesinden sorumludur. Satıcı, mücbir sebepler veya nakliyeyi engelleyen olağanüstü durumlar nedeni ile sözleşme konusu Mal/Hizmeti süresi içinde teslim edemez ise, durumu öğrendiği tarihten itibaren 3 (üç) gün içinde Alıcı'ya bildirmekle yükümlüdür. Sözleşme konusu Mal/Hizmet, Alıcı'dan başka bir kişiye teslim edilecek ise, teslim edilecek kişinin teslimatı kabul etmemesinden Satıcı sorumlu tutulamaz.</p>

          <br><p><b>MADDE 8 – CAYMA HAKKI</b></p>
          <p>Alıcı, hiçbir hukuki ve cezai sorumluluk üstlenmeksizin ve hiçbir gerekçe göstermeksizin, satın aldığı Mal/Hizmeti teslim tarihten itibaren 14 (ondört) gün içerisinde cayma hakkını kullanarak iade edebilir.Cayma hakkı bildirimi ve Sözleşmeye ilişkin sair bildirimler Satıcı'ya ait ve/veya Web sitesinde belirtilen iletişim kanalları ile gönderilecektir. GÖZLÜK MEKANI, İŞBU MESAFELİ SATIŞ SÖZLEŞMESİNİN TARAFI OLMADIĞINDAN DP'YE KARŞI CAYMA HAKKI KULLANILAMAZ VEYA GÖZLÜK MEKANI'ndan BEDEL İADESİ TALEP EDİLEMEZ. Cayma hakkının kullanılması için süresi içerisinde Satıcı'ya mevzuat hükümlerine ve Websitesi'ndeki cayma hakkı kullanım seçeneğine uygun olarak bildirimde bulunulması şarttır. Cayma hakkının kullanılması halinde: a) Alıcı cayma hakkını kullanmasından itibaren 10 (on) gün içerisinde Malı Satıcı'ya geri gönderir. b) Cayma hakkı kapsamında iade edilecek Mal kutusu, ambalajı, varsa standart aksesuarları varsa Mal ile birlikte hediye edilen diğer ürünlerin de eksiksiz ve hasarsız olarak iade edilmesi gerekmektedir. Cayma hakkının kullanılmasını takip eden 14 (ondört) gün içerisinde Mal bedeli Alıcı'ya ödediği şekilde iade edilir. Mal, Satıcı'ya iade edilirken, Malın teslimi sırasında Alıcı'ya ibraz edilmiş olan orijinal faturanın da Alıcı tarafından iade edilmesi gerekmektedir.</p>
          <p>Alıcı iade edeceği Malı önbilgilendirme formunda belirtilen Satıcı'nın anlaşmalı kargo şirketi ile Satıcı'ya gönderdiği sürece iade kargo bedeli Satıcı'ya aittir. Alıcı'nın iade edeceği Malı önbilgilendirme formunda belirtilen Satıcı'nın anlaşmalı kargo şirketi dışında bir kargo şirketi ile göndermesi halinde iade kargo bedeli ve Malın kargo sürecinde uğrayacağı hasardan Satıcı sorumlu değildir.</p>

          <br><p><b>MADDE 9 – CAYMA HAKKININ KULLANILAMAYACAĞI HALLER</b></p>
          <p>Cayma hakkı aşağıdaki hallerde kullanılamaz: a) Fiyatı finansal piyasalardaki dalgalanmalara bağlı olarak değişen ve satıcının kontrolünde olmayan mal veya hizmetlere ilişkin sözleşmelerde (Ziynet, altın ve gümüş kategorisindeki ürünler) b) Tüketicinin istekleri veya açıkça onun kişisel ihtiyaçları doğrultusunda hazırlanan, niteliği itibariyle geri gönderilmeye elverişli olmayan ve çabuk bozulma tehlikesi olan veya son kullanma tarihi geçme ihtimali olan malların teslimine ilişkin sözleşmelerde c) Tesliminden sonra ambalaj, bant, mühür, paket gibi koruyucu unsurları açılmış olan mallardan; iadesi sağlık ve hijyen açısından uygun olmayanların teslimine ilişkin sözleşmelerde d) Tesliminden sonra başka ürünlerle karışan ve doğası gereği ayrıştırılması mümkün olmayan mallara ilişkin sözleşmelerde e) Tüketici tarafından ambalaj, bant, mühür, paket gibi koruyucu unsurları açılmış olması şartıyla maddi ortamda sunulan kitap, ses veya görüntü kayıtlarına, yazılım programlarına ve bilgisayar sarf malzemelerine ilişkin sözleşmelerde f) Abonelik sözleşmesi kapsamında sağlananlar dışında gazete, dergi gibi süreli yayınların teslimine ilişkin sözleşmelerde g) Belirli bir tarihte veya dönemde yapılması gereken, konaklama, eşya taşıma, araba kiralama, yiyecek-içecek tedariki ve eğlence veya dinlenme amacıyla yapılan boş zamanın değerlendirilmesine ilişkin sözleşmelerde h) Bahis ve piyangoya ilişkin hizmetlerin ifasına ilişkin sözleşmelerde ı) Cayma hakkı süresi sona ermeden önce, tüketicinin onayı ile ifasına başlanan hizmetlere ilişkin sözleşmelerde i) Elektronik ortamda anında ifa edilen hizmetler ile tüketiciye anında teslim edilen gayri maddi mallara ilişkin sözleşmelerde ve sözleşmeye konu Mal/Hizmet'in Mesafeli Sözleşmeler Yönetmeliği'nin uygulama alanı dışında bırakılmış olan (satıcının düzenli teslimatları ile alıcının meskenine teslim edilen gıda maddelerinin, içeceklerin ya da diğer günlük tüketim maddeleri ile seyahat, konaklama, lokantacılık, eğlence sektörü gibi alanlarda hizmetler) Mal/Hizmet türlerinden müteşekkil olması halinde Alıcı ve Satıcı arasındaki hukuki ilişkiye Mesafeli Sözleşmeler Yönetmeliği hükümleri uygulanamaması sebebiyle cayma hakkı kullanılamayacaktır. Tatil kategorisinde satışa sunulan bu tür Mal/Hizmetlerin iptal ve iade şartları Satıcı uygulama ve kurallarına tabidir.</p>

          <br><p><b>MADDE 10 - UYUŞMAZLIKLARIN ÇÖZÜMÜ</b></p>
          <p>İşbu Mesafeli Satış Sözleşmesi'nin uygulanmasında, Gümrük ve Ticaret Bakanlığınca ilan edilen değere kadar Alıcının Mal veya Hizmeti satın aldığı ve ikametgahının bulunduğu yerdeki Tüketici Hakem Heyetleri ile Tüketici Mahkemeleri yetkilidir. 6502 Sayılı Tüketicinin Korunması Hakkında Kanun'un 68'nci. Maddesinin 1. fıkrasında belirtilen alt ve üst limitler doğrultusunda tüketici talepleri hakkında ilçe/il tüketici hakem heyetleri yetkilidir.</p>

          <br><p><b>MADDE 11 - MALIN/HİZMETİN FİYATI</b></p>
          <p>Malın peşin veya vadeli satış fiyatı, sipariş formunda yer almakla birlikte, sipariş sonu gönderilen bilgilendirme maili ve ürün ile birlikte müşteriye gönderilen fatura içeriğinde mevcut olan fiyattır. Satıcı veya Gözlük Mekanı tarafından yapılan indirimler, kuponlar, kargo ücreti ve sair uygulamalar satış fiyatına yansıtılır.</p>

          <br><p><b>MADDE 12 - TEMERRÜD HALİ VE HUKUKİ SONUÇLARI</b></p>
          <p>Alıcı'nın, kredi kartı ile yapmış olduğu işlemlerde temerrüde düşmesi halinde kart sahibi bankanın kendisi ile yapmış olduğu kredi kartı sözleşmesi çerçevesinde faiz ödeyecek ve bankaya karşı sorumlu olacaktır. Bu durumda ilgili banka hukuki yollara başvurabilir; doğacak masrafları ve vekâlet ücretini Alıcı'dan talep edebilir ve her koşulda Alıcı'nın borcundan dolayı temerrüde düşmesi halinde, Alıcı'nın borcu gecikmeli ifasından dolayı Satıcı'nın uğradığı zarar ve ziyandan Alıcı sorumlu olacaktır.</p>

          <br><p><b>MADDE 13 – BİLDİRİMLER ve DELİL SÖZLEŞMESİ</b></p>
          <p>İşbu Sözleşme tahtında Taraflar arasında yapılacak her türlü yazışma, mevzuatta sayılan zorunlu haller dışında, e-mail aracılığıyla yapılacaktır. Alıcı, işbu Sözleşme'den doğabilecek ihtilaflarda Satıcı'nın ve Gözlük Mekanı resmi defter ve ticari kayıtlarıyla, kendi veritabanında, sunucularında tuttuğu elektronik bilgilerin ve bilgisayar kayıtlarının, bağlayıcı, kesin ve münhasır delil teşkil edeceğini, bu maddenin Hukuk Muhakemeleri Kanunu'nun 193. maddesi anlamında delil sözleşmesi niteliğinde olduğunu kabul, beyan ve taahhüt eder.</p>

          <br><p><b>MADDE 14 - YÜRÜRLÜK</b></p>
          <p>14 (on dört) maddeden ibaret bu Sözleşme, Taraflarca okunarak, 04/07/2017 tarihinde, Alıcı tarafından elektronik ortamda onaylanmak suretiyle akdedilmiş ve yürürlüğe girmiştir.</p>
          SATICI <br>
          ALICI<br>
        </p>
      </div>
    </div>
  </div>
</section>
<!-- Main Container End -->
@endsection
