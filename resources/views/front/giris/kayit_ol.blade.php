@extends('front.master')
@section('content')
<!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="index.html">Anasayfa</a><span>&raquo;</span></li>
          <li><strong>Giriş</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main container">
      <div class="page-content">
        <div class="account-login">
          <form class="" action="#" method="post">
            <div class="box-authentication">
              <h4>Kayıt Ol</h4><p>Yeni Hesap Oluştur</p>
              <label for="name">Adınız<span class="required">*</span></label>
              <input id="name" name="name" type="text" class="form-control">

              <label for="surname">Soyadınız<span class="required">*</span></label>
              <input id="surname" name="surname" type="text" class="form-control">

              <label for="emmail_register">Email Adresiniz<span class="required">*</span></label>
              <input id="emmail_register" type="text" class="form-control">

              <label for="password">Şifreniz<span class="required">*</span></label>
              <input id="password" name="password" type="text" class="form-control">

              <button class="button"><i class="fa fa-user"></i>&nbsp; <span>Kayıt Ol.</span></button>
            </div>
          </form>
      </div>
    </div>
  </div>
</section>
<!-- Main Container End -->
@endsection
