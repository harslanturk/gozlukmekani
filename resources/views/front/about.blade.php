@extends('front.master')
@section('content')
   <!-- Breadcrumbs -->

<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="/">Anasayfa</a><span>&raquo;</span></li>

          <li><strong>Hakkımızda</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumbs End -->

<!-- Main Container -->
<div class="main container">

   <div class="about-page">
      <div class="col-xs-12 col-sm-6">

        <h1>Hakkımızda<span class="text_color"></span></h1>
        <p>İnternet üzerinden satış ve pazarlama yapmak adına kurulmuş ve faaliyet süresi içerisinde Türkiye' nin önde gelen sanal mağazalarından biri olmayı hedeflemektedir.</p>
        <p>Yeni trend güneş gözlükleri ve optik çerçeveleri orjinal faturalı ve garanti belgeli bir şekilde bulabileceğiniz Türkiye' nin online optik sektörü alışveriş platformudur.</p>
        <p>% 100 Güvenli alışveriş yapabilmeniz için gerekli tüm teknolokij altyapıya sahiptir. En temel prensibimiz; müşteri memnuniyeti ve tüketici haklarına saygıdır.</p>
      </div>
      <div class="col-xs-12 col-sm-6" style="padding-top:25px;">
        <div class="single-img-add sidebar-add-slider">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active"> <img src="/front/images/logo.png" alt="slide1"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
<!-- Main Container End -->
<!-- service section -->

<div class="jtv-service-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-truck flip-horizontal"></i> </div>
          <div class="service-content">
            <h5>Ücretsiz Kargo </h5>
            <p>100.00 <i class="fa fa-try"></i> üzeri siparişlerinize</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-mail-forward"></i> </div>
          <div class="service-content">
            <h5>Garanti </h5>
            <p>Tün Ürünlerde 2 Yıl Garanti</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-4 col-xs-12">
        <div class="jtv-service">
          <div class="ser-icon"> <i class="fa fa-comments flip-horizontal"></i> </div>
          <div class="service-content">
            <h5>24/7 Müşteri Desteği </h5>
            <p>Günde 24 Saat Destek</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer -->
@endsection
