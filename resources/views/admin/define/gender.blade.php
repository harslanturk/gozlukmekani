@extends('layouts.welcome')
@section('content')
<div class="row">
  <div class="col-md-2">
    <!-- LINKED LIST GROUP-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tanımlamalar</h3>
        </div>
        <div class="panel-body">
            <div class="list-group border-bottom">
                <a href="/admin/define/color" class="list-group-item">Renk</a>
                <a href="/admin/define/gender" class="list-group-item active">Cinsiyet</a>
                <a href="/admin/define/quality" class="list-group-item">Kalite</a>
                <a href="/admin/define/glass" class="list-group-item">Gözlük Camı</a>
                <a href="/admin/define/slider" class="list-group-item">Slider</a>
            </div>
        </div>
    </div>
    <!-- END LINKED LIST GROUP-->
  </div>
    <div class="col-md-10">
      @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sistemdeki Cinsiyetler</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
              <form class="form-horizantal" action="/admin/define/gender/create" method="post">
                {{csrf_field()}}
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td>
                        <label class="control-label">Cinsiyet</label>
                      </td>
                      <td>
                        <input type="text" class="form-control" name="name" id="gender-name" placeholder="En az 5 Harf"/>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>
                        <button type="submit" class="btn btn-success" id="kaydet">Kaydet</button>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </form>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cinsiyet</th>
                            <th>Tarih</th>
                            <th class="col-sm-2">İşlemler</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; ?>
                      @foreach($genders as $gender)
                        <tr id="{{$gender->id}}">
                            <td>{{$no}}</td>
                            <td>{{$gender->name}}</td>
                            <td>{{$gender->created_at}}</td>
                            <td>
                                <a href="#" class="btn btn-danger" onclick="delete_row({{$gender->id}},'/admin/define/gender/delete/');" title="Sil">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                                <a href="#" class="btn btn-info guncelle" title="Güncelle" data-toggle="modal" data-target="#modal_update" id="{{$gender->id}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                          <?php $no++; ?>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span><strong>Veriyi Sil</strong> ?</div>
                    <div class="mb-content">
                        <p>İlgili Kaydı Silmek İstediğinize Emin Misiniz?</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes">Evet</button>
                            <button class="btn btn-default btn-lg mb-control-close">Hayır</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
<!-- MODALS -->
  <div class="modal" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">

  </div>
@endsection
@section('jscode')
<script type="text/javascript">
setTimeout(function(){
  $('.x-navigation-minimize').click()
});
document.getElementById('kaydet').setAttribute('disabled','');
$('#gender-name').keyup(function(){
  var renk = $('#gender-name').val();
  if (renk.length>2) {
  document.getElementById('kaydet').removeAttribute('disabled');
}else {
  document.getElementById('kaydet').setAttribute('disabled','');
}
});
$('.guncelle').click(function () {
    var id = $(this).attr('id');
    console.log(id);
    $.ajax({
        url: '/admin/define/gender/edit',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id: id},
        success: function(data){
            document.getElementById('modal_update').innerHTML=data;
        },
        error: function(jqXHR, textStatus, err){}
    });
});
</script>
@endsection
