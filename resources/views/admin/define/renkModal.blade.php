<div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizantal" action="/admin/define/color/update" method="post">
          {{csrf_field()}}
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="defModalHead">{{$color->name}} Renk Güncelle</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <input type="hidden" name="id" value="{{$color->id}}">
                  <label class="control-label col-xs-12 col-md-3">Renk</label>
                  <div class="col-md-9-col-xs-12">
                    <input type="text" class="form-control" name="name" id="renk-name" value="{{$color->name}}"/>
                  </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
              <button type="submit" class="btn btn-warning">Güncelle</button>
          </div>
        </form>
    </div>
</div>
