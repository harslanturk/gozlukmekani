@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
      @if(Session::has('error'))
      <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
          <strong>{{Session::get('error')}}</strong>
      </div>
      @endif
        <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/define/slider/update')}}">
          {{csrf_field()}}
        <div class="panel panel-default">
          <input type="hidden" class="form-control" name="id" value="{{$slider->id}}"/>
            <div class="panel-heading">
                <h3 class="panel-title">Slider Düzenle</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                  <label class="col-md-2 col-xs-12 control-label">Resim Seçiniz</label>
                  <div class="col-md-10 col-xs-12">
                    <input type="file" class="file" data-preview-file-type="any" name="image" id="image"/>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Yazı 1</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="text_1" value="{{$slider->text_1}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Yazı 2</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="text_2" value="{{$slider->text_2}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Yazı 3</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="text_3" value="{{$slider->text_3}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Url</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="url"  value="{{$slider->url}}"/>
                    </div>
                </div>
            </div>

            <div class="panel-body">
              <div class="panel-heading">
                <h3 class="panel-title">Yüklü Slider</h3>
              </div>
              <input type="hidden" id="resim" value="{{$slider->image}}">
              @if($slider->image)
              <div class="gallery" id="links">
                <a class="gallery-item" href="{{$slider->image}}" data-gallery>
                  <div class="image">
                    <img src="{{$slider->image}}" />
                    <ul class="gallery-item-controls">
                      <li><span class="image-remove" id="{{$slider->id}}" title="Sil"><i class="fa fa-times"></i></span></li>
                    </ul>
                  </div>
                </a>
              </div>
              @endif
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
@section('jscode')
<script type="text/javascript">
  var resim = document.getElementById('resim').value;
  if (resim != '') {
    setTimeout(function(){
      $('div.file-caption-name')[0].innerHTML = 'Yeni Slider İçin Eski Slideri Siliniz !'
      $('.input-group-btn > ').attr('disabled','')
    });
  }
  $('.image-remove').click(function(){
    var id = $(this).attr('id');

    $.ajax({
        url: '/admin/define/slider/imageDelete',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {
          id: id
        },
        success: function(data){
          if (data == 1) {
            location.reload();
          }
        },
        error: function(jqXHR, textStatus, err){}
    });
  });
</script>
@endsection
