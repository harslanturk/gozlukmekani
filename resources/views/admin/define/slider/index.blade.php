@extends('layouts.welcome')
@section('content')
<div class="row">
  <div class="col-md-2">
    <!-- LINKED LIST GROUP-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tanımlamalar</h3>
        </div>
        <div class="panel-body">
            <div class="list-group border-bottom">
                <a href="/admin/define/color" class="list-group-item">Renk</a>
                <a href="/admin/define/gender" class="list-group-item">Cinsiyet</a>
                <a href="/admin/define/quality" class="list-group-item">Kalite</a>
                <a href="/admin/define/glass" class="list-group-item">Gözlük Camı</a>
                <a href="/admin/define/slider" class="list-group-item active">Slider</a>
            </div>
        </div>
    </div>
    <!-- END LINKED LIST GROUP-->
  </div>
    <div class="col-md-10">
      @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sistemdeki Sliderlar</h3>
                <ul class="panel-controls">
                    <li><a href="{{url('/admin/define/slider/create')}}" class="btn-primary" style="color:white;"><span class=" fa fa-plus"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="col-sm-2">Slider</th>
                            <th>Yazı 1</th>
                            <th>Yazı 2</th>
                            <th>Yazı 3</th>
                            <th>Tarih</th>
                            <th class="col-sm-2">İşlemler</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; ?>
                      @foreach($sliders as $slider)
                        <tr id="{{$slider->id}}">
                            <td>{{$no}}</td>
                            <td> <img src="{{$slider->image}}" alt="" style="height:auto;width:100%;"></td>
                            <td>{{$slider->text_1}}</td>
                            <td>{{$slider->text_2}}</td>
                            <td>{{$slider->text_3}}</td>
                            <td>{{$slider->created_at}}</td>
                            <td>
                                <a href="#" class="btn btn-danger" onclick="delete_row({{$slider->id}},'/admin/define/slider/delete/');" title="Sil">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                                <a href="/admin/define/slider/edit/{{$slider->id}}" class="btn btn-info guncelle" title="Güncelle">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                          <?php $no++; ?>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span><strong>Veriyi Sil</strong> ?</div>
            <div class="mb-content">
                <p>İlgili Kaydı Silmek İstediğinize Emin Misiniz?</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Evet</button>
                    <button class="btn btn-default btn-lg mb-control-close">Hayır</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->
@endsection
@section('jscode')
<script type="text/javascript">
setTimeout(function(){
  $('.x-navigation-minimize').click();
  $('tr > td').attr('style','vertical-align:middle');
});
</script>
@endsection
