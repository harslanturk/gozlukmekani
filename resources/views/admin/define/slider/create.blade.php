@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
      @if(Session::has('error'))
      <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
          <strong>{{Session::get('error')}}</strong>
      </div>
      @endif
        <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/define/slider/save')}}">
          {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Yeni Slider</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                  <label class="col-md-2 col-xs-12 control-label">Resim Seçiniz</label>
                  <div class="col-md-10 col-xs-12">
                    <input type="file" class="file" data-preview-file-type="any" name="image" id="image"/>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Yazı 1</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="text_1"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Yazı 2</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="text_2"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Yazı 3</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="text_3"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Url</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" name="url"/>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
