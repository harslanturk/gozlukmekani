@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{Session::get('error')}}</strong>
        </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/modek/update/'.$modek->id)}}">
          {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>{{$modek->name}}</strong> Düzenle</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Model Adı</label>
                    <div class="col-md-6 col-xs-12">
                      <input type="text" class="form-control" name="name" value="{{$modek->name}}"/>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-success pull-right">Güncelle</button>
            </div>
        </div>
        </form>

    </div>
</div>
@endsection
