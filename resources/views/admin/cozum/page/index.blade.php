@extends('layouts.welcome')
@section('content')
<div class="row">
  <div class="col-md-2">
    <!-- LINKED LIST GROUP-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tanımlamalar 2</h3>
        </div>
        <div class="panel-body">
            <div class="list-group border-bottom">
                <a href="/admin/cozum/type" class="list-group-item">Tip</a>
                <a href="/admin/cozum/category" class="list-group-item">Kategori</a>
                <a href="/admin/cozum/page" class="list-group-item active">Sayfalar</a>
            </div>
        </div>
    </div>
    <!-- END LINKED LIST GROUP-->
  </div>
    <div class="col-md-10">
      @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sistemdeki Sayfalar</h3>
                <ul class="panel-controls">
                    <li><a href="{{url('/admin/cozum/page/create')}}" class="btn-primary" style="color:white;"><span class=" fa fa-plus"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Kategori</th>
                        <th>Başlık</th>
                        <th>Açıklama</th>
                        <th>Etiket</th>
                        <th>Tarih</th>
                        <th class="col-sm-2">İşlemler</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $no = 1; ?>
                      @foreach($pages as $page)
                        <tr id="{{$page->id}}">
                          <td>{{$no}}</td>
                          <td>{{$page->category->title}}</td>
                          <td>{{$page->title}}</td>
                          <td>{{$page->description}}</td>
                          <td>{{$page->keyword}}</td>
                          <td>{{$page->created_at}}</td>
                          <td>
                              <a href="#" class="btn btn-danger" onclick="delete_row({{$page->id}},'/admin/cozum/page/delete/');" title="Sil">
                                <i class="fa fa-trash-o"></i>
                              </a>
                              <a href="{{URL::to('/admin/cozum/page/edit/'.$page->id)}}" class="btn btn-info" title="Güncelle">
                                <i class="fa fa-edit"></i>
                              </a>
                          </td>
                        </tr>
                        <?php $no++; ?>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span><strong>Veriyi Sil</strong> ?</div>
                    <div class="mb-content">
                        <p>İlgili Kaydı Silmek İstediğinize Emin Misiniz?</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes">Evet</button>
                            <button class="btn btn-default btn-lg mb-control-close">Hayır</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
@endsection
@section('jscode')
<script type="text/javascript">
  setTimeout(function(){
    $('.x-navigation-minimize').click()
  });
</script>
@endsection
