@extends('layouts.welcome')
@section('content')
<div class="row">
  <div class="col-md-12">
    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <strong>{{Session::get('error')}}</strong>
    </div>
    @elseif(Session::has('success'))
      <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <strong>{{Session::get('success')}}</strong>
      </div>
    @endif
      <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/cozum/page/save')}}">
        {{csrf_field()}}
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Yeni Sayfa</h3>
          </div>
          <div class="panel-body">
          <div class="form-group">
            <label class="col-md-2 col-xs-12 control-label">Kategori</label>
            <div class="col-md-9 col-xs-12">
              <select class="form-control select" data-live-search="true" name="category_id" id="category_id">
                <option selected disabled>Kategori Seçiniz</option>
                @foreach($categorys as $category)
                <option value="{{$category->id}}">{{$category->title}}</option>
                @endforeach
              </select>
            </div>
          </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Başlık</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" name="title" required/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Açıklama</label>
              <div class="col-md-9 col-xs-12">
                <textarea class="form-control" name="description" rows="8" cols="80" style="resize:none;"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">İçerik</label>
              <div class="col-md-9 col-xs-12">
                  <textarea class="summernote" name="content"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Sıralama</label>
              <div class="col-md-9 col-xs-12">
                <input type="number" class="form-control" name="priority" placeholder="1,2,3,4,5,6,7,8,9 vb..."/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Anahtar Kelimeler</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="tagsinput" placeholder="Anahtar Kelime" name="keyword"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Üst Sayfa</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" data-live-search="true" name="parent_id">
                  <option selected disabled>Üst Kategori Seçiniz</option>
                  @foreach($pages as $page)
                  <option value="{{$page->id}}">{{$page->title}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button class="btn btn-success pull-left" id="kaydet">Kaydet</button>
          </div>
      </div>
      </form>
    </div>
</div>
@endsection
@section('jscode')
<script type="text/javascript">
  $('#kaydet').click(function(){
    var category_id = $('#category_id').val();
    if (!category_id) {
      sweetAlert('Kategori Seçilmedi.');
      return false;
    }
  });
</script>
@endsection
