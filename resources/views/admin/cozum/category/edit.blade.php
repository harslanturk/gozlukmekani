@extends('layouts.welcome')
@section('content')
<div class="row">
  <div class="col-md-12">
    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <strong>{{Session::get('error')}}</strong>
    </div>
    @elseif(Session::has('success'))
      <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <strong>{{Session::get('success')}}</strong>
      </div>
    @endif
      <form class="form-horizontal" method="post" action="{{URL::to('/admin/cozum/category/update/'.$category->id)}}">
        {{csrf_field()}}
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{$category->title}} Kategori Güncelle</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Başlık</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" name="title" value="{{$category->title}}"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Tip</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" data-live-search="true" name="type_id" id="type_id">
                  <option selected disabled>Tip Seçiniz</option>
                  @foreach($types as $type)
                  @if($type->id == $category->type_id)
                  <option value="{{$type->id}}" selected>{{$type->name}}</option>
                  @else
                  <option value="{{$type->id}}">{{$type->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Açıklama</label>
              <div class="col-md-9 col-xs-12">
                <textarea class="form-control" name="description" rows="8" cols="80" style="resize:none;">{{$category->description}}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Sıralama</label>
              <div class="col-md-9 col-xs-12">
                <input type="number" class="form-control" name="priority" placeholder="1,2,3,4,5,6,7,8,9 vb..."  value="{{$category->priority}}"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Üst Kategori</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" data-live-search="true" name="parent_id">
                  <option selected disabled>Üst Kategori Seçiniz</option>
                  @foreach($categorys as $val)
                    @if($val->id == $category->parent_id)
                      <option value="{{$val->id}}" selected>{{$val->title}}</option>
                    @else
                      <option value="{{$val->id}}">{{$val->title}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button class="btn btn-success pull-left">Güncelle</button>
          </div>
      </div>
      </form>
    </div>
</div>
@endsection
