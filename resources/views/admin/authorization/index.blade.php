@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Yetki Grupları</h3>
                <ul class="panel-controls">
                    <li>
                      <a href="{{URL::to('/admin/authorization/create')}}" class="btn-primary" style="color:white;"><span class=" fa fa-plus"></span></a>
                    </li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>İsim</th>
                            <th>Status</th>
                            <th class="col-sm-2">İşlemler</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php $no=1;?>
                      @foreach($groups as $group)
                        <tr id="{{$group->id}}">
                            <td>{{$no}}</td>
                            <td>{{$group->name}}</td>
                            @if($group->status == 1)
                            <td><strong class="text-success">Aktif</strong></td>
                            @else
                            <td><span class="text-danger">Pasif</span></td>
                            @endif
                            <td>
                                <a href="#" class="btn btn-danger" onclick="delete_row({{$group->id}},'/admin/auth-group/delete/');" title="Sil">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                                <a href="{{URL::to('/admin/authorization/edit/'.$group->id)}}" class="btn btn-info" title="Güncelle">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <?php $no++; ?>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span><strong>Veriyi Sil</strong> ?</div>
                    <div class="mb-content">
                        <p>İlgili Kaydı Silmek İstediğinize Emin Misiniz?</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes">Evet</button>
                            <button class="btn btn-default btn-lg mb-control-close">Hayır</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
@endsection
