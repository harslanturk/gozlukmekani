@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{$group->name}} Yetki Düzenle</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
              <form action="/admin/auth-group/update/{{$group->id}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="auth_group_id" value="{{$group->id}}">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
              <table class="table table-hover">
                  <thead>
                      <tr>
                        <th>Yetki Adı</th>
                        <th>
                          <label class="switch switch-small">
                              <input type="checkbox" class="Allread"/>
                              <span></span>
                          </label>Listeleme
                        </th>
                        <th>
                          <label class="switch switch-small">
                              <input type="checkbox" class="Alladd"/>
                              <span></span>
                          </label>Ekleme
                        </th>
                        <th>
                          <label class="switch switch-small">
                              <input type="checkbox" class="Allupdate"/>
                              <span></span>
                          </label>Düzenleme
                        </th>
                        <th>
                          <label class="switch switch-small">
                              <input type="checkbox" class="Alldelete"/>
                              <span></span>
                          </label>Silme
                        </th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php
                    $selected = "";
                     ?>
                    @foreach($old_auths as $old)
                      <tr>
                          <td>{{$old->name}}</td>
                          @if($old->read == 1)
                          <?php $selected="checked";?>
                          @endif
                          <td>
                              <label class="switch switch-small">
                                <input type="hidden" class="read" name="read[{{$old->m_id}}]" value="0"/>
                                <input type="checkbox" class="read" name="read[{{$old->m_id}}]" value="1" {{$selected}}/>
                                <span></span>
                              </label>
                          </td>
                          <?php $selected="";?>
                          @if($old->add == 1)
                          <?php $selected="checked";?>
                          @endif
                          <td>
                              <label class="switch switch-small">
                                <input type="hidden" class="add" name="add[{{$old->m_id}}]" value="0"/>
                                <input type="checkbox" class="add" name="add[{{$old->m_id}}]" value="1" {{$selected}}/>
                                <span></span>
                              </label>
                          </td>
                          <?php $selected="";?>
                          @if($old->update == 1)
                          <?php $selected="checked";?>
                          @endif
                          <td>
                              <label class="switch switch-small">
                                <input type="hidden" class="update" name="update[{{$old->m_id}}]" value="0"/>
                                <input type="checkbox" class="update" name="update[{{$old->m_id}}]" value="1" {{$selected}}/>
                                <span></span>
                              </label>
                          </td>
                          <?php $selected="";?>
                          @if($old->update == 1)
                          <?php $selected="checked";?>
                          @endif
                          <td>
                              <label class="switch switch-small">
                                <input type="hidden" class="delete" name="delete[{{$old->m_id}}]" value="0"/>
                                <input type="checkbox" class="delete" name="delete[{{$old->m_id}}]" value="1" {{$selected}}/>
                                <span></span>
                              </label>
                          </td>
                      </tr>
                      <?php $selected = ""; ?>
                      @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>
                        <button class="btn btn-success" type="submit" name="button">Güncelle</button>
                      </td>
                    </tr>
                  </tfoot>
              </table>
              </form>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
@endsection
@section('jscode')
  <script type="text/javascript">
    $('.Allread').change(function(){
    $(':checkbox.read').prop('checked', this.checked);
    });
    $('.Alladd').change(function(){
    $(':checkbox.add').prop('checked', this.checked);
    });
    $('.Allupdate').change(function(){
    $(':checkbox.update').prop('checked', this.checked);
    });
    $('.Alldelete').change(function(){
    $(':checkbox.delete').prop('checked', this.checked);
    });
  </script>
@endsection
