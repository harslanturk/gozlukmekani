@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">

        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Randevularım</h3>
                <ul class="panel-controls">
                    <li><a href="#" style="color:white;" id="createCalendar" class="btn-primary"  data-toggle="modal" data-target="#modalYeniRandevu"><span class=" fa fa-plus"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>

            <!-- START CONTENT FRAME -->
            <div class="content-frame">
                <!-- START CONTENT FRAME TOP -->
                <div class="content-frame-top">
                    <div class="page-title">
                        <h2><span class="fa fa-calendar"></span> Takvim</h2>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-default content-frame-left-toggle"><span class="fa fa-bars"></span></button>
                    </div>
                </div>
                <!-- END CONTENT FRAME TOP -->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert_holder"></div>
                            <div class="calendar">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                <!-- END CONTENT FRAME BODY -->

            </div>
            <!-- END CONTENT FRAME -->
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span><strong>Veriyi Sil</strong> ?</div>
            <div class="mb-content">
                <p>İlgili Kaydı Silmek İstediğinize Emin Misiniz?</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Evet</button>
                    <button class="btn btn-default btn-lg mb-control-close">Hayır</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- Randevu Ekleme Modeli -->
<div class="modal fade" id="modalYeniRandevu" tabindex="-1" role="dialog">

</div>
<!-- /.modal -->
@endsection
@section('jscode')
    <script>
        $(document).ready(function() {
            setTimeout(function(){
              $('.x-navigation-minimize').click()
            });
            //color picker with addon
            /*var calendar_url = '{{url('/admin/event')}}';
            if(calendar_url){
                var a =$('#calendar div > div');
                a[0].removeAttribute('style');
                //console.log(a[0]);
            }*/
            var base_url = '{{ url('/') }}';
            $('#calendar').fullCalendar({
                weekends: true,
                lang:'tr',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                // Sürükleyerek Güncelleme Yapma
                eventDrop: function(event, delta) {
                    var start = moment(event.start).format('YYYY-MM-DD HH:mm:ss');
                    var end = moment(event.end).format('YYYY-MM-DD HH:mm:ss');
                    //sweetAlert(start+'-'+end);
                    $.ajax({
                        url: '/admin/calendar/updateEventDrop',
                        type: 'POST',
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        cache: false,
                        data: {title:event.title,
                            start_time:start,
                            end_time:end,
                            id:event.id},
                        success: function(data){
                            sweetAlert(data);
                            setTimeout(function () {
                                window.location.reload();
                            },1000);
                            //document.getElementById('modalYeniRandevu').innerHTML=data;
                        },
                        error: function(jqXHR, textStatus, err){}
                    });
                },
                dayClick: function(date, jsEvent, view) {
                    var start = date.format();
                    $('#createCalendar').click();
                    setTimeout(function () {
                        document.getElementsByName('start_time')[0].type ='date';
                        document.getElementsByName('start_time')[0].value = start;
                        document.getElementsByName('end_time')[0].focus();
                        $('body').removeAttr('style');
                    },500);
                    //sweetAlert(start);
                    /*alert('Clicked on: ' + date.format());

                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                    alert('Current view: ' + view.name);

                    // change the day's background color just for fun
                    $(this).css('background-color', 'red');*/

                },
                eventLimit: true, // allow "more" link when too many events
                events: {
                    url: base_url + '/admin/calendar/api',
                    error: function() {
                        alert("Gelen Json Verisi Bulunamadı.");
                    }
                }
            });
        });
        $('#createCalendar').click(function () {
            var deger = 'a';
            /*console.log($(this).attr('name'));*/
            $.ajax({
                url: '/admin/calendar/createCalendarModalShow',
                type: 'POST',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                cache: false,
                data: {deger: deger},
                success: function(data){
                    document.getElementById('modalYeniRandevu').innerHTML=data;
                    RenkSec(10,'color');
                    $('body').removeAttr('style');
                },
                error: function(jqXHR, textStatus, err){}
            });
        });
        $('.calendarModal').click(function () {
            var event_id = $(this).attr('id');
            console.log(event_id);
            $.ajax({
                url: '/admin/calendar/editModal',
                type: 'POST',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                cache: false,
                data: {event_id: event_id},
                success: function(data){
                    document.getElementById('modalUpdateCalendar').innerHTML=data;
                },
                error: function(jqXHR, textStatus, err){}
            });
        });
        function titleBtn() {
            console.log('asd');
            var title = $('#title').val().trim();
            if(title.length >= 5){
                document.getElementById('randevuSaveBtn').removeAttribute('disabled');
            }
        }
        function RenkSec(number,name) {
            var rand = Math.round((Math.random() * number));
            var renkler = ['lightblue','lightcoral','lightgrey','lightgreen','lightpink','lightsalmon','lightseagreen','lightskyblue','lightslategray','lightsteelblue'];
            console.log(renkler[rand]);
            document.getElementsByName(name)[0].value = renkler[rand];
            document.getElementsByName(name)[0].style.backgroundColor = renkler[rand];
            document.getElementsByName(name)[0].style.color = 'white';
        }

    </script>
@endsection
