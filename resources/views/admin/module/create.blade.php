@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
         @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form action="{{url('/admin/module/save')}}" method="POST" class="form-horizantal">
            {{csrf_field()}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Modül Ekleme</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3">Modül Adı</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="name"/ placeholder="Anasayfa, Mesajlarım vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-md-3">Url</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="url"/ placeholder="admin/module vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-md-3">İcon</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="icon"/ placeholder="fa fa-icon, fa fa-folder vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-md-3">Menüdeki Öncelik</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="number" class="form-control" name="ranking" placeholder="1,2,3,4,5,6,7,8,9 vb..."/>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-xs-12 col-md-3">Menü de Gözüksün</label>
                          <div class="col-md-9 col-xs-12">
                            <label class="switch switch-small">
                            <input type="hidden" name="menu" value="0"/>
                            <input type="checkbox" name="menu" value="1"/>
                            <span></span>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-danger">Modül Ekle</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
