@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
         @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form action="{{url('/admin/module/update/'.$module->id)}}" method="POST" class="form-horizantal">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$module->id}}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$module->name}} Güncelle</h3>
                    <div class="pull-right">
                        <a href="#" class="btn btn-success" id="ustModulBtn">Alt Modül Ekle</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-6" id="ust_modul">
                        <input type="hidden" name="ust" value="1">
                         <div class="form-group">
                            <label for="" class="control-label col-xs-12 col-sm-3">Alt Modül Adı</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="text" class="form-control" name="name" value="{{$module->name}}" placeholder="Anasayfa, Mesajlarım vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-md-3">Url</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="text" class="form-control" name="url" value="{{$module->url}}" placeholder="admin/module vb..."/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-md-3">İcon</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="text" class="form-control" name="icon" value="{{$module->icon}}" placeholder="fa fa-icon, fa fa-folder vb..."/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-md-3">Menüdeki Öncelik</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="number" class="form-control" name="ranking" value="{{$module->ranking}}" placeholder="1,2,3,4,5,6,7,8,9 vb..."/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="altModul">
                        <input type="hidden" name="alt" value="1">
                        <div class="form-group">
                            <label for="" class="control-label col-xs-12 col-sm-3">Alt Modül Adı</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="text" class="form-control" name="name" placeholder="Anasayfa, Mesajlarım vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-xs-12 col-sm-3">Alt Modül Url</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="text" class="form-control" name="url" placeholder="admin/module vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-xs-12 col-sm-3">Alt Modül İcon</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="text" class="form-control" name="icon" placeholder="fa fa-icon, fa fa-folder vb...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-xs-12 col-sm-3">Alt Modül Öncelik</label>
                            <div class="col-md-9-col-xs-12">
                                <input type="number" class="form-control" name="ranking" placeholder="1,2,3,4,5,6,7,8,9 vb...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-danger">Modül Güncelle</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('jscode')
    <script>
        var click = 0;
        $('#altModul :input').attr('disabled','');
    $('#ustModulBtn').click(function(){
        if(click == 0){
            $('#ust_modul :input').attr('disabled','');
            $('#altModul :input').removeAttr('disabled','');
            $('#ustModulBtn').text('Üst Modül Ekle')
            click=+1;
        }else{
            $('#ustModulBtn').text('Alt Modül Ekle')
            $('#altModul :input').attr('disabled','');
            $('#ust_modul :input').removeAttr('disabled','');
            click  = 0;
        }

    });
    </script>
@endsection
