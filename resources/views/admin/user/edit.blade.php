@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
      @if(Session::has('hata'))
      <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
          <strong>{{Session::get('hata')}}</strong>
      </div>
      @endif
        <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/user/update/'.$user->id)}}">
          {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>{{$user->name}}</strong> Düzenle</h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Kullanıcı Adı</label>
                    <div class="col-md-6 col-xs-12">
                      <input type="text" class="form-control" name="name" value="{{$user->name}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">E-Mail</label>
                    <div class="col-md-6 col-xs-12">
                      <input type="text" class="form-control" name="email" value="{{$user->email}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Password</label>
                    <div class="col-md-6 col-xs-12">
                      <input type="password" class="form-control" name="password"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Resim Seçiniz</label>
                    <div class="col-md-6 col-xs-12">
                          <input type="file" class="fileinput btn-danger" name="image" id="image" data-filename-placement="inside" title="Resim Seç"/>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <button class="btn btn-success pull-right">Güncelle</button>
            </div>
        </div>
        </form>

    </div>
</div>
@endsection
