@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
         @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form action="{{url('/admin/product/update')}}" method="POST" class="form-horizantal">
        {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>{{$product->product_number}}</strong> Ürün Güncelleme</h3>
            </div>
            <div class="panel-body">
              <input type="hidden" name="id" value="{{$product->id}}">
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Yeni Ürün</label>
                  <div class="col-md-9 col-xs-12">
                    <label class="check"><input type="hidden" class="icheckbox" name="new" value="0"/></label>
                    <label class="check"><input type="checkbox" class="icheckbox" name="new" value="1" <?php if ($product->new) {
                      echo 'checked';
                    } ?>/></label>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Marka</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="brand_id">
                        <option selected disabled>Marka Seçiniz</option>
                        @foreach($brands as $brand)
                          @if($brand->id == $product->brand_id)
                          <option value="{{$brand->id}}" selected>{{$brand->name}}</option>
                          @else
                          <option value="{{$brand->id}}">{{$brand->name}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Model</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="modek_id">
                        <option selected disabled>Model Seçiniz</option>
                        @foreach($modeks as $modek)
                          @if($modek->id == $product->modek_id)
                          <option value="{{$modek->id}}" selected>{{$modek->name}}</option>
                          @else
                          <option value="{{$modek->id}}">{{$modek->name}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ürün Numarası</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="text" name="product_number" maxlength="100" value="{{$product->product_number}}" required/>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Renk</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="color_id">
                        <option selected disabled>Renk Seçiniz</option>
                        @foreach($colors as $color)
                          @if($color->id == $product->color_id)
                          <option value="{{$color->id}}" selected>{{$color->name}}</option>
                          @else
                          <option value="{{$color->id}}">{{$color->name}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Cinsiyet</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="gender_id">
                        <option selected disabled>Cinsiyet Seçiniz</option>
                        @foreach($genders as $gender)
                          @if($gender->id == $product->gender_id)
                          <option value="{{$gender->id}}" selected>{{$gender->name}}</option>
                          @else
                          <option value="{{$gender->id}}">{{$gender->name}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Kalite</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="quality_id">
                        <option selected disabled>Kalite Seçiniz</option>
                        @foreach($qualitys as $quality)
                          @if($quality->id == $product->quality_id)
                          <option value="{{$quality->id}}" selected>{{$quality->name}}</option>
                          @else
                          <option value="{{$quality->id}}">{{$quality->name}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Gözlük Camı</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="glass_id">
                        <option selected disabled>Gözlük Camı Seçiniz</option>
                        @foreach($glass as $glas)
                          @if($glas->id == $product->glass_id)
                          <option value="{{$glas->id}}">{{$glas->name}}</option>
                          @else
                          <option value="{{$glas->id}}">{{$glas->name}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Stok Adeti</label>
                  <div class="col-md-9 col-xs-12" style="padding-bottom: 15px;">
                    <input class="form-control" type="number" name="stock" maxlength="100" value="{{$product->stock}}" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Boyut</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="string" name="size" maxlength="100" value="{{$product->size}}" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ağırlık</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="number" name="weight" maxlength="100" value="{{$product->weight}}"/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Geliş Fiyatı</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="number" name="in_price" maxlength="100" value="{{$product->in_price}}" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Satış Fiyatı</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="number" id="out_price" name="out_price" maxlength="100" value="{{$product->out_price}}" required/>
                  </div>
                </div>
                <div class="form-group">
                  <?php
                    $checked = '';
                    $discount = '';
                    $disabled = 'disabled';
                    if ($product->discount > 0) {
                      $checked = 'checked';
                      $discount = $product->discount;
                      $disabled = '';
                    }
                   ?>
                  <label class="col-md-3 col-xs-12 control-label">İndirim</label>
                    <div class="col-md-5" style="margin-bottom:15px;">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <input type="checkbox" id="discount_ok" {{$checked}}/>
                            </span>
                            <input type="text" class="form-control text-danger" placeholder="% Olarak İndirim" name="discount" id="discount" value="{{$discount}}" {{$disabled}} style="font-weight:bold;"/>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom:15px;">
                      <input type="text" class="form-control text-danger" placeholder="İndirimli Fiyat" id="discount_price" disabled style="font-weight:bold;"/>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ekartman</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="text" name="ekartman" maxlength="100" value="{{$product->ekartman}}" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Renk Kodu</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="text" name="color_code" maxlength="50" value="{{$product->color_code}}" required/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Hızlı Açıklama</label>
                <div class="col-md-12 col-xs-12">
                  <textarea name="quick_content" rows="5" class="form-control" placeholder="255 Karaktere Kadar Hızlı Bir Açıklama Girebilirsiniz...">{{$product->quick_content}}</textarea>
                </div>
              <?php
                $kategori = explode(',',trim($product->category_id,','));
                $count = count($kategori);
                $say = 0;
                // echo '<pre>';
                // print_r($kategori);
                // die();
               ?>
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ürün Kategorileri</label>
                  <div class="col-md-12">
                      <select class="form-control select" data-live-search="true" multiple name="category_id[]">
                        @foreach($categorys as $key => $category)
                          @for($i=0;$i<$count;$i++)
                            @if($kategori[$i] == $category->id)
                            <?php $say++; ?>
                            @endif
                          @endfor
                          @if($say != 0)
                            <option value="{{$category->id}}" selected>{{$category->title}}</option>
                            @else
                            <option value="{{$category->id}}">{{$category->title}}</option>
                            @endif
                            <?php $say = 0; ?>
                        @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Ürün Açıklaması</label>
                <div class="block">
                  <textarea class="summernote" name="content">{{$product->content}}</textarea>
                </div>
              </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-success pull-left">Güncelle</button>
            </div>
        </div>
        </form>
        <form class="form-horizantal" action="/admin/product/image" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
          <div class="panel panel-primary">
            <div class="panel-body">
              <div class="panel-heading">
                  <h3 class="panel-title">Resim Yükleme</h3>
              </div>
              <div class="form-group">
                <input type="hidden" name="product_id" value="{{$product->id}}">
                <label class="col-md-3 col-xs-12 control-label" style="margin-top:15px;">Resim Seçiniz</label>
                <div class="col-md-9 col-xs-12 resim">
                  <input type="file" multiple class="file" data-preview-file-type="any" name="image[]" id="image"/>
                </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="panel-heading">
                <h3 class="panel-title">Yüklü Resimler</h3>
              </div>
            <div class="gallery" id="links">
              @foreach($product_images as $product_image)
              <?php $image_name = explode('/',$product_image->image);  ?>
              <a class="gallery-item" href="{{$product_image->image}}" title="{{$image_name[3]}}" data-gallery>
                  <div class="image">
                    <img src="{{$product_image->image}}" alt="{{$image_name[3]}}"/>
                    <ul class="gallery-item-controls">
                      <li><span class="image-remove" id="{{$product_image->id}}" title="Sil"><i class="fa fa-times"></i></span></li>
                    </ul>
                  </div>
                  <div class="meta">
                      <strong>{{$image_name[3]}}</strong>
                  </div>
              </a>
              @endforeach
            </div>
            </div>
          </div>
        </form>
    </div>
</div>

<button style="display:none;" class="btn btn-info mb-control" data-box="#message-box-info" id="uyari"></button>
<!-- info -->
<div class="message-box message-box-info animated fadeIn" data-sound="fail" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-info"></span> Geçersiz Uzantılar</div>
            <div class="mb-content">
                <strong><p id="inner"></p></strong>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Kapat</button>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
@endsection
@section('jscode')
<script type="text/javascript">
  // iki kolon yapınca margin yemiyor.Ondan burda yapıldı.
  $('.col-md-9').attr('style','margin-bottom:15px');
  $('.resim').attr('style','margin-top:8px');
  $('.col-md-12').attr('style','margin-bottom:15px');

  if ({{$product_images->count()}}>3) {
  setTimeout(function(){
    $('.input-group-btn > ').attr('disabled','')
  });
  }

  $('#image').change(function(){
      var file = document.getElementById("image");
      if (file.files.length>4) {
        file.value = '';
          $('#uyari').click();
          $('#inner')[0].innerHTML = 'En Fazla 4 Adet Resim Seçebilirsiniz !';
      }

      var dosyalar = '';
      for (var i = 0; i < file.files.length; i++) {
        var image = file.files[i].type.split('/');
        //console.log(image[0]);

        if (image[0] != 'image') {
          dosyalar = file.files[i].name+'<br>'+dosyalar;
        }
      }
      if (dosyalar) {
        $('#uyari').click();
        $('#inner')[0].innerHTML = dosyalar;
        //alert('Geçersiz Dosya '+dosyalar);
        file.value = '';
      }
    });

  $('.image-remove').click(function(){
    var id = $(this).attr('id');
    var product_id = {{$product->id}};

    $.ajax({
        url: '/admin/product/image/delete',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {
          id: id,
          product_id: product_id
        },
        success: function(data){
          if (data == 1) {
            location.reload();
          }
        },
        error: function(jqXHR, textStatus, err){}
    });
  });

  $(document).ready(function(){
    var out_price       = document.getElementById('out_price');
    var discount_price  = document.getElementById('discount_price');
    var discount  = document.getElementById('discount');
    var indirim_check   = document.getElementById('discount_ok').checked;

    if (indirim_check) {
        console.log('indirim Uygulandı');
        discount_price.value = out_price.value - ((out_price.value * discount.value)/100);
      $('#discount').keyup(function(){
          console.log('indirim Uygulandı');
          discount_price.value = out_price.value - ((out_price.value * discount.value)/100);
      });
    }
  });
  $('#discount_ok').change(function(){
    var indirim_check   = document.getElementById('discount_ok').checked;
    if (indirim_check) {
      console.log('İşaretli');
      document.getElementById('discount').removeAttribute('disabled');
      $('#discount').keyup(function(){
          console.log('indirim Uygulandı');
          discount_price.value = out_price.value - ((out_price.value * discount.value)/100);
      });
    }else {
      console.log('İşaretli Değil');
      document.getElementById('discount').setAttribute('disabled','');
    }
  });
  $('#out_price').keyup(function(){

    if (discount.value) {
      console.log('ikinci İş');
      discount_price.value = out_price.value - ((out_price.value * discount.value)/100);
    }
  });
</script>
@endsection
