@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
         @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form action="{{url('/admin/product/save')}}" method="POST" class="form-horizantal">
        {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Ürün Ekleme</h3>
            </div>
            <div class="panel-body">
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Yeni Ürün</label>
                  <div class="col-md-9 col-xs-12">
                    <label class="check"><input type="hidden" class="icheckbox" name="new" value="0"/></label>
                    <label class="check"><input type="checkbox" class="icheckbox" name="new" value="1"/></label>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Marka</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="brand_id">
                        <option selected disabled>Marka Seçiniz</option>
                        @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group" style="padding-top:15px;">
                    <label class="col-md-3 col-xs-12 control-label">Model</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="modek_id">
                        <option selected disabled>Model Seçiniz</option>
                        @foreach($modeks as $modek)
                        <option value="{{$modek->id}}">{{$modek->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ürün Numarası</label>
                  <div class="col-md-9 col-xs-12">
                    <input class="form-control" type="text" name="product_number" maxlength="100" required/>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Renk</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="color_id">
                        <option selected disabled>Renk Seçiniz</option>
                        @foreach($colors as $color)
                        <option value="{{$color->id}}">{{$color->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Cinsiyet</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="gender_id">
                        <option selected disabled>Cinsiyet Seçiniz</option>
                        @foreach($genders as $gender)
                        <option value="{{$gender->id}}">{{$gender->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Kalite</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="quality_id">
                        <option selected disabled>Kalite Seçiniz</option>
                        @foreach($qualitys as $quality)
                        <option value="{{$quality->id}}">{{$quality->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-xs-12 control-label">Gözlük Camı</label>
                    <div class="col-md-9 col-xs-12">
                      <select class="form-control select" data-live-search="true" name="glass_id">
                        <option selected disabled>Gözlük Camı Seçiniz</option>
                        @foreach($glass as $glas)
                        <option value="{{$glas->id}}">{{$glas->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
              </div>
              <div class="col-sm-6">
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Stok Adeti</label>
                <div class="col-md-9 col-xs-12" style="padding-bottom: 15px;">
                  <input class="form-control" type="number" name="stock" maxlength="100" required/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Boyut</label>
                <div class="col-md-9 col-xs-12">
                  <input class="form-control" type="string" name="size" maxlength="100"/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Ağırlık</label>
                <div class="col-md-9 col-xs-12">
                  <input class="form-control" type="number" name="weight" maxlength="100"/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Geliş Fiyatı</label>
                <div class="col-md-9 col-xs-12">
                  <input class="form-control" type="number" name="in_price" maxlength="100" required/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Satış Fiyatı</label>
                <div class="col-md-9 col-xs-12">
                  <input class="form-control" type="number" name="out_price" maxlength="100" id="out_price" required/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">İndirim</label>
                  <div class="col-md-5" style="margin-bottom:15px;">
                      <div class="input-group">
                          <span class="input-group-addon">
                              <input type="checkbox" id="discount_ok"/>
                          </span>
                          <input type="text" class="form-control" placeholder="% Olarak İndirim" name="discount" id="discount" disabled/>
                      </div>
                  </div>
                  <div class="col-md-4" style="margin-bottom:15px;">
                    <input type="text" class="form-control" placeholder="İndirimli Fiyat" id="discount_price" disabled/>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Ekartman</label>
                <div class="col-md-9 col-xs-12">
                  <input class="form-control" type="text" name="ekartman" maxlength="100" required/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Renk Kodu</label>
                <div class="col-md-9 col-xs-12">
                  <input class="form-control" type="text" name="color_code" maxlength="50" required/>
                </div>
              </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Hızlı Açıklama</label>
                <div class="col-md-12 col-xs-12">
                  <textarea name="quick_content" rows="5" class="form-control" placeholder="255 Karaktere Kadar Hızlı Bir Açıklama Girebilirsiniz..."></textarea>
                </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ürün Kategorileri</label>
                  <div class="col-md-12">
                      <select class="form-control select" data-live-search="true" multiple name="category_id[]">
                        @foreach($categorys as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Ürün Açıklaması</label>
                <div class="block">
                    <textarea class="summernote" name="content"></textarea>
                </div>
              </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-primary pull-left">Kaydet</button>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
@section('jscode')
<script type="text/javascript">
  // iki kolon yapınca margin yemiyor.Ondan burda yapıldı.
  $('.col-md-9').attr('style','margin-bottom:15px');
  $('.col-md-12').attr('style','margin-bottom:15px');

  $('#discount_ok').change(function(){
    var out_price       = document.getElementById('out_price');
    var discount_price  = document.getElementById('discount_price');
    var discount  = document.getElementById('discount');
    var indirim_check   = document.getElementById('discount_ok').checked;

    if (indirim_check) {
      console.log('İşaretli');
      document.getElementById('discount').removeAttribute('disabled');
      $('#discount').keyup(function(){
          console.log('indirim Uygulandı');
          discount_price.value = out_price.value - ((out_price.value * discount.value)/100);
      });
    }else {
      console.log('İşaretli Değil');
      document.getElementById('discount').setAttribute('disabled','');
    }
  });
  $('#out_price').keyup(function(){

    if (discount.value) {
      console.log('ikinci İş');
      discount_price.value = out_price.value - ((out_price.value * discount.value)/100);
    }
  });
</script>
@endsection
