@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{Session::get('error')}}</strong>
        </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/brand/save')}}">
          {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Yeni Marka</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Marka Adı</label>
                  <div class="col-md-6 col-xs-12">
                    <input type="text" class="form-control" name="name"/>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Resim Seçiniz</label>
                  <div class="col-md-6 col-xs-12">
                        <input type="file" class="fileinput btn-danger" name="image" id="image" data-filename-placement="inside" title="Resim Seç"/>
                  </div>
              </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-primary pull-right">Kaydet</button>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
