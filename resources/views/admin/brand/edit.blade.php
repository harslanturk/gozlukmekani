@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{Session::get('error')}}</strong>
        </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{Session::get('success')}}</strong>
            </div>
        @endif
        <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{URL::to('/admin/brand/update/'.$brand->id)}}">
          {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>{{$brand->name}}</strong> Düzenle</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Marka Adı</label>
                  <div class="col-md-6 col-xs-12">
                    <input type="text" class="form-control" name="name" value="{{$brand->name}}"/>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Resim Seçiniz</label>
                  <div class="col-md-6 col-xs-12">
                        <input type="file" class="fileinput btn-danger" name="image" id="image" data-filename-placement="inside" title="Resim Seç"/>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Menü Resmi 1</label>
                <div class="col-md-6 col-xs-12">
                  <input type="file" class="fileinput btn-primary" name="image_1[]" id="image_1" multiple data-filename-placement="inside" title="Resim Seç"/>
                </div>
              </div>
              <?php
                $menu_urun_id = explode(',',trim($brand->menu_urun_id,','));
                $count = count($menu_urun_id);
                $say = 0;
                // echo '<pre>';
                // print_r($kategori);
                // die();
               ?>
              <div class="form-group">
                  <label class="col-md-3 col-xs-12 control-label">Ürün Seçiniz</label>
                  <div class="col-md-9">
                      <select class="form-control select" data-live-search="true" multiple name="menu_urun_id[]">
                        @foreach($products as $key => $product)
                          @for($i=0;$i<$count;$i++)
                            @if($menu_urun_id[$i] == $product->id)
                            <?php $say++; ?>
                            @endif
                          @endfor
                          @if($say != 0)
                            <option value="{{$product->id}}" selected>{{$product->product_number}}</option>
                            @else
                            <option value="{{$product->id}}">{{$product->product_number}}</option>
                            @endif
                            <?php $say = 0; ?>
                        @endforeach
                      </select>
                  </div>
              </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-success pull-right">Güncelle</button>
            </div>
        </div>
        </form>
    </div>
</div>
<button style="display:none;" class="btn btn-info mb-control" data-box="#message-box-info" id="uyari"></button>
<!-- info -->
<div class="message-box message-box-info animated fadeIn" data-sound="fail" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-info"></span> Geçersiz Uzantılar</div>
            <div class="mb-content">
                <strong><p id="inner"></p></strong>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Kapat</button>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
@endsection
@section('jscode')
<script type="text/javascript">
$('#image_1').change(function(){
    var file = document.getElementById("image_1");
    if (file.files.length>3) {
      file.value = '';
        $('#uyari').click();
        $('#inner')[0].innerHTML = 'En Fazla 3 Adet Resim Seçebilirsiniz !';
    }

    var dosyalar = '';
    for (var i = 0; i < file.files.length; i++) {
      var image = file.files[i].type.split('/');
      //console.log(image[0]);

      if (image[0] != 'image') {
        dosyalar = file.files[i].name+'<br>'+dosyalar;
      }
    }
    if (dosyalar) {
      $('#uyari').click();
      $('#inner')[0].innerHTML = dosyalar;
      //alert('Geçersiz Dosya '+dosyalar);
      file.value = '';
    }
  });
</script>
@endsection
