<div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizantal" action="/admin/customer/cartEmail" method="post">
          {{csrf_field()}}
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="defModalHead"> Müşterinin Sepeti Görüntüleniyor</h4>
          </div>
          <div class="modal-body">
            <table class="table table-responsive">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Ürün Adı</th>
                  <th>Adet</th>
                  <th class="col-sm-2">Resim</th>
                </tr>
              </thead>
              <tbody>
                @foreach($carts as $key => $cart)
                <?php
                  $cover = App\Helpers\helper::getImageCover($cart->product_id);
                ?>
                  <tr>
                    <td>{{($key+1)}}</td>
                    <td>{{$cart->product->product_number}}</td>
                    <td>{{$cart->quantity}}</td>
                    <td> <img src="{{$cover->image}}" alt="" class="img-responsive"> </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
              <input type="hidden" name="user_id" value="{{$user_id}}">
              <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
              <button type="submit" class="btn btn-success">Email Gönder</button>
          </div>
        </form>
    </div>
</div>
