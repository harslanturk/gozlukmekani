@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-md-12">

        @if(Session::has('error'))
         <div class="alert alert-danger" role="alert">
           <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
           <strong>{{Session::get('error')}}</strong>
         </div>
         @elseif(Session::has('success'))
           <div class="alert alert-success" role="alert">
             <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
             <strong>{{Session::get('success')}}</strong>
           </div>
         @endif
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sistemdeki Müşteriler</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Müşteri İsmi</th>
                            <th>Doğum Tarihi</th>
                            <th>Hesap Durumu</th>
                            <th>Sepet</th>
                            <th>Telefon</th>
                            <th>Kayıt Tarihi</th>
                            <th class="col-sm-2">İşlemler</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; ?>
                      @foreach($customers as $customer)
                      <?php
                        $carts = App\Helpers\helper::sepetControl($customer->user_id);
                        // echo $carts->count();
                        // die();
                       ?>
                        <tr id="{{$customer->id}}">
                            <td>{{$no}}</td>
                            <td>{{$customer->user->name.' '.$customer->user->surname}}</td>
                            <td>{{$customer->birthdate}}</td>
                            @if($customer->status == 1)
                            <td>Kayıt Tamamlanmış</td>
                            @elseif($customer->status == 2)
                            <td>E-Mail Onaylanmamış</td>
                            @else
                            <td>Hesap Kapatılmış</td>
                            @endif
                            @if($carts->count())
                            <td> <a href="#" class="cart" data-toggle="modal" data-target="#cartView" id="{{$customer->user_id}}">Görüntüle</a> </td>
                            @else
                            <td>Boş</td>
                            @endif
                            <td>{{$customer->gsm}}</td>
                            <td>{{App\Helpers\helper::dmYHi($customer->created_at,'d-m-Y')}}</td>
                            <td>
                                <a href="#" class="btn btn-danger" onclick="delete_row({{$customer->id}},'/admin/customer/delete/');" title="Sil">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                                <a href="{{URL::to('/admin/product/edit/'.$customer->id)}}" class="btn btn-info" title="Güncelle">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                          <?php $no++; ?>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span><strong>Veriyi Sil</strong> ?</div>
                    <div class="mb-content">
                        <p>İlgili Kaydı Silmek İstediğinize Emin Misiniz?</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes">Evet</button>
                            <button class="btn btn-default btn-lg mb-control-close">Hayır</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
<!-- MODALS -->
<div class="modal" id="cartView" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">

</div>
@endsection
@section('jscode')
<script type="text/javascript">
setTimeout(function(){
  $('.x-navigation-minimize').click()
});
$('.cart').click(function () {
    var id = $(this).attr('id');
    console.log(id);
    $.ajax({
        url: '/admin/customer/cart',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {id: id},
        success: function(data){
            document.getElementById('cartView').innerHTML=data;
        },
        error: function(jqXHR, textStatus, err){}
    });
});
</script>
@endsection
