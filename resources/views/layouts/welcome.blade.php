<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META SECTION -->
        <title>Gözlük Mekanı Yönetim Paneli</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf_token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->
        <link rel="stylesheet" type="text/css" id="theme" href="{{url('/css/theme-default.css')}}"/>
        <!-- EOF CSS INCLUDE -->
        <link rel="stylesheet" type="text/css"href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css"/>
        <!-- fullCalendar 2.2.5-->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                @include('sidebar')
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                @include('header')
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="/">Anasayfa</a></li>
                    <li class="active text-danger">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    @yield('content')
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{URL::to('audio/alert.mp3')}}" preload="auto"></audio>
        <audio id="audio-fail" src="{{URL::to('audio/fail.mp3')}}" preload="auto"></audio>
        <!-- END PRELOADS -->
        @include('scripts')
        @yield('jscode')
    </body>
</html>
