<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Front\HomeController@index');
// Route::get('/list/product/{marka}/{list_value}','Front\HomeController@urunMarkaGetirListValue');
// Route::get('/list/product/{marka}','Front\HomeController@urunMarkaGetir');
//Route::get('/product/{marka}/{id}','Front\HomeController@urunGetir');
Route::get('/product-list/{category}','Front\HomeController@urunListele');
Route::get('/tum-urunler','Front\HomeController@allProduct');
Route::get('/tum-urunler/{list_value}','Front\HomeController@allProductListValue');
Route::post('/search','Front\HomeController@search');
Route::get('/search/{list_value}/{data}','Front\HomeController@searchListValue');
Route::get('/login','Front\HomeController@login');
Route::post('/front/register','CustomerController@register');
Route::get('/user-email-activation-code/{code}','CustomerController@emailActivation');
Route::post('/product/comment/new','CommentController@newComment');
Route::post('/quick_view/{id}','Front\HomeController@quickView');
Route::get('/iletisim','Front\HomeController@contact');
Route::get('/hakkimizda','Front\HomeController@about');
Route::get('/indirim','Front\HomeController@indirim');
Route::get('/iptal-iade-degisim','Front\HomeController@iptal_iade_degisim');
Route::get('/uzak-mesafeli-satis-sozlesmesi','Front\HomeController@mesafeli_satis_sozlesmesi');
Route::get('/test','Front\HomeController@test');
Route::get('/odemeTest','Front\HomeController@odemeTest');
Route::get('/odemeCTest','Front\HomeController@odemeCTest');
Route::post('/test','Front\HomeController@testP');



Route::group(['middleware' => ['auth']], function () {
  Route::get('/access',function(){
    echo 'YETKİSİZ';
  });
  //Hesabım Sayfaları
  Route::get('/myaccount','Front\WishlistController@myaccount');
  Route::get('/myaccount/wishlist','Front\WishlistController@wishlistIndex');
  Route::get('/myaccount/adres','Front\WishlistController@adres');
  Route::get('/myaccount/cart','CartController@myCart');
  Route::get('/sepet-urun-sil/{id}','CartController@deletegetCart');
  Route::get('/myaccount/teslimat-bilgileri','CartController@teslimatBilgileri');
  Route::post('/myaccount/siparis-bilgileri','CartController@siparisBilgileri');

  Route::post('/myaccount/save','Front\WishlistController@myaccountSave');
  Route::post('/myaccount/adres/save','Front\WishlistController@adresSave');
  Route::post('/myaccount/adres/createAdres','Front\WishlistController@createAdres');
  Route::post('/myaccount/adres/editAdres','Front\WishlistController@editAdres');
  Route::post('/myaccount/adres/update','Front\WishlistController@updateAdres');
  Route::get('/myaccount/adres/delete/{id}','Front\WishlistController@deleteAdres');

  Route::post('/sepete-ekle','CartController@addtoCart');
  Route::post('/sepet-yenile','CartController@gettoCart');
  Route::post('/sepet-urun-sil','CartController@deleteCart');
  Route::post('/sepet-adet-guncelle','CartController@updateQtyCart');
  Route::post('/sepet-adet-yenile','CartController@getQtyCart');

  Route::post('/product/my-wishlist/add','Front\WishlistController@addWishlist');
  Route::get('/myaccount/wishlist/delete/{id}','Front\WishlistController@deleteWishlist');

  Route::post('/myaccount/on-bilgilendirme','CartController@onBilgilendirme');
  Route::post('/myaccount/mesafeli-satis','CartController@mesafeliSatis');

});
Auth::routes();
Route::group(['middleware' => ['admin']], function () {
Route::get('/admin', 'HomeController@index');

//Kullanıcı İşlemleri
Route::get('/admin/user','UserController@index');
Route::get('/admin/user/create','UserController@createUser');
Route::get('/admin/user/edit/{id}','UserController@edit');

Route::post('/admin/user/save','UserController@saveUser');
Route::post('/admin/user/update/{id}','UserController@update');

//Yetkilendirme İşlemleri
Route::get('/admin/authorization','AuthorizationController@index');
Route::get('/admin/authorization/edit/{id}','AuthorizationController@editAuthorization');

Route::post('/admin/auth-group/update/{id}','AuthorizationController@updateAuthGroup');
Route::post('/admin/auth-group/delete/{id}','AuthorizationController@deleteAuth');

//Modul İşlemleri
Route::get('/admin/module','ModuleController@index');
Route::get('/admin/module/create','ModuleController@create');
Route::get('/admin/module/edit/{id}','ModuleController@edit');

Route::post('/admin/module/save','ModuleController@save');
Route::post('/admin/module/update/{id}','ModuleController@update');
Route::post('/admin/module/delete/{id}','ModuleController@delete');

//Marka - Brand İşlemleri
Route::get('/admin/brand','BrandController@index');
Route::get('/admin/brand/create','BrandController@create');
Route::get('/admin/brand/edit/{id}','BrandController@edit');

Route::post('/admin/brand/save','BrandController@save');
Route::post('/admin/brand/update/{id}','BrandController@update');
Route::post('/admin/brand/delete/{id}','BrandController@delete');

//Modek -- Model aslında ama model kullanılamadıgı ıcın modek olarak tanımlandı
Route::get('/admin/modek','ModekController@index');
Route::get('/admin/modek/create','ModekController@create');
Route::get('/admin/modek/edit/{id}','ModekController@edit');

Route::post('/admin/modek/save','ModekController@save');
Route::post('/admin/modek/update/{id}','ModekController@update');
Route::post('/admin/modek/delete/{id}','ModekController@delete');

//Product Ürün İşlemleri
Route::get('/admin/product','ProductController@index');
Route::get('/admin/product/create','ProductController@create');
Route::get('/admin/product/edit/{id}','ProductController@edit');

Route::post('/admin/product/save','ProductController@save');
Route::post('/admin/product/update','ProductController@update');
Route::post('/admin/product/delete/{id}','ProductController@delete');
Route::post('/admin/product/image','ProductController@product_image');
Route::post('/admin/product/image/delete','ProductController@product_imageDelete');

//Define Tanımlamalar /Color
Route::get('/admin/define','DefineController@index');
Route::get('/admin/define/color','DefineController@index');

Route::post('/admin/define/color/create','DefineController@colorCreate');
Route::post('/admin/define/color/edit','DefineController@colorEdit');
Route::post('/admin/define/color/update','DefineController@colorUpdate');
Route::post('/admin/define/color/delete/{id}','DefineController@colorDelete');

//Gender
Route::get('/admin/define/gender','DefineController@genderIndex');

Route::post('/admin/define/gender/create','DefineController@genderCreate');
Route::post('/admin/define/gender/edit','DefineController@genderEdit');
Route::post('/admin/define/gender/update','DefineController@genderUpdate');
Route::post('/admin/define/gender/delete/{id}','DefineController@genderDelete');

//Quality
Route::get('/admin/define/quality','DefineController@qualityIndex');

Route::post('/admin/define/quality/create','DefineController@qualityCreate');
Route::post('/admin/define/quality/edit','DefineController@qualityEdit');
Route::post('/admin/define/quality/update','DefineController@qualityUpdate');
Route::post('/admin/define/quality/delete/{id}','DefineController@qualityDelete');

//Glass
Route::get('/admin/define/glass','DefineController@glassIndex');

Route::post('/admin/define/glass/create','DefineController@glassCreate');
Route::post('/admin/define/glass/edit','DefineController@glassEdit');
Route::post('/admin/define/glass/update','DefineController@glassUpdate');
Route::post('/admin/define/glass/delete/{id}','DefineController@glassDelete');

//Calendar Events
Route::get('/admin/calendar','EventController@index');
Route::get('admin/calendar/api','EventController@api');

Route::post('/admin/calendar/createCalendarModalShow','EventController@createCalendarModalShow');
Route::post('/admin/calendar/calendarModalSave','EventController@calendarModalSave');
Route::post('/admin/calendar/editModal','EventController@editModal');
Route::post('/admin/calendar/updateCalendar/{id}','EventController@update');
Route::post('/admin/calendar/updateEventDrop','EventController@updateEventDrop');

//CÖzüm Lazım Type
Route::get('/admin/cozum','TypeController@index');
Route::get('/admin/cozum/type','TypeController@index');
Route::get('/admin/cozum/type/create','TypeController@create');
Route::get('/admin/cozum/type/edit/{id}','TypeController@edit');

Route::post('/admin/cozum/type/save','TypeController@save');
Route::post('/admin/cozum/type/update/{id}','TypeController@update');
Route::post('/admin/cozum/type/delete/{id}','TypeController@delete');

//CÖzüm Lazım Category
Route::get('/admin/cozum/category','CategoryController@index');
Route::get('/admin/cozum/category/create','CategoryController@create');
Route::get('/admin/cozum/category/edit/{id}','CategoryController@edit');

Route::post('/admin/cozum/category/save','CategoryController@save');
Route::post('/admin/cozum/category/update/{id}','CategoryController@update');
Route::post('/admin/cozum/category/delete/{id}','CategoryController@delete');

//Cözüm Sayfalar
Route::get('/admin/cozum/page','PageController@index');
Route::get('/admin/cozum/page/create','PageController@create');
Route::get('/admin/cozum/page/edit/{id}','PageController@edit');

Route::post('/admin/cozum/page/save','PageController@save');
Route::post('/admin/cozum/page/update/{id}','PageController@update');
Route::post('/admin/cozum/page/delete/{id}','PageController@delete');

//Slider İşlemleri
Route::get('/admin/define/slider','SliderController@index');
Route::get('/admin/define/slider/create','SliderController@create');
Route::get('/admin/define/slider/edit/{id}','SliderController@edit');

Route::post('/admin/define/slider/save','SliderController@save');
Route::post('/admin/define/slider/update','SliderController@update');
Route::post('/admin/define/slider/imageDelete','SliderController@imageDelete');
Route::post('/admin/define/slider/delete/{id}','SliderController@delete');

//Comment - Yorum İşlemleri
Route::get('/admin/comment/confirm/{id}','CommentController@confirm');

//Müşteri İşlemleri
Route::get('/admin/customer','CustomerController@index');
Route::post('/admin/customer/delete/{id}','CustomerController@delete');
Route::post('/admin/customer/cart','CustomerController@cartView');
Route::post('/admin/customer/cartEmail','CustomerController@cartEmail');

});

Route::get('/{deger}','Front\HomeController@checkStatus');
Route::get('/{deger}/{list_value}','Front\HomeController@checkStatusList');
Route::get('/{marka}/{urun_kodu}/{id}','Front\HomeController@checkUrun');
