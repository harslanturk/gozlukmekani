
    function delete_row(row,url){

        var box = $("#mb-remove-row");
        box.addClass("open");

        box.find(".mb-control-yes").on("click",function(){
            box.removeClass("open");
            var id = row;
            console.log(url+row);
            $.ajax({
                url: url+row,
                type: 'POST',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                cache: false,
                data: {id: id},
                success: function(data){
                    if (data == 1) {
                        $("#"+row).hide("slow",function(){
                            $(this).remove();
                        });
                    }else if(data == 'product'){
                        console.log(data);
                        alert('Markaya Bağlı Ürün Mevcut İlk önce Ürünleri Siliniz');
                    }
                },
                error: function(jqXHR, textStatus, err){}
            });
            
                id = '';
                url = '';
        });

    }
    setTimeout(function(){
            $('div[role=alert]').hide('slow')
        },2000);
