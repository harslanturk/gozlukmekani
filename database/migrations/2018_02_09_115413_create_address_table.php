<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address',function(Blueprint $table){
          $table->increments('id');
          $table->integer('customer_id');
          $table->integer('address_type');
          $table->string('name');
          $table->integer('vergi_no');
          $table->string('company',255);
          $table->integer('city_id');
          $table->integer('region_id');
          $table->integer('zip_code');
          $table->text('adres');
          $table->integer('status');
          $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('address');
    }
}
