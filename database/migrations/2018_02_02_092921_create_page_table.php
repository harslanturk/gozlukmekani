<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page',function(Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('category_id');
          $table->integer('gallery_id');
          $table->string('title',255);
          $table->string('slug',255);
          $table->string('description',255);
          $table->text('content');
          $table->integer('priority');
          $table->integer('parent');
          $table->integer('parent_id');
          $table->string('keyword',255);
          $table->integer('status');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page');
    }
}
