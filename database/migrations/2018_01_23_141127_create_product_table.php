<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product',function(Blueprint $table){
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('brand_id');
          $table->integer('modek_id');
          $table->string('quick_content',255);
          $table->text('content');
          $table->string('product_number',100)->unique();
          $table->integer('color_id');
          $table->string('color_code');
          $table->integer('stock');
          $table->integer('size');
          $table->float('weight',8,2);
          $table->integer('gender_id');
          $table->integer('quality_id');
          $table->float('in_price',8,2);
          $table->float('out_price',8,2);
          $table->string('ekartman',25);
          $table->integer('glass_id');
          $table->string('comment_id');// 1,3,5 şeklinde tutulacak
          $table->string('category_id');// ,1,2,3,4,5, şeklinde tutuluyor
          $table->float('discount',8,2); // İndirim
          $table->integer('new');
          $table->integer('status');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
