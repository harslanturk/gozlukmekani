<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';
    protected $fillable = [
      'user_id','image','text_1','text_2','text_3','url','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
