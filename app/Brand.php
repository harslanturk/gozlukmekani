<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand';
    protected $fillable = [
      'user_id','name','slug','image','status','image_1','menu_urun_id'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
