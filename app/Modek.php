<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modek extends Model
{
    protected $table = 'modek';
    protected $fillable = [
        'user_id','name','status'
    ];
    
    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
