<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';
    protected $fillable = [
      'user_id','mail_code','gsm','birthdate','gender','banned','tc_no','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
