<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Page;
use App\Category;
use App\Type;
use App\Helpers\helper;

class PageController extends Controller
{
    public function index()
    {
      $pages = Page::where('status',1)->get();

      return view('admin.cozum.page.index',[
        'pages' => $pages
      ]);
    }

    public function create()
    {
      $tipage = Type::where('name','like','%page%')->first();
      $categorys = Category::where('status',1)->where('type_id',$tipage->id)->get();
      $pages = Page::where('status',1)->get();
      return view('admin.cozum.page.create',[
        'categorys' => $categorys,
        'pages'     => $pages
      ]);
    }

    public function save(Request $request)
    {
      $data = $request->all();
      $data['gallery_id'] = 0;
      $data['status'] = 1;
      $data['user_id'] = Auth::user()->id;
      $data['slug'] = helper::tr_slug($data['title']);
      $data['parent'] = 0;
      if (empty($data['parent_id'])) {
        $data['parent_id'] = 0;
      }
      // echo '<pre>';
      // print_r($data);
      // die();

      try{
        Page::create($data);
        Session::flash('success', 'Sayfa Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function edit(Request $request,$id)
    {
      $tipage = Type::where('name','like','%page%')->first();
      $categorys = Category::where('status',1)->where('type_id',$tipage->id)->get();

      $page = Page::where('id',$id)->first();
      $pages = Page::where('status',1)->get();
      return view('admin.cozum.page.edit',[
        'categorys' => $categorys,
        'pages'     => $pages,
        'page'      => $page
      ]);
    }
    public function update(Request $request,$id)
    {
      $data = $request->all();
      unset($data['_token']);
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Page::where('id',$id)->update($data);
        Session::flash('success', 'Sayfa Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
    public function delete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Page::where('id',$id)->update([
          'status' => 0
        ]);
        Session::flash('success', 'Sayfa Silindi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
}
