<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use App\Helpers\helper;
use App\Brand;
use App\Product;

class BrandController extends Controller
{
    public function index()
    {
      $brands = Brand::where('status',1)->get();
      return view('admin.brand.index',[
        'brands' => $brands
      ]);
    }
    public function create()
    {
      return view('admin.brand.create');
    }
    public function save(Request  $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $data['image'] = '';
      $data['status'] = 1;
      $data['slug'] = helper::tr_slug($data['name']);

      try{
        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/img/uploads/');
        $image->move($destinationPath,$input['imagename']);

        $data['image'] = '/img/uploads/'.$input['imagename'];
        $brand = Brand::create($data);
        Session::flash('success', 'Marka Oluşturuldu');
        return redirect()->to('/admin/brand/edit/'.$brand->id);
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
    public function edit($id)
    {
      $brand = Brand::where('id',$id)->first();
      $products = Product::where('brand_id',$brand->id)
                          ->where('status',1)->orderBy('product_number')->get();
      return view('admin.brand.edit',[
        'brand' => $brand,
        'products' => $products
      ]);
    }
    public function update(Request $request,$id)
    {
      $data = $request->all();
      $data['slug'] = helper::tr_slug($data['name']);
      $imgtotal = '';
      $veri = '';
      $images = $request->file('image_1');

      if (empty($data['menu_urun_id'])) {
              $data['menu_urun_id'] = ',0,';
      }else {
        foreach ($data['menu_urun_id'] as $key => $value) {
          $veri .= ','.$data['menu_urun_id'][$key];
        }
        $data['menu_urun_id'] = $veri.',';
      }

      if (!empty($images)) {
        foreach ($images as $key => $image) {
          $name = $image->getClientOriginalName();
          $destinationPath = public_path('/img/uploads/');

          $image->move($destinationPath,$name);
          $imgtotal .= '/img/uploads/'.$name.',';
        }
        $image = Brand::where('id',$id)->first();
        $image->image_1 = rtrim($imgtotal,',');
        $image->save();

      }
      // echo '<pre>';
      // print_r(rtrim($imgtotal,','));
      // die();
      try {
        $image = $request->file('image');
        if (!empty($image)) {
          $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

          $destinationPath = public_path('/img/uploads/');
          $image->move($destinationPath,$input['imagename']);

          $data['image'] = '/img/uploads/'.$input['imagename'];
        }

        Brand::findOrFail($id)->update($data);
        return redirect()->back();
      } catch (\Exception $e) {
        Session::flash('hata',$e->getMessage());
        return redirect()->back();
      }

    }
    public function delete(Request $request,$id)
    {
      $data = $request->all();

      try{
        $product = Product::where('brand_id',$id)->get();
        if ($product->count()) {
          Session::flash('error', $product->count().' Adet Ürün Mevcut İlk önce Ürünleri Siliniz');
          return 'product';
        }else{
          Brand::where('id',$id)->update(['status' => 0]);
          Session::flash('success', 'ok');
          return 1;
        }
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
}
