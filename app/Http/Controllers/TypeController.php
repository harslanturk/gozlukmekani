<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use Auth;
use Session;

class TypeController extends Controller
{
    public function index()
    {
      $types = Type::where('status',1)->get();

      return view('admin.cozum.type.index',[
        'types' => $types
      ]);
    }

    public function create()
    {
      return view('admin.cozum.type.create');
    }
    public function save(Request $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $data['status']  = 1;

      try{
        Type::create($data);
        Session::flash('success', 'Tip Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
    public function edit(Request $request,$id)
    {
      $type = Type::where('id',$id)->first();

      return view('admin.cozum.type.edit',[
        'type' => $type
      ]);
    }
    public function update(Request $request,$id)
    {
      $data = $request->all();

      try{
        Type::where('id',$id)->update([
          'name' => $data['name']
        ]);
        Session::flash('success', 'Tip Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
    public function delete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Type::where('id',$id)->update([
          'status' => 0
        ]);
        Session::flash('success', 'Tip Silindi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
}
