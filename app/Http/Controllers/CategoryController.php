<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use Auth;
use Session;
use App\Category;
use App\Helpers\helper;

class CategoryController extends Controller
{
    public function index()
    {
      $categorys = Category::where('status',1)->get();

      return view('admin.cozum.category.index',[
        'categorys' => $categorys
      ]);
    }

    public function create()
    {
      $types = Type::where('status',1)->get();
      $categorys = Category::where('status',1)->get();
      return view('admin.cozum.category.create',[
        'types' => $types,
        'categorys' => $categorys
      ]);
    }

    public function save(Request $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $data['status']  = 1;
      $data['parent']  = 0;
      $data['slug']    = helper::tr_slug($data['title']);
      if (empty($data['parent_id'])) {
        $data['parent_id'] = 0;
      }
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Category::create($data);
        Session::flash('success', 'Kategori Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function edit(Request $request,$id)
    {
      $category = Category::where('id',$id)->first();
      $categorys = Category::where('status',1)->get();
      $types = Type::where('status',1)->get();
      return view('admin.cozum.category.edit',[
        'category'  => $category,
        'types'     => $types,
        'categorys' => $categorys
      ]);
    }
    public function update(Request $request,$id)
    {
      $data = $request->all();
      unset($data['_token']);
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Category::where('id',$id)->update($data);
        Session::flash('success', 'Kategori Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
    public function delete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Category::where('id',$id)->update([
          'status' => 0
        ]);
        Session::flash('success', 'Tip Silindi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
}
