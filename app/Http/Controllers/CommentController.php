<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use App\Comment;
use App\User;

class CommentController extends Controller
{
  public function newComment(Request $request)
  {
    $data = $request->all();
    $data['status']   = 2;
    $data['user_id']  = Auth::user()->id;

    // echo '<pre>';
    // print_r($data);
    // die();
    try{
      Comment::create($data);
      return 1;
    }catch(\Exception $e){
      Session::flash('error', $e->getMessage());
      return $e->getMessage();
    }
  }

  public function confirm($id)
  {
    try{
      Comment::where('id',$id)->update(['status' => 1]);
      Session::flash('success', 'Yorum Başarı İle Onaylandı.');
      return redirect()->back();
    }catch(\Exception $e){
      Session::flash('error', $e->getMessage());
      return $e->getMessage();
    }
  }
}
