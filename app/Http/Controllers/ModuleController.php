<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use Session;
use Auth;
use App\Helpers\helper;

class ModuleController extends Controller
{
    public function index()
    {
        $modules = Module::where('status',1)->get();
        return view('admin.module.index',[
            'modules' => $modules
        ]);
    }

    public function create()
    {
        return view('admin.module.create');
    }

    public function save(Request $request)
    {
        $data = $request->all();
        $slash = strpos($data['url'],'/');
        if($slash>0){
            $data['url'] = '/'.$data['url'];
        }
        $data['slug'] = helper::tr_slug($data['name']);
        $data['user_id'] = Auth::user()->id;
        $data['parent'] = 0;
        $data['parent_id'] = 0;
        $data['status'] = 1;

        // echo '<pre>';
        // print_r($data);
        // die();

        try{
            Module::create($data);
            Session::flash('success', 'Modül Oluşturuldu');
            return redirect()->back();
        }catch(\Exception $e){
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $module = Module::where('id',$id)->first();
        return view('admin.module.edit', [
            'module' => $module
        ]);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();
        if(!empty($data['ust'])){
        unset($data['_token']);
        unset($data['ust']);
          try{
              Module::where('id',$data['id'])->update($data);
              Session::flash('success', 'Modül Güncellendi');
              return redirect()->back();
          }catch(\Exception $e){
              Session::flash('error', $e->getMessage());
              return redirect()->back();
          }
        }elseif(!empty($data['alt'])){
          $data['parent_id'] = $data['id'];
          unset($data['id']);
          $data['slug'] = helper::tr_slug($data['name']);
          $data['user_id'] = Auth::user()->id;
          $data['parent'] = 1;
          $data['status'] = 1;
          // echo '<pre>';
          // print_r($data);
          // die();
          try{
              Module::create($data);
              Session::flash('success', 'Alt Modül Oluşturuldu');
              return redirect()->back();
          }catch(\Exception $e){
              Session::flash('error', $e->getMessage());
              return redirect()->back();
          }
        }

    }

    public function delete(Request $request,$id)
    {
      $data = $request->all();

      try{
          Module::where('id',$id)->update(['status' => 0]);
          Session::flash('success', 'Alt Modül Oluşturuldu');
          return 1;
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
}
