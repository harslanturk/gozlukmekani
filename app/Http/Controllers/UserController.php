<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use App\Helpers\Helper;
use Illuminate\Support\Facades\File;
use App\AuthGroup;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.user.index',[
            'users' => $users
        ]);
    }
    public function createUser()
    {
      $auth_group = AuthGroup::where('status',1)->get();
      return view('admin.user.create',[
        'auth_group' => $auth_group
      ]);
    }
    public function saveUser(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      $data['password'] = bcrypt($data['password']);
        $image = $request->file('image');
        if ($image) {
          $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

          $destinationPath = public_path('/img/uploads/');
          $image->move($destinationPath,$input['imagename']);

          $data['image'] = '/img/uploads/'.$input['imagename'];
        }

      try {
        User::create($data);
        $users = User::all();
        return view('admin.user.index',[
            'users' => $users
        ]);
      } catch (\Exception $e) {
        Session::flash('hata',$e->getMessage());
        return redirect()->back();
      }

    }
    public function edit($id)
    {
      $user = User::where('id',$id)->first();
      return view('admin.user.edit',[
        'user' => $user
      ]);
    }

    public function update(Request $request,$id)
    {
      $data = $request->all();
      try {
        if (!empty($request->file('image'))) {
          $image = $request->file('image');
          $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

          $destinationPath = public_path('/img/uploads/');
          $image->move($destinationPath,$input['imagename']);

          $data['image'] = '/img/uploads/'.$input['imagename'];
        }

        if (!empty($data['password'])) {
          $data['password'] = bcrypt($data['password']);
        }else{
          unset($data['password']);
        }

        User::findOrFail($id)->update($data);
        return redirect()->back();
      } catch (\Exception $e) {
        Session::flash('hata',$e->getMessage());
        return redirect()->back();
      }

    }
}
