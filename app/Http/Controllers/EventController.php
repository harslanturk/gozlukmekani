<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    public function index()
    {
      return view('admin.calendar.index');
    }

    public function api()
    {
      $events = Event::select('id', 'user_id', 'title', 'start_time as start', 'end_time as end','color')
          ->get();
     /* echo '<pre>';
              print_r($events);
              die();*/
      return $events;
    }

    public function createCalendarModalShow()
    {
        return view('admin.calendar.createCalendarModal');
    }
    public function calendarModalSave(Request $request)
    {
        $data = $request->all();
        Event::create($data);

        return redirect()->back();
    }
    public function editModal(Request $request)
    {
        $data = $request->all();
        $events = Event::findOrFail($data['event_id']);
        /*echo '<pre>';
        echo $events->id;
        print_r($events);
        die();*/
        return view('admin.calendar.editModal', ['events' => $events]);
    }
    public function update(Request $request, $id)
    {
        $event = $request->all();
        Event::find($id)->update($event);

        return redirect()->back();
    }

    public function updateEventDrop(Request $request)
    {
        $data = $request->all();
        Event::find($data['id'])->update($data);

        return 'Güncelleme Tamam Sayfa Yeniden Yüklenecek';
    }
}
