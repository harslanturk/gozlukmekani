<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use App\Helpers\helper;
use App\Modek;

class ModekController extends Controller
{
    public function index()
    {
      $modeks = Modek::where('status',1)->get();

      return view('admin.modek.index',[
        'modeks' => $modeks
      ]);
    }
    public function create()
    {
      return view('admin.modek.create');
    }
    public function save(Request  $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $data['image'] = '';
      $data['status'] = 1;

      try{
        Modek::create($data);
        Session::flash('success', 'Model Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
    public function edit($id)
    {
      $modek = Modek::where('id',$id)->first();
      return view('admin.modek.edit',[
        'modek' => $modek
      ]);
    }
    public function update(Request $request,$id)
    {
      $data = $request->all();
      try {
        Modek::findOrFail($id)->update($data);
        return redirect()->back();
      } catch (\Exception $e) {
        Session::flash('hata',$e->getMessage());
        return redirect()->back();
      }

    }    
    public function delete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Modek::where('id',$id)->update(['status' => 0]);
          Session::flash('success', 'ok');
          return 1;
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
}
