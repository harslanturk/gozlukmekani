<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\Product;
use App\Category;
use App\ProductImage;
use App\Comment;
use App\Helpers\helper;
use App\Brand;
use App\Cart;

class HomeController extends Controller
{

    public function contact()
    {
      return view('front.contact');
    }

    public function about()
    {
      return view('front.about');
    }
    public function index()
    {
      $one_cikan = Category::where('title','Öne Çıkanlar')->first();
      $one_cikanlars = Product::where('category_id','like','%,'.$one_cikan->id.',%')->where('status',1)->get();
        $cok_satan = Category::where('title','Çok Satanlar')->first();
        $cok_satanlars = Product::where('category_id','like','%,'.$cok_satan->id.',%')->where('status',1)->get();
          $ozel_urun = Category::where('title','Özel Ürünler')->first();
          $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->get();
            $hafta = Category::where('title','Haftanın Satanları')->first();
            $haftanin_urunlers = Product::where('category_id','like','%,'.$hafta->id.',%')->where('status',1)->take(8)->get();

      return view('front.home',[
        'one_cikanlars'   => $one_cikanlars,
        'cok_satanlars'   => $cok_satanlars,
        'ozel_urunlers'   => $ozel_urunlers,
        'haftanin_urunlers'  => $haftanin_urunlers
      ]);
    }

    public function urunGetir($id)
    {
      $product  = Product::where('id',$id)->first();
      $images   = ProductImage::where('product_id',$id)->orderBy('id','asc')->get();
      $comments = Comment::where('product_id',$id)->where('status',1)->orderBy('id','desc')->take(5)->get();
      return view('front.product.urun_goruntule',[
        'product'  => $product,
        'images'   => $images,
        'comments' => $comments
      ]);
    }

    public function CategoryListele($category)
    {
      $cat = Category::where('slug','like','%'.$category.'%')->first();
      $cat_products = DB::table('product')
                       ->leftJoin('brand','brand.id','=','product.brand_id')
                       ->leftJoin('modek','modek.id','=','product.modek_id')
                       ->where('product.status',1)
                       ->where('category_id','like','%,'.$cat->id.',%')
                       ->select('product.*','brand.name as brand_name','brand.slug','modek.name as modek_name')
                       ->Paginate(12);

      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.slug')
                  ->where('product.category_id','like','%,'.$cat->id.',%')
                       ->where('product.status',1)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

    $ozel_urun = Category::where('title','Özel Ürünler')->first();
    $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

                    // echo '<pre>';
                    // print_r($brands);
                    // die();
      return view('front.product.category',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function CategoryListeleListValue($category,$list_value)
    {
      $orderBy ='';
      $list    ='';
      if ($list_value == 'name') {
        $list_value = 'brand.name';
        $orderBy = 'asc';
        $list ='name';
      }elseif ($list_value == 'max_min') {
        $list_value = 'product.out_price';
        $orderBy = 'desc';
        $list ='max_min';
      }elseif ($list_value == 'min_max') {
        $list_value = 'product.out_price';
        $orderBy = 'asc';
        $list ='min_max';
      }elseif ($list_value == 'new') {
        $list_value = 'product.created_at';
        $orderBy = 'desc';
        $list ='new';
      }
      $cat = Category::where('slug','like','%'.$category.'%')->first();
      $cat_products = DB::table('product')
                       ->leftJoin('brand','brand.id','=','product.brand_id')
                       ->leftJoin('modek','modek.id','=','product.modek_id')
                       ->where('product.status',1)
                       ->where('category_id','like','%,'.$cat->id.',%')
                       ->orderBy($list_value,$orderBy)
                       ->select('product.*','brand.name as brand_name','brand.slug','modek.name as modek_name')
                       ->Paginate(12);

      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.slug')
                       ->where('product.status',1)
                  ->where('product.category_id','like','%,'.$cat->id.',%')
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

    $ozel_urun = Category::where('title','Özel Ürünler')->first();
    $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

                    // echo '<pre>';
                    // print_r($brands);
                    // die();
      return view('front.product.category',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'list'          => $list,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function urunListeleMarka($category,$marka)
    {
      $cat = Category::where('slug','like','%'.$category.'%')->first();
      $brands = Brand::where('slug',$marka)->first();
      $cat_products = DB::table('product')
                       ->leftJoin('brand','brand.id','=','product.brand_id')
                       ->leftJoin('modek','modek.id','=','product.modek_id')
                       ->where('product.status',1)
                       ->where('product.brand_id',$brands->id)
                       ->where('category_id','like','%,'.$cat->id.',%')
                       ->select('product.*','brand.name as brand_name','brand.slug','modek.name as modek_name')
                       ->Paginate(12);

       $ozel_urun = Category::where('title','Özel Ürünler')->first();
       $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();


                    // echo '<pre>';
                    // print_r($brands);
                    // die();
      return view('front.product.marka_list',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function login()
    {
      return view('front.giris.login');
    }

    public function quickView($id)
    {
      $product  = Product::where('id',$id)->first();
      $images   = ProductImage::where('product_id',$id)->orderBy('id','asc')->get();
      $comments = Comment::where('product_id',$id)->where('status',1)->orderBy('id','desc')->take(5)->get();
        // echo '<pre>';
        // print_r($comments);
        // die();
      return view('front.product.popup_product',[
        'product'  => $product,
        'images'   => $images,
        'comments' => $comments
      ]);
    }
    public function allProduct()
    {
      //$cat_products = Product::where('status',1)->Paginate(12);
      $cat_products = DB::table('product')
                    ->leftJoin('brand','brand.id','=','product.brand_id')
                    ->leftJoin('modek','modek.id','=','product.modek_id')
                    ->where('product.status',1)
                    ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                    ->Paginate(12);
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.slug')
                       ->where('product.status',1)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

    $ozel_urun = Category::where('title','Özel Ürünler')->first();
    $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($brands);
      // die();

      return view('front.product.product_list',[
        'cat_products'  => $cat_products,
        'brands'    => $brands,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function allProductListValue($list_value)
    {
      $orderBy ='';
      $list ='';
      if ($list_value == 'name') {
        $list_value = 'brand.name';
        $orderBy = 'asc';
        $list ='name';
      }elseif ($list_value == 'max_min') {
        $list_value = 'product.out_price';
        $orderBy = 'desc';
        $list ='max_min';
      }elseif ($list_value == 'min_max') {
        $list_value = 'product.out_price';
        $orderBy = 'asc';
        $list ='min_max';
      }elseif ($list_value == 'new') {
        $list_value = 'product.created_at';
        $orderBy = 'desc';
        $list ='new';
      }
      $cat_products = DB::table('product')
                    ->leftJoin('brand','brand.id','=','product.brand_id')
                    ->leftJoin('modek','modek.id','=','product.modek_id')
                    ->where('product.status',1)
                    ->orderBy($list_value,$orderBy)
                    ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                    ->Paginate(12);
      //$cat_products = Product::where('status',1)->simplePaginate($list_value);
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.slug')
                       ->where('product.status',1)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

    $ozel_urun = Category::where('title','Özel Ürünler')->first();
    $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($cat_products);
      // die();

      return view('front.product.product_list',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'list'    => $list,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function urunMarkaGetir($marka)
    {
      $brand =  Brand::where('slug',$marka)->where('status',1)->first();
      $cat_products = DB::table('product')
                    ->leftJoin('brand','brand.id','=','product.brand_id')
                    ->leftJoin('modek','modek.id','=','product.modek_id')
                    ->where('product.status',1)
                    ->where('brand.slug',$brand->slug)
                    ->orderBy('brand_name')
                    ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                    ->Paginate(12);

      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($cat_products);
      // echo $cat_products->total();
      // die();

      return view('front.product.marka_list',[
        'cat_products'  => $cat_products,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }
    public function urunMarkaGetirListValue($marka,$list_value)
    {
      $orderBy ='';
      $list    ='';
      if ($list_value == 'name') {
        $list_value = 'brand.name';
        $orderBy = 'asc';
        $list ='name';
      }elseif ($list_value == 'max_min') {
        $list_value = 'product.out_price';
        $orderBy = 'desc';
        $list ='max_min';
      }elseif ($list_value == 'min_max') {
        $list_value = 'product.out_price';
        $orderBy = 'asc';
        $list ='min_max';
      }elseif ($list_value == 'new') {
        $list_value = 'product.created_at';
        $orderBy = 'desc';
        $list ='new';
      }
      $brand =  Brand::where('slug',$marka)->where('status',1)->first();
      $cat_products = DB::table('product')
                    ->leftJoin('brand','brand.id','=','product.brand_id')
                    ->leftJoin('modek','modek.id','=','product.modek_id')
                    ->where('product.status',1)
                    ->where('brand.slug',$brand->slug)
                    ->orderBy($list_value,$orderBy)
                    ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                    ->Paginate(12);

      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($cat_products);
      // die();

      return view('front.product.marka_list',[
        'cat_products'  => $cat_products,
        'list'    => $list,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }
    public function urunCategoryMarkaGetir($category,$marka)
    {
      $brand =  Brand::where('slug',$marka)->where('status',1)->first();
      $cat = Category::where('slug','like','%'.$category.'%')->first();
      $cat_products = DB::table('product')
                    ->leftJoin('brand','brand.id','=','product.brand_id')
                    ->leftJoin('modek','modek.id','=','product.modek_id')
                    ->where('product.status',1)
                    ->where('brand.slug',$brand->slug)
                    ->where('category_id','like','%,'.$cat->id.',%')
                    ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                    ->Paginate(12);

      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($cat_products);
      // die();

      return view('front.product.category_brand',[
        'cat_products'  => $cat_products,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function checkStatus($deger)
    {
        $marka = Brand::where('slug', $deger)->first();
        $cat = Category::where('slug','like','%'.$deger.'%')->first();
        if(isset($marka))
        {
          return $this->urunMarkaGetir($marka->slug);
        }elseif (isset($cat)) {
          //Kategoriye göre Ürün listeleme
          return $this->CategoryListele($cat->slug);
        }elseif($deger == 'indirim'){
          return $this->indirim();
        }
        else{
          echo "hata";
        }
    }

    public function checkStatusList($deger, $listValue)
    {

        $marka = Brand::where('slug','like','%'.$deger.'%')->first();
        $category = Category::where('slug','like','%'.$deger.'%')->first();

        if (isset($marka)) {
           return $this->urunMarkaGetirListValue($marka->slug,$listValue);
        }elseif (isset($category)) {
            $catMarka = Brand::where('slug','like','%'.$listValue.'%')->first();
          if (isset($catMarka)) {
             return $this->urunCategoryMarkaGetir($category->slug,$listValue);
          }else {
            return $this->CategoryListeleListValue($category->slug,$listValue);
          }

        }elseif($deger == 'indirim'){
          $catMarka = Brand::where('slug','like','%'.$listValue.'%')->first();
          if (isset($catMarka)) {
           return $this->indirimMarka($listValue);
          }else{
            return $this->indirimListValue($listValue);
          }
        }
    }

    public function checkUrun($marka,$urun_kodu,$id)
    {
      if (is_numeric($id)) {
        $marka = Brand::where('slug',$marka)->first();
        $product  = Product::where('id',$id)
                            ->where('brand_id',$marka->id)
                            ->where('product_number',$urun_kodu)
                       ->where('status',1)
                            ->first();
        $images   = ProductImage::where('product_id',$id)->orderBy('id','asc')->get();
        $comments = Comment::where('product_id',$id)->where('status',1)->orderBy('id','desc')->take(5)->get();
        return view('front.product.urun_goruntule',[
          'product'  => $product,
          'images'   => $images,
          'comments' => $comments
        ]);
      }elseif($marka == 'indirim') {
        //Kategoriye Göre marka listeleme

        $list_value = $id;
        $orderBy ='';
        $list    ='';
        if ($list_value == 'name') {
          $list_value = 'brand.name';
          $orderBy = 'asc';
          $list ='name';
        }elseif ($list_value == 'max_min') {
          $list_value = 'product.out_price';
          $orderBy = 'desc';
          $list ='max_min';
        }elseif ($list_value == 'min_max') {
          $list_value = 'product.out_price';
          $orderBy = 'asc';
          $list ='min_max';
        }elseif ($list_value == 'new') {
          $list_value = 'product.created_at';
          $orderBy = 'desc';
          $list ='new';
        }
        $brands = Brand::where('slug',$urun_kodu)->first();
        $cat_products = DB::table('product')
                         ->leftJoin('brand','brand.id','=','product.brand_id')
                         ->leftJoin('modek','modek.id','=','product.modek_id')
                         ->where('product.status',1)
                         ->where('product.brand_id',$brands->id)
                          ->where('product.discount','>',0)
                         ->orderBy($list_value,$orderBy)
                         ->select('product.*','brand.name as brand_name','brand.slug','modek.name as modek_name')
                         ->Paginate(12);
         $ozel_urun = Category::where('title','Özel Ürünler')->first();
         $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();


                      // echo '<pre>';
                      // print_r($cat_products);
                      // die();
        return view('front.product.indirim_brand',[
          'cat_products'  => $cat_products,
          'brands'        => $brands,
          'list'          => $list,
          'ozel_urunlers' => $ozel_urunlers
        ]);
      }else{
                //Kategoriye Göre marka listeleme

        $list_value = $id;
        $orderBy ='';
        $list    ='';
        if ($list_value == 'name') {
          $list_value = 'brand.name';
          $orderBy = 'asc';
          $list ='name';
        }elseif ($list_value == 'max_min') {
          $list_value = 'product.out_price';
          $orderBy = 'desc';
          $list ='max_min';
        }elseif ($list_value == 'min_max') {
          $list_value = 'product.out_price';
          $orderBy = 'asc';
          $list ='min_max';
        }elseif ($list_value == 'new') {
          $list_value = 'product.created_at';
          $orderBy = 'desc';
          $list ='new';
        }
        $cat = Category::where('slug','like','%'.$marka.'%')->first();
        $brands = Brand::where('slug',$urun_kodu)->first();
        $cat_products = DB::table('product')
                         ->leftJoin('brand','brand.id','=','product.brand_id')
                         ->leftJoin('modek','modek.id','=','product.modek_id')
                         ->where('product.status',1)
                         ->where('product.brand_id',$brands->id)
                         ->where('category_id','like','%,'.$cat->id.',%')
                         ->orderBy($list_value,$orderBy)
                         ->select('product.*','brand.name as brand_name','brand.slug','modek.name as modek_name')
                         ->Paginate(12);

         $ozel_urun = Category::where('title','Özel Ürünler')->first();
         $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

                      // echo '<pre>';
                      // print_r($cat_products);
                      // die();
        return view('front.product.category_brand',[
          'cat_products'  => $cat_products,
          'brands'        => $brands,
          'list'          => $list,
          'ozel_urunlers' => $ozel_urunlers
        ]);
      }
    }

    public function search(Request $request)
    {
      $data = $request->all();
      $cat_products = DB::table('product')
                ->leftJoin('brand','brand.id','=','product.brand_id')
                ->leftJoin('modek','modek.id','=','product.modek_id')
                ->where('brand.name','like','%'.$data['search'].'%')
                       ->where('product.status',1)
                ->orWhere('product.product_number','like','%'.$data['search'].'%')
                ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                ->orderBy('product.product_number','asc')
                ->Paginate(12);
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->where('brand.name','like','%'.$data['search'].'%')
                  ->groupBy('brand.name','brand.slug')
                       ->where('product.status',1)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();
      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();
      // echo '<pre>';
      // print_r($urun);
      // die();

      return view('front.product.search',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'data'          => $data['search'],
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function searchListValue($list_value,$data)
    {
      $orderBy ='';
      $list ='';
      if ($list_value == 'name') {
        $list_value = 'brand.name';
        $orderBy = 'asc';
        $list ='name';
      }elseif ($list_value == 'max_min') {
        $list_value = 'product.out_price';
        $orderBy = 'desc';
        $list ='max_min';
      }elseif ($list_value == 'min_max') {
        $list_value = 'product.out_price';
        $orderBy = 'asc';
        $list ='min_max';
      }elseif ($list_value == 'new') {
        $list_value = 'product.created_at';
        $orderBy = 'desc';
        $list ='new';
      }
      $cat_products = DB::table('product')
                        ->leftJoin('brand','brand.id','=','product.brand_id')
                        ->leftJoin('modek','modek.id','=','product.modek_id')
                        ->where('brand.name','like','%'.$data.'%')
                        ->orWhere('product.product_number','like','%'.$data.'%')
                        ->orderBy($list_value,$orderBy)
                        ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                        ->orderBy('product.product_number','asc')
                        ->Paginate(12);
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->where('brand.name','like','%'.$data.'%')
                  ->groupBy('brand.name','brand.slug')
                       ->where('product.status',1)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();
      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($cat_products);
      // die();

      return view('front.product.search',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'list'          => $list,
        'data'          => $data,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function indirim()
    {
      $cat_products = DB::table('product')
                ->leftJoin('brand','brand.id','=','product.brand_id')
                ->leftJoin('modek','modek.id','=','product.modek_id')
                ->where('product.status',1)
                ->where('product.discount','>',0)
                ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                ->orderBy('product.product_number','asc')
                ->Paginate(12);
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.slug')
                  ->where('product.status',1)
                  ->where('product.discount','>',0)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($urun);
      // die();

      return view('front.product.indirim',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function indirimListValue($list_value)
    {
      $orderBy ='';
      $list ='';
      if ($list_value == 'name') {
        $list_value = 'brand.name';
        $orderBy = 'asc';
        $list ='name';
      }elseif ($list_value == 'max_min') {
        $list_value = 'product.out_price';
        $orderBy = 'desc';
        $list ='max_min';
      }elseif ($list_value == 'min_max') {
        $list_value = 'product.out_price';
        $orderBy = 'asc';
        $list ='min_max';
      }elseif ($list_value == 'new') {
        $list_value = 'product.created_at';
        $orderBy = 'desc';
        $list ='new';
      }

      $cat_products = DB::table('product')
                        ->leftJoin('brand','brand.id','=','product.brand_id')
                        ->leftJoin('modek','modek.id','=','product.modek_id')
                        ->where('product.discount','>',0)
                        ->where('product.status',1)
                        ->orderBy($list_value,$orderBy)
                        ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                        ->orderBy('product.product_number','asc')
                        ->Paginate(12);
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.slug')
                    ->where('product.discount','>',0)
                  ->where('product.status',1)
                  ->select('brand.name','brand.slug',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

      $ozel_urun = Category::where('title','Özel Ürünler')->first();
      $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      return view('front.product.indirim',[
        'cat_products'  => $cat_products,
        'brands'        => $brands,
        'list'          => $list,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function indirimMarka($list_value)
    {
      $brand =  Brand::where('slug',$list_value)->where('status',1)->first();
      $cat_products = DB::table('product')
                    ->leftJoin('brand','brand.id','=','product.brand_id')
                    ->leftJoin('modek','modek.id','=','product.modek_id')
                    ->where('product.discount','>',0)
                    ->where('product.status',1)
                    ->where('brand.slug',$brand->slug)
                    ->select('product.*','brand.name as brand_name','modek.name as modek_name','brand.slug')
                    ->Paginate(12);

        $ozel_urun = Category::where('title','Özel Ürünler')->first();
        $ozel_urunlers = Product::where('category_id','like','%,'.$ozel_urun->id.',%')->where('status',1)->take(2)->get();

      // echo '<pre>';
      // print_r($cat_products);
      // die();

      return view('front.product.indirim_brand',[
        'cat_products'  => $cat_products,
        'ozel_urunlers' => $ozel_urunlers
      ]);
    }

    public function iptal_iade_degisim()
    {
      return view('front.giris.iptal_iade_degisim');
    }

    public function mesafeli_satis_sozlesmesi()
    {
      return view('front.giris.uzak_mesafeli_satis_sozlesmesi');
    }

    public function testP(Request $request)
    {
      $data = $request->all();
      echo "<pre>";
      print_r($data);
      die();
    }

    public function test()
    {
      /*@foreach($my_cart as $val)
    <?php 
    $kdv = ($val->product->out_price * 18)/100;
    $total += ($val->product->out_price * $val->quantity);
    ?>
    <input name="ORDER_PNAME[]" value="{{$val->product->product_number}}" type="hidden">    
    <input name="ORDER_PCODE[]" value="{{$val->product->product_number}}" type="hidden">    
    <input name="ORDER_PRICE[]" value="{{$val->product->out_price}}" type="hidden">
    <input name="ORDER_QTY[]" value="{{$val->quantity}}" type="hidden">
    <input name="ORDER_PRICE_TYPE[]" value="NET" type="hidden">
    <input name="ORDER_VAT[]" value="{{$kdv}}" type="hidden">
    @endforeach*/
      /*$deger = 'MERCHANT': 'OPU_TEST', 
                      'ORDER_REF': '21831832', 
                      'ORDER_DATE': '2018-03-28', 
                      'ORDER_PNAME[0]': 'Test Urun', 
                      'ORDER_PNAME[1]': 'Test Urun-2', 
                      'ORDER_PCODE[0]':'Test Urun Kodu-2', 
                      'ORDER_PCODE[1]':'Test Urun Kodu', 
                      'ORDER_PINFO[0]':'Test urun Aciklamasi-2', 
                      'ORDER_PINFO[1]':'Test urun Aciklamasi', 
                      'ORDER_PRICE[0]':'10', 
                      'ORDER_PRICE[1]':'20', 
                      'ORDER_QTY[0]':'1', 
                      'ORDER_QTY[1]':'2', 
                      'ORDER_VAT[0]':'18', 
                      'ORDER_VAT[1]':'18', 
                      'ORDER_SHIPPING':'5', 
                      'PRICES_CURRENCY': 'TRY', 
                      'PAY_METHOD': 'CCVISAMC', 
                      'ORDER_PRICE_TYPE[0]':'GROSS', 
                      'ORDER_PRICE_TYPE[1]':'NET', 
                      'SELECTED_INSTALLMENTS_NO': '1,2,3,4,5,6,7,8,9,10,11,12';*/

$bugun = date("Y-m-d h:i:s");
$sBugun = strlen($bugun);
echo $sBugun."<br>";
$secretKey="N8J!T4@1|n+h1|(~l#W9";
$input = "8GZLUKMEK20N8J!T4@1|n+h1|(~l#W9192018-06-11 15:51:3519MacBook Air 13 inch9iPhone 4S5MBA134IP4S27Extended Warranty - 5 Years0420006500.5011122242242503EUR21010București10București2RO8CCVISAMC5GROSS3NET4TRUE";
$output = hash_hmac('md5', $input, $secretKey);
      echo "<pre>";
      print_r($output);
      die();
    }

    public function odemeTest()
    {
      $sonParams = array();
      $pName = array();
      $pCode = array();
      $pInfo = array();
      $pPrice = array();
      $pQTY = array();
      $pVat = array();
      $pType = array();
      $user = Auth::user();
      $carts = Cart::where('user_id',$user->id)->get();
      foreach($carts as $key => $value)
      {
        $pName = array_merge($pName, array('ORDER_PNAME['.$key.']' => $value->product->product_number));
        $pCode = array_merge($pCode, array('ORDER_PCODE['.$key.']' => $value->product_id));
        $pInfo = array_merge($pInfo, array('ORDER_PINFO['.$key.']' => $value->product->quick_content));
        $pPrice = array_merge($pPrice, array('ORDER_PRICE['.$key.']' => $value->product->out_price));
        $pQTY = array_merge($pQTY, array('ORDER_QTY['.$key.']' => $value->quantity));
        $pVat = array_merge($pVat, array('ORDER_VAT['.$key.']' => "24"));
        $pType = array_merge($pType, array('ORDER_PRICE_TYPE['.$key.']' => "GROSS"));
      }
      $topParams = array(
         "MERCHANT" => "GZLUKMEK",
         "ORDER_REF" =>  "2322",
         "ORDER_DATE" => "2018-06-21 12:04:58",        
      );
      $midParams = array(
         "ORDER_SHIPPING"=>"0",
         "PRICES_CURRENCY" => "TRY",
         "DISCOUNT" => "0",
         "DELIVERY_CITY" => "Radauti",
         "DELIVERY_STATE" => "Suceava",
         "DELIVERY_COUNTRYCODE" => "RO",
         "PAY_METHOD" => "CCVISAMC",
      );
      $sonParams = array_merge($sonParams, $topParams, $pName, $pCode, $pInfo, $pPrice, $pQTY, $pVat, $midParams, $pType);
        $arParams = array(

         "MERCHANT" => "GZLUKMEK",
         "ORDER_REF" =>  "2322",
         "ORDER_DATE" => "2018-06-21 12:04:58",
         "ORDER_PNAME[0]" => "DVD Player",
         "ORDER_PCODE[0]" => "CDPLY",
         "ORDER_PINFO[0]" => "Incarcator inclus",
         "ORDER_PRICE[0]" => "3.50",
         "ORDER_QTY[0]" => "1",
         "ORDER_VAT[0]"=>"24",
         "ORDER_SHIPPING"=>"0",
         "PRICES_CURRENCY" => "TRY",
         "DISCOUNT" => "0",
         "DELIVERY_CITY" => "Radauti",
         "DELIVERY_STATE" => "Suceava",
         "DELIVERY_COUNTRYCODE" => "RO",
         "PAY_METHOD" => "CCVISAMC",
         "ORDER_PRICE_TYPE[0]"=>"GROSS",
      );
      echo "<pre>";
      print_r($arParams);
      echo "</pre>";
      echo "<pre>";
      print_r($sonParams);
      echo "</pre>";
      /*echo "<pre>";
      print_r($pCode);
      echo "</pre>";
      echo "<pre>";
      print_r($pInfo);
      echo "</pre>";
      echo "<pre>";
      print_r($pPrice);
      echo "</pre>";
      echo "<pre>";
      print_r($pQTY);
      echo "</pre>";
      echo "<pre>";
      print_r($pVat);
      echo "</pre>";
      echo "<pre>";
      print_r($pType);
      echo "</pre>";*/
      die();
    }

    public function odemeCTest()
    {
$url = "https://secure.payu.com.tr/order/alu/v3";
date_default_timezone_set('UTC');

$secretKey = 'N8J!T4@1|n+h1|(~l#W9';

$arParams = array(

   "MERCHANT" => "GZLUKMEK",
   "LANGUAGE" => "TR",
   "ORDER_REF" =>  rand(1, 10000),
   "ORDER_DATE" => date('Y-m-d H:i:s'),
   "PAY_METHOD" => "CCVISAMC",
   "BACK_REF" => "http://www.backref.com.tr",
   "PRICES_CURRENCY" => "TRY",
   "SELECTED_INSTALLMENTS_NUMBER" => "1",
   "ORDER_SHIPPING"=>"5",
   "CLIENT_IP" => $_SERVER["REMOTE_ADDR"],


   "ORDER_PNAME[0]" => "Test Ürünü",
   "ORDER_PCODE[0]" => "Test Kodu",
   "ORDER_PINFO[0]" => "Test Açıklaması",
   "ORDER_PRICE[0]" => "5",
   "ORDER_VAT[0]"=>"18",
   "ORDER_PRICE_TYPE[0]"=>"NET",
   "ORDER_QTY[0]" => "1",

   "ORDER_PNAME[1]" => "Test Ürünü-2",
   "ORDER_PCODE[1]" => "Test Kodu-2",
   "ORDER_PINFO[1]" => "Test Açıklaması-2",
   "ORDER_PRICE[1]" => "15",
   "ORDER_VAT[1]"=>"24",
   "ORDER_PRICE_TYPE[1]"=>"GROSS",
   "ORDER_QTY[1]" => "3",

   "CC_NUMBER" => "4355084355084358",
   "EXP_MONTH" => "12",
   "EXP_YEAR" => "2018",
   "CC_CVV" => "000",
   "CC_OWNER" => "000",

   "BILL_FNAME" => "Ad",
   "BILL_LNAME" => "Soyad",
   "BILL_EMAIL" => "mail@mail.com",
   "BILL_PHONE" => "02129003711",
   "BILL_FAX" => "02129003711",
   "BILL_ADDRESS" => "Birinci Adres satırı",
   "BILL_ADDRESS2" => "İkinci Adres satırı",
   "BILL_ZIPCODE" => "34000",
   "BILL_CITY" => "ISTANBUL",
   "BILL_COUNTRYCODE" => "TR",
   "BILL_STATE" => "Ayazağa",

   "DELIVERY_FNAME" => "Ad",
   "DELIVERY_LNAME" => "Soyad",
   "DELIVERY_EMAIL" => "mail@mail.com",
   "DELIVERY_PHONE" => "02129003711",
   "DELIVERY_COMPANY" => "PayU Ödeme Kuruluşu A.Ş.",
   "DELIVERY_ADDRESS" => "Birinci Adres satırı",
   "DELIVERY_ADDRESS2" => "İkinci Adres satırı",
   "DELIVERY_ZIPCODE" => "34000",
   "DELIVERY_CITY" => "ISTANBUL",
   "DELIVERY_STATE" => "TR",
   "DELIVERY_COUNTRYCODE" => "Ayazağa",
);

ksort($arParams);
$hashString = "";
foreach ($arParams as $key => $val) {
    $hashString .= strlen($val) . $val;
}
$arParams["ORDER_HASH"] = hash_hmac("md5", $hashString, $secretKey);
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arParams));
$response = curl_exec($ch);

$curlerrcode = curl_errno($ch);
$curlerr = curl_error($ch);

if (empty($curlerr) && empty($curlerrcode)) {
    $parsedXML = @simplexml_load_string($response);
    if ($parsedXML !== FALSE) {
        echo "<pre>";
        print_r($response);
        echo "</pre>";

        $payuTranReference = $parsedXML->REFNO;

        if ($parsedXML->STATUS == "SUCCESS") {


            if (($parsedXML->RETURN_CODE == "3DS_ENROLLED") && (!empty($parsedXML->URL_3DS))) {
                header("Location:" . $parsedXML->URL_3DS);
            }

            echo "<pre>";
            echo "SUCCES [PayU reference number: " . $payuTranReference . "]";
            echo "</pre>";

        } else {
            echo "Error: " . $parsedXML->RETURN_MESSAGE . " [" . $parsedXML->RETURN_CODE . "]";
            echo "<br>";
            echo "Error Details: " . $parsedXML->ERRORMESSAGE . "<br> Error Code: [" . $parsedXML->PROCRETURNCODE . "]";
            if (!empty($payuTranReference)) {
                //the transaction was register to PayU system, but some error occured during the bank authorization.
                //See $parsedXML->RETURN_MESSAGE and $parsedXML->RETURN_CODE for details
                echo "<pre>";
                echo " [PayU reference number: " . $payuTranReference . "]";
                echo "</pre>";

            }
        }
    }
} else {
    echo "cURL error: " . $curlerr;

}

    }
}
