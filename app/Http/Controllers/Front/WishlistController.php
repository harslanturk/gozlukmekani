<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use App\Wishlist;
use App\User;
use App\Product;
use App\Customer;
use App\City;
use App\Region;
use App\Address;

class WishlistController extends Controller
{
    public function addWishlist(Request $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $data['status']  = 1;

      $wishlist = Wishlist::where('user_id',$data['user_id'])->where('product_id',$data['product_id'])->first();
      if ($wishlist) {
        Wishlist::where('user_id',$data['user_id'])->where('product_id',$data['product_id'])->delete();
        return 2;
      }else {
        try{
          Wishlist::create($data);
          return 1;
        }catch(\Exception $e){
          return $e->getMessage();
        }
      }
      // echo '<pre>';
      // print_r($data);
      // die();

    }

    public function wishlistIndex()
    {
      $wishlists = Wishlist::where('user_id',Auth::user()->id)->where('status',1)->get();
      return view('front.myaccount.wishlist',[
        'wishlists' => $wishlists
      ]);
    }

    public function deleteWishlist($id)
    {
      Wishlist::where('id',$id)->delete();
      return redirect()->back();
    }

    public function myaccount()
    {
      $user_id  = Auth::user()->id;
      $user     = User::where('id',$user_id)->first();
      $customer = Customer::where('user_id',$user_id)->first();
      // echo '<pre>';
      // print_r($user_id);
      // die();
      return view('front.myaccount.index',[
        'user'      => $user,
        'customer'  => $customer
      ]);
    }

    public function myaccountSave(Request $request)
    {
      $data = $request->all();
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        $user = User::where('id',$data['id'])->first();
        if (empty($data['password'])) {
          $data['password'] = $user->password;
        }else {
          $data['password'] = bcrypt($data['password']);
        }
          User::where('id',$data['id'])->update([
            'name'      => $data['name'],
            'surname'   => $data['surname'],
            'password'  => $data['password'],
            'email'  => $data['email']
          ]);
          Customer::where('user_id',$data['id'])->update([
            'gsm'       => $data['gsm'],
            'tc_no'     => $data['tc_no'],
            'birthdate' => $data['birthdate'],
            'gender'    => $data['gender']
          ]);
          Session::flash('success', 'Bilgileriniz Başarı İle Değiştirildi.');
          return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }

    }

    public function adres()
    {
      $user_id = Auth::user()->id;
      $adress = Address::where('customer_id',$user_id)->get();
      return view('front.myaccount.adres',[
        'adress' => $adress
      ]);
    }

    public function createAdres()
    {
      $citys = City::where('status',1)->orderBy('name','asc')->get();
      return view('front.myaccount.create_adres',[
        'citys' => $citys
      ]);
    }

    public function editAdres(Request $request)
    {
      $data = $request->all();
      $adres = Address::where('id',$data['id'])->first();
      $citys = City::where('status',1)->orderBy('name','asc')->get();
      return view('front.myaccount.editAdres',[
        'adres' => $adres,
        'citys' => $citys
      ]);
    }

    public function updateAdres(Request $request)
    {
      $data = $request->all();

      if ($data['address_type'] == 2) {
        $data['company'] = '';
        $data['vergi_no'] = 0;
      }
      try{
        unset($data['_token']);
          Address::where('id',$data['id'])->update($data);
          Session::flash('success', 'Adres Bilgileriniz Başarı İle Güncellenmiştir.');
          return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function adresSave(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      if ($data['address_type'] == 2) {
        $data['company'] = '';
        $data['vergi_no'] = 0;
      }
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
          Address::create($data);
          Session::flash('success', 'Adres Bilgileriniz Başarı İle Kaydedildi.');
          return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function deleteAdres($id)
    {
      try{
          Address::destroy($id);
          Session::flash('success', 'Adres Başarı İle Silindi.');
          return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
}
