<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use App\Helpers\helper;
use App\Product;
use App\Brand;
use App\Modek;
use App\Color;
use App\Gender;
use App\Quality;
use App\Glass;
use App\ProductImage;
use App\Category;
use App\Type;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function index()
    {
      $products = Product::where('status',1)->get();

      return view('admin.product.index',[
        'products' => $products
      ]);
    }
    public function create()
    {
      $modeks = Modek::where('status',1)->get();
      $brands = Brand::where('status',1)->get();
      $colors = Color::where('status',1)->get();
      $genders = Gender::where('status',1)->get();
      $qualitys = Quality::where('status',1)->get();
      $glass = Glass::where('status',1)->get();
      $type = Type::where('name','like','%Ürün%')->first();
      $categorys = Category::where('type_id',$type->id)->get();
      return view('admin.product.create',[
        'brands'    => $brands,
        'modeks'    => $modeks,
        'colors'    => $colors,
        'genders'   => $genders,
        'qualitys'  => $qualitys,
        'glass'     => $glass,
        'categorys' => $categorys
      ]);
    }

    public function save(Request $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $data['status'] = 1;
      $data['name'] = '';
      $veri = '';

      if (empty($data['modek_id'])) {
        $data['modek_id'] = 0;
      }
      if (empty($data['category_id'])) {
        $data['category_id'] = ',0,';
      }else {
        foreach ($data['category_id'] as $key => $value) {
          $veri .= ','.$data['category_id'][$key];
        }
        $data['category_id'] = $veri.',';
      }

      if (empty($data['new'])) {
        $data['new'] = 0;
      }
      if (empty($data['discount'])) {
        $data['discount'] = 0.00;
      }
      if (empty($data['quality_id'])) {
        $data['quality_id'] = 0;
      }
      // echo '<pre>';
      // print_r($data);
      // die();

      try{
        $last_id = Product::create($data);
        Session::flash('success', 'Ürün Oluşturuldu');
        return redirect('/admin/product/edit/'.$last_id->id);
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function edit($id)
    {
      $product = Product::where('id',$id)->first();

        $modeks = Modek::where('status',1)->get();
        $brands = Brand::where('status',1)->get();
        $colors = Color::where('status',1)->get();
        $genders = Gender::where('status',1)->get();
        $qualitys = Quality::where('status',1)->get();
        $glass = Glass::where('status',1)->get();
        $product_images = ProductImage::where('product_id',$id)->where('status',1)->orderBy('id','asc')->get();
        $type = Type::where('name','like','%Ürün%')->first();
        $categorys = Category::where('type_id',$type->id)->get();
        return view('admin.product.edit',[
          'brands'    => $brands,
          'modeks'    => $modeks,
          'colors'    => $colors,
          'genders'   => $genders,
          'qualitys'  => $qualitys,
          'glass'     => $glass,
          'product'   => $product,
          'product_images' => $product_images,
          'categorys' => $categorys
        ]);
    }

    public function update(Request $request)
    {
      $data = $request->all();
      unset($data['_token']);
      $veri = '';

      if (empty($data['category_id'])) {
        $data['category_id'] = ',0,';
      }else {
        foreach ($data['category_id'] as $key => $value) {
          $veri .= ','.$data['category_id'][$key];
        }
        $data['category_id'] = $veri.',';
      }
      if (empty($data['new'])) {
        $data['new'] = 0;
      }
      if (empty($data['discount'])) {
        $data['discount'] = 0.00;
      }
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Product::where('id',$data['id'])->update($data);
        Session::flash('success', 'Ürün Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function delete($id)
    {
      try{
        Product::where('id',$id)->update([
          'status' => 0
        ]);
        Session::flash('success', 'Ürün Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function product_image(Request $request)
    {
      $images = $request->file('image');
      foreach ($images as $key => $image) {
        $name = $image->getClientOriginalName();
        $destinationPath = public_path('/img/product/');
        $image->move($destinationPath,$name);

        $image = new ProductImage();
        $image->user_id = Auth::user()->id;
        $image->product_id = $request->input('product_id');
        $image->image = '/img/product/'.$name;
        $image->status = 1;
        $image->save();
      }
      // echo '<pre>';
      // print_r($images);
      // die();

      return redirect()->back();
    }

    public function product_imageDelete(Request $request)
    {
      $data = $request->all();
      $image = ProductImage::where('id',$data['id'])->where('product_id',$data['product_id'])->first();
      // echo '<pre>';
      // print_r($image);
      // die();
      if(File::exists(public_path($image->image))){
          File::delete(public_path($image->image));
          ProductImage::destroy($data['id']);
          return 1;
      }
    }
}
