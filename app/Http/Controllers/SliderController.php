<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use App\Slider;
use Illuminate\Support\Facades\File;

class SliderController extends Controller
{
    public function index()
    {
      $sliders = Slider::where('status',1)->get();
      return view('admin.define.slider.index',[
        'sliders' => $sliders
      ]);
    }

    public function create()
    {
      return view('admin.define.slider.create');
    }

    public function save(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      $data['user_id'] = Auth::user()->id;
        $image = $request->file('image');
        if ($image) {
          $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

          $destinationPath = public_path('/front/images/slider/');
          $image->move($destinationPath,$input['imagename']);

          $data['image'] = '/front/images/slider/'.$input['imagename'];
        }

      try {
        Slider::create($data);
        $sliders = Slider::all();
        return redirect('admin/define/slider/index',[
            'sliders' => $sliders
        ]);
      } catch (\Exception $e) {
        Session::flash('error',$e->getMessage());
        return redirect()->back();
      }
    }

    public function edit($id)
    {
      $slider = Slider::where('id',$id)->first();
      return view('admin.define.slider.edit',[
        'slider' => $slider
      ]);
    }

    public function imageDelete(Request $request)
    {
      $data = $request->all();
      $image = Slider::where('id',$data['id'])->first();
      // echo '<pre>';
      // print_r($image);
      // die();
      if(File::exists(public_path($image->image))){
          File::delete(public_path($image->image));
          Slider::where('id',$data['id'])->update(['image' => '']);
          return 1;
      }
    }

    public function update(Request $request)
    {
      $data = $request->all();
      unset($data['_token']);
      try{
        $image = $request->file('image');
        if ($image) {
          $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

          $destinationPath = public_path('/front/images/slider/');
          $image->move($destinationPath,$input['imagename']);

          $data['image'] = '/front/images/slider/'.$input['imagename'];
        }
        Slider::where('id',$data['id'])->update($data);
        Session::flash('success', 'Ürün Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function delete($id)
    {
      try{
        Slider::where('id',$id)->update([
          'status' => 0
        ]);
        Session::flash('success', 'Slider Silindi');
        return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }
}
