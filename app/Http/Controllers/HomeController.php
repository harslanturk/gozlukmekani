<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Comment;

class HomeController extends Controller
{
    public function index()
    {
        $comments = Comment::where('status',2)->get();
        return view('admin.home',[
          'comments' => $comments
        ]);

    }
}
