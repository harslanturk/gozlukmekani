<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuthGroup;
use App\Module;
use App\Authorization;
use DB;

class AuthorizationController extends Controller
{
    public function index()
    {
      $groups = AuthGroup::where('status',1)->get();
      return view('admin.authorization.index',[
        'groups' => $groups
      ]);
    }
    public function deleteAuth(Request $request,$id)
    {
      $data = $request->all();
      AuthGroup::where('id',$id)->update(['status' => 0]);
      return 1;
    }
    public function editAuthorization($id)
    {
      $group = AuthGroup::where('id',$id)->first();
      $old_auths = DB::table('module')
                  ->leftJoin(DB::raw('(SELECT * FROM authorization WHERE auth_group_id = '.$id.') authorization'),'module.id','=','authorization.modul_id')
                   ->orderBy('authorization.modul_id','asc')
                   ->select('authorization.*','module.name','module.id as m_id')
                   ->get();
      $new_auths = Module::where('status',1)->get();
// echo '<pre>';
// print_r($new_auths);
// die();
      return view('admin.authorization.edit',[
        'old_auths' => $old_auths,
        'group' => $group,
        'new_auths' => $new_auths
      ]);
    }
    public function updateAuthGroup(Request $request,$id)
    {
      $data = $request->all();
      Authorization::where('auth_group_id',$data['auth_group_id'])->delete();

      foreach ($data['read'] as $key => $dat) {

        $new_auth = new Authorization();
        $new_auth->user_id = $data['user_id'];
        $new_auth->modul_id = $key;
        $new_auth->auth_group_id = $data['auth_group_id'];
        $new_auth->read = $data['read'][$key];
        $new_auth->add = $data['add'][$key];
        $new_auth->update = $data['update'][$key];
        $new_auth->delete = $data['delete'][$key];
        $new_auth->status = 1;
        $new_auth->save();

      }
        return redirect()->back();
      // echo '<pre>';
      // print_r($data);
      // die();
    }
}
