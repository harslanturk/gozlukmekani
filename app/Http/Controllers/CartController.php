<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Product;
use App\Address;
use App\Customer;
use DB;
use Session;
use Auth;
use App\Helpers\helper;

class CartController extends Controller
{
    public function addtoCart(Request $request)
    {
      $data = $request->all();
      if (isset($data['quantity'])) {
        $adet = $data['quantity'];
      }else{
        $adet = 1;
      }
      $data['user_id'] = Auth::user()->id;

      $cart = Cart::where('user_id',$data['user_id'])
                  ->where('product_id',$data['product_id'])
                  ->first();
      if ($cart) {
        $cart->increment('quantity',$adet);
      }else {
        $data['quantity'] = $adet;
        Cart::create($data);
      }
      return 1;
    }
    public function gettoCart()
    {
      $user_id = Auth::user()->id;
        echo '<div class="mini-cart" id="mini-cart">
            <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#">
              <div class="cart-icon"><i class="fa fa-shopping-cart"></i></div>
              <div class="shoppingcart-inner hidden-xs hidden-sm"><span class="cart-title">Sepetim</span>
                ';
                  if (!empty($user_id)) {
                    $carts = Cart::where('user_id',$user_id)->get();
                    $my_cart_count = $carts->count();
                    $total = 0;
                    foreach ($carts as $key => $value) {
                      $total += ($value->product->out_price * $value->quantity);
                    }
                  }else {
                    $carts = '';
                    $my_cart_count = 0;
                    $total = 0;
                  }
                echo '<span class="cart-total">';
                  if($total != 0){
                    echo $my_cart_count.' Ürün '.$total.'<i class="fa fa-try"></i>';
                  }else{
                    echo 'Alışverişe Başla';
                  }
                  echo '</span></div>
              </a></div>
            <div>
              <div class="top-cart-content">
                <div class="block-subtitle hidden-xs">Sepetinizdeki Ürünler</div>
                <ul id="cart-sidebar" class="mini-products-list">';
                  if($carts){
                    foreach($carts as $cart){
                      $cover = helper::getImageCover($cart->product_id);
                    echo '<li class="item odd">
                      <a href="shopping_cart.html" title="'.$cart->product->product_number.'" class="product-image">
                        <img src="'.$cover->image.'" alt="'.$cart->product->product_number.'" width="65">
                      </a>
                      <div class="product-details">
                        <a href="#" title="Ürünü Sil" onclick="sepetSil('.$cart->id.');" class="remove-cart"><i class="icon-close"></i></a>
                        <p class="product-name"><a href="shopping_cart.html">'.$cart->product->product_number.'</a> </p>
                        <strong>'.$cart->quantity.'</strong> x <span class="price">'.$cart->product->out_price.'</span>
                      </div>
                    </li>';
                    }
                  }
                echo '</ul>';
                if($total){
                  echo '<div class="top-subtotal">Toplam: <span class="price">'.$total.' <i class="fa fa-try"></i></span></div>
                  <div class="actions">
                    <form class="" action="/myaccount/cart" method="get">
                    <button class="btn-checkout" type="submit"><i class="fa fa-check"></i><span>Satın Al</span></button>
                    <button class="view-cart" type="submit"><i class="fa fa-shopping-cart"></i> <span>Sepete Git</span></button>
                    </form>
                  </div>';
                }
                else{
                  echo '<div class="top-subtotal" style="text-align:center;color:purple;">Sepetinizde Ürün Yok</span></div>
                  <div class="actions" style="text-align:center;">
                    <button class="btn-checkout" type="button"><i class="fa fa-shopping-cart"></i><span>Alışverişe Başla</span></button>
                  </div>';
                }
              echo '</div>
            </div>
          </div>';
    }

    public function deleteCart(Request $request)
    {
      $data = $request->all();
      $id = $data['id'];
      try {
        Cart::destroy($id);
        return 1;
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }

    public function myCart()
    {
      $user_id = Auth::user()->id;
      $carts = Cart::where('user_id',$user_id)->get();
      return view('front.myaccount.cart',[
        'carts' => $carts
      ]);
    }

    public function deletegetCart($id)
    {
      try {
        Cart::destroy($id);
        return redirect()->back();
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }

    public function updateQtyCart(Request $request)
    {
      $data = $request->all();

      try {
        if ($data['increment'] == 1) {
          Cart::where('id',$data['cart_id'])->increment('quantity');
        }else {
          Cart::where('id',$data['cart_id'])->decrement('quantity');
        }
        return redirect()->back();
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }

    public function getQtyCart()
    {
      $user_id = Auth::user()->id;
      $carts = Cart::where('user_id',$user_id)->get();
      echo '<table class="table table-bordered cart_summary" id="sepet">
          <thead>
            <tr>
              <th class="cart_product">Ürün Resmi</th>
              <th>Ürün Adı</th>
              <th class="col-sm-2">Birim Fiyat</th>
              <th class="col-sm-3">Adet</th>
              <th class="col-sm-2">Toplam</th>
              <th  class="action col-sm-1"><i class="fa fa-trash-o"></i></th>
            </tr>
          </thead>
          <tbody>';
            $total = 0;
            $total_dis = 0;
            $discount = 0;

            foreach($carts  as $cart){
              $cover = helper::getImageCover($cart->product_id);
              $total += ($cart->quantity * $cart->product->out_price);
              $discount = $cart->product->discount;
              if ($discount > 0) {
                $total_dis += ((($cart->quantity * $cart->product->out_price)*$cart->product->discount)/100);
              }
            echo '<tr>
              <td class="cart_product"><a href="#"><img src="'.$cover->image.'" alt="'.$cart->product_number.'"></a></td>
              <td class="cart_description"><p class="product-name"><a href="#">'.$cart->product->product_number.'</a></p>
              <td class="price"><span>'.$cart->product->out_price.' <i class="fa fa-try"></i> </span></td>
              <td>
                <div class="cart-plus-minus">
                  <div class="numbers-row">
                    <div onclick="deincrement('.$cart->id.');" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                      <input type="text" class="qty" title="Adet" value="'.$cart->quantity.'" maxlength="12" name="qty" readonly>
                    <div onclick="increment('.$cart->id.');" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                  </div>
                </div>
              </td>
              <td class="price"><span class="'.$cart->id.' urun_toplam">'.($cart->quantity * $cart->product->out_price).' <i class="fa fa-try"></i></span></td>
              <td class="action"><a href="/sepet-urun-sil/'.$cart->id.'"><i class="icon-close"></i></a></td>
            </tr>';
          }
          echo '</tbody>
          <tfoot>
            <tr>
              <td colspan="4" class="text-right"><strong style="color:#702376;">Genel Toplam</strong></td>
              <td colspan="2" class="text-center"><strong style="color:#702376;" id="total"> '.($total - $total_dis).' <i class="fa fa-try"></i></strong></td>
            </tr>
          </tfoot>
        </table>';
    }

    public function teslimatBilgileri()
    {
      $user_id = Auth::user()->id;
      try {
        $adress = Address::where('customer_id',$user_id)->get();
        $carts = Cart::where('user_id',$user_id)->get();
        return view('front.myaccount.teslimat_bilgileri',[
          'carts' => $carts,
          'adress' => $adress
        ]);
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }

    public function onBilgilendirme()
    {
      $customer_id = Auth::user()->id;
      $carts = Cart::where('user_id',$customer_id)->get();
      return view('front.myaccount.on_bilgilendirme',[
        'carts' => $carts
      ]);
    }

    public function mesafeliSatis()
    {
      $customer_id = Auth::user()->id;
      $carts = Cart::where('user_id',$customer_id)->get();
      $customer = Customer::where('user_id',$customer_id)->first();
      $adres = Address::where('customer_id',$customer_id)->first();
      return view('front.myaccount.mesafeli_satis',[
        'carts' => $carts,
        'customer' => $customer,
        'adres' => $adres
      ]);
    }

    public function siparisBilgileri(Request $request)
    {
      $data = $request->all();
     /* echo "<pre>";
      print_r($data);
      die();*/
      $user_id = Auth::user()->id;
      try {
        $adress = Address::where('customer_id',$user_id)->get();
        $carts = Cart::where('user_id',$user_id)->get();
        return view('front.myaccount.siparis_bilgileri',[
          'carts' => $carts,
          'adress' => $adress
        ]);
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }
}
