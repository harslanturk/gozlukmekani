<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Color;
use Auth;
use DB;
use App\Gender;
use App\Quality;
use App\Glass;

class DefineController extends Controller
{
    public function index()
    {
      $colors = Color::where('status',1)->get();
      return view('admin.define.index',[
        'colors' => $colors
      ]);
    }

    public function colorCreate(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      $data['user_id'] = Auth::user()->id;
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Color::create($data);
        Session::flash('success', 'Renk Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function colorEdit(Request $request)
    {
      $data = $request->all();
      try{
        $color = Color::where('id',$data['id'])->first();
        return view('admin.define.renkModal',[
          'color' => $color
        ]);
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function colorUpdate(Request $request)
    {
      $data = $request->all();
      unset($data['_token']);
      try{
        Color::where('id',$data['id'])->update($data);
        Session::flash('success', 'Renk Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function colorDelete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Color::where('id',$id)->update(['status' => 0]);
        Session::flash('success', 'Silme Başarılı');
        return 1;
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function genderIndex($value='')
    {
      $genders = Gender::where('status',1)->get();
      return view('admin.define.gender',[
        'genders' => $genders
      ]);
    }

    public function genderCreate(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      $data['user_id'] = Auth::user()->id;
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Gender::create($data);
        Session::flash('success', 'Cinsiyet Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function genderEdit(Request $request)
    {
      $data = $request->all();
      try{
        $gender = Gender::where('id',$data['id'])->first();
        return view('admin.define.genderModal',[
          'gender' => $gender
        ]);
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function genderUpdate(Request $request)
    {
      $data = $request->all();
      unset($data['_token']);
      try{
        Gender::where('id',$data['id'])->update($data);
        Session::flash('success', 'Cinsiyet Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function genderDelete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Gender::where('id',$id)->update(['status' => 0]);
        Session::flash('success', 'Silme Başarılı');
        return 1;
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function qualityIndex($value='')
    {
      $qualitys = Quality::where('status',1)->get();
      return view('admin.define.quality',[
        'qualitys' => $qualitys
      ]);
    }

    public function qualityCreate(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      $data['user_id'] = Auth::user()->id;
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Quality::create($data);
        Session::flash('success', 'Kalite Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function qualityEdit(Request $request)
    {
      $data = $request->all();
      try{
        $quality = Quality::where('id',$data['id'])->first();
        return view('admin.define.qualityModal',[
          'quality' => $quality
        ]);
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function qualityUpdate(Request $request)
    {
      $data = $request->all();
      unset($data['_token']);
      try{
        Quality::where('id',$data['id'])->update($data);
        Session::flash('success', 'Kalite Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function qualityDelete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Quality::where('id',$id)->update(['status' => 0]);
        Session::flash('success', 'Silme Başarılı');
        return 1;
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function glassIndex($value='')
    {
      $glass = Glass::where('status',1)->get();
      return view('admin.define.glass',[
        'glass' => $glass
      ]);
    }

    public function glassCreate(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      $data['user_id'] = Auth::user()->id;
      // echo '<pre>';
      // print_r($data);
      // die();
      try{
        Glass::create($data);
        Session::flash('success', 'Gözlük Camı Oluşturuldu');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function glassEdit(Request $request)
    {
      $data = $request->all();
      try{
        $glass = Glass::where('id',$data['id'])->first();
        return view('admin.define.glassModal',[
          'glass' => $glass
        ]);
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
    }

    public function glassUpdate(Request $request)
    {
      $data = $request->all();
      unset($data['_token']);
      try{
        Glass::where('id',$data['id'])->update($data);
        Session::flash('success', 'Gözlük Camı Güncellendi');
        return redirect()->back();
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }

    public function glassDelete(Request $request,$id)
    {
      $data = $request->all();

      try{
        Glass::where('id',$id)->update(['status' => 0]);
        Session::flash('success', 'Silme Başarılı');
        return 1;
      }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
      }
    }
}
