<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\User;
use App\Customer;
use App\Helpers\helper;
use App\Mail\Welcome;
use App\Mail\SepetMail;
use App\Cart;

class CustomerController extends Controller
{
  public function register(Request $request)
  {

  try {
    $data = $request->all();
    $data['password']       = bcrypt($data['password']);
    $data['delegation_id']  = 0;
    $data['status']         = 1;
    $data['admin']          = 2;

    $user = User::create($data);
    $customer['user_id']    = $user->id;
    $customer['mail_code']  = str_random(15);
    $customer['status']     = 2;  //Mail Onayı Bekleniyor

    $user['name'] = $data['name'];
    $user['surname'] = $data['surname'];
    $user['email'] = $data['email'];
    $user['password'] = $data['password'];

    Customer::create($customer);
    Session::flash('success','Lütfen Mail Adresinize Gönderilen Onay Koduna Tıklayınız !');
    \Mail::to($user)->send(new Welcome($user));
    return redirect()->back();
    } catch (\Exception $e) {
      Session::flash('error',$e->getMessage());
      return redirect()->back();
    }

  }

  public function emailActivation($code)
  {
    $code_control = Customer::where('mail_code',$code)->first();
    if ($code_control) {
     $code_control->status = 1;
     $code_control->save();
     Session::flash('email-ok','Mailiniz Onaylanmıştır Giriş Yapabilirsiniz');
     return redirect()->to('/login');
   }else {
     Session::flash('email-error','E-Mail Kodunuz Onaylanmamıştır.Lütfen E-Maildeki Linke Tıklayarak Giriş Yapınız.');
     return redirect()->to('/login');
   }
  }

  public function index()
  {
    $customers = Customer::where('status',1)->get();
    return view('admin.customer.index',[
      'customers' => $customers
    ]);
  }

  public function delete($id)
  {
    try{
      Customer::where('id',$id)->update([
        'status' => 0
      ]);
      Session::flash('success', 'Müşteri Silindi');
      return redirect()->back();
    }catch(\Exception $e){
        Session::flash('error', $e->getMessage());
        return redirect()->back();
    }
  }

  public function cartView(Request $request)
  {
    $data = $request->all();

    $carts = Cart::where('user_id',$data['id'])->get();
    return view('admin.customer.cart',[
      'carts' => $carts,
      'user_id' => $data['id']
    ]);
  }

  public function cartEmail(Request $request)
  {
        $data     = $request->all();
        $user_id  = $data['user_id'];
      try{
          $cart    = Cart::where('user_id',$user_id)->take(2)->get();
          $user     = User::where('id',$user_id)->first();
          Session::flash('success','Müşteriye Sepetteki Ürünleri Başarı ile Gönderildi!');
          \Mail::to($user)->send(new SepetMail($cart));
          return redirect()->back();
      }catch(\Exception $e){
          Session::flash('error', $e->getMessage());
          return redirect()->back();
      }
  }
}
