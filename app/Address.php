<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    protected $fillable = [
      'customer_id','address_type','name','vergi_no','company','city_id','region_id','zip_code','adres','status'
    ];

    public function customer()
    {
      return $this->belongsTo('App\Customer');
    }

    public function city()
    {
      return $this->belongsTo('App\City');
    }

    public function region()
    {
      return $this->belongsTo('App\Region');
    }
}
