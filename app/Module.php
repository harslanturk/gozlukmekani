<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'module';
    protected $fillable = [
        'user_id','name','slug','menu','url','icon','parent','parent_id','ranking','status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
