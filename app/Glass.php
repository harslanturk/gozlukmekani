<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Glass extends Model
{
    protected $table = 'glass';
    protected $fillable = [
      'user_id','name','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
