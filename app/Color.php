<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'color';
    protected $fillable = [
      'user_id','name','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
