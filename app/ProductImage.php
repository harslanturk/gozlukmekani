<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_image';
    protected $fillable = [
      'product_id','user_id','image','status'
    ];

    public function product()
    {
      return $this->belongsTo('App\Product');
    }
    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
