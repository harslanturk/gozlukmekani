<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'city';
    protected $fillable = [
      'city_id','name','status'
    ];

    public function city()
    {
      return $this->belongsTo('App\City');
    }
}
