<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'type';
    protected $fillable = [
      'user_id','name','status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
