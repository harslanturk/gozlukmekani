<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    protected $table = 'authorization';
    protected $fillable = [
      'user_id','modul_id','auth_group_id','read','add','update','delete','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
