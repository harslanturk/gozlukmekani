<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'gender';
    protected $fillable = [
      'user_id','name','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
