<?php

namespace App\Helpers;
use App\Delegation;
use App\User;
use Carbon\Carbon;
use Session;
use App\Module;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\Slider;
use App\ProductImage;
use App\Wishlist;
use App\Comment;
use App\Cart;
use App\Brand;


class Helper
{
    // Zaman farkını türkçe olarak geri döndürme.
    public static function trDiff($tarih)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->diffForHumans();
        return $result;
    }

    public static function dmYHi($tarih,$format)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->format($format);
        return $result;
    }

    public static function get_modules()
    {
        $modules = Module::where('status','1')
                        ->where('parent_id','0')
                        ->orderBy('ranking','asc')
                        ->get();

        return $modules;
    }

    public static function get_par_modules()
    {
        $par_modules = Module::where('status','1')
                        ->where('parent_id','>','0')
                        ->orderBy('ranking','asc')
                        ->get();

        return $par_modules;
    }
    public static function getMenuUst($id)
    {
      $del_id = User::where('id',$id)->first();
      $menu = DB::table('authorization')
                ->join('module','module.id','=','authorization.modul_id')
                ->where('auth_group_id',$del_id->delegation_id)
                ->where('module.menu',1)
                ->where('module.status',1)
                ->where('module.parent_id',0)
                ->where('authorization.status',1)
                ->orderBy('module.ranking','asc')
                ->select('authorization.*','module.*')
                ->get();
                return $menu;
    }
    public static function getMenuAlt($id)
    {
      $del_id = User::where('id',$id)->first();
      $menu = DB::table('authorization')
                ->join('module','module.id','=','authorization.modul_id')
                ->where('auth_group_id',$del_id->delegation_id)
                ->where('module.parent_id','>',0)
                ->orderBy('module.ranking','asc')
                ->select('authorization.*','module.*')
                ->get();
                // echo '<pre>';
                // print_r($menu);
                // die();
                return $menu;
    }

    public static function tr_slug($slug)
    {
      $bul = ['Ç', 'Ş', 'Ğ', 'Ü', 'İ', 'Ö', 'ç', 'ş', 'ğ', 'ü', 'ö', 'ı', '+', '#'];
      $degistir = ['c', 's', 'g', 'u', 'i', 'o', 'c', 's', 'g', 'u', 'o', 'i', 'plus', 'sharp'];

      $slug = strtolower(str_replace($bul, $degistir, $slug));
      $slug = preg_replace("@[^A-Za-z0-9\-_\.\+]@i", ' ', $slug);
      $slug = trim(preg_replace('/\s+/', ' ', $slug));
      $slug = str_replace(' ', '-', $slug);
      return $slug;
    }

    public static function count_user()
    {
      $c_user = User::where('status',1)->count();
      return $c_user;
    }

    public static function count_product()
    {
      $c_product = Product::where('status',1)->count();
      return $c_product;
    }

    public static function parent_id($id)
    {
      $parent = Category::where('status',1)->where('id',$id)->first();

        return $parent;
    }

    public static function getSlider()
    {
      $slider = Slider::where('status',1)->get();

      return $slider;
    }

    public static function getImageCover($product_id)
    {
      $image = ProductImage::where('product_id',$product_id)->orderBy('id','asc')->first();

      return $image;
    }

    public static function getWishlist($user_id,$product_id)
    {
      $wishlist = Wishlist::where('user_id',$user_id)
                          ->where('product_id',$product_id)
                          ->first();

      return $wishlist;
    }

    public static function getComment($product_id,$user_id)
    {
      $comment = Comment::where('product_id',$product_id)
                        ->where('user_id',$user_id)
                        ->first();
      return $comment;
    }

    public static function myCart($user_id)
    {
      $my_cart = Cart::where('user_id',$user_id)->get();

      return $my_cart;
    }

    public static function getBrand()
    {
      $brands = Brand::where('status',1)->get();

      return $brands;
    }

    public static function getBrandMenu()
    {
      $brands = DB::table('product')
                  ->join('brand','brand.id','=','product.brand_id')
                  ->groupBy('brand.name','brand.image_1','brand.menu_urun_id')
                  ->where('product.status',1)
                  ->where('brand.status',1)
                  ->select('brand.name','brand.image_1','brand.menu_urun_id',DB::raw('COUNT(product.brand_id) as brand_count'))
                  ->get();

      //Brand::where('status',1)->get();

      return $brands;
    }

    public static function getBreadcrumbs($getUrl)
    {
      $url = explode('/',$getUrl);
      $urlCount = count($url);

      if ($urlCount > 4 && $url[3] != 'tum-urunler') {
        $category = Category::where('slug',$url[3])->first();
        $brand    = Brand::where('slug',$url[4])->first();
        if (!$brand) {
          $brand    = Brand::where('slug',$url[3])->first();
          if ($brand) {
            $data = '<li><a href="/'.$brand->slug.'">'.$brand->name.'</a></li>';
          }else {
            $data = '<li><a href="/'.$category->slug.'">'.$category->title.'</a></li>';
          }
        }elseif ($category) {
          $data = '<li><a href="/'.$category->slug.'">'.$category->title.'</a><span>&raquo;</span></li><li><strong>'.$brand->name.'</strong></li>';
        }else {
          $brand    = Brand::where('slug',$url[3])->first();
          $product = Product::where('id',$url[5])->first();
          $data = '<li><a href="/'.$brand->slug.'">'.$brand->name.'</a><span>&raquo;</span></li><li><strong>'.$product->product_number.'</strong></li>';
        }
        return $data;
      }else {
        $category = Category::where('slug',$url[3])->first();
        $brand    = Brand::where('slug',$url[3])->first();
        if ($category) {
          $data = '<li><strong>'.$category->title.'</strong></a></li>';
        }elseif ($brand) {
          $data = '<li><strong>'.$brand->name.'</strong></a></li>';
        }else {
          $data = '<li><strong>Tüm Ürünler</strong></li>';
        }
        return $data;
      }
    }

    public static function sepetControl($user_id)
    {
      $carts = Cart::where('user_id',$user_id)->get();
      return $carts;
    }
}
