<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = [
      'user_id','brand_id','modek_id','quick_content','content','product_number','color_id','color_code','stock','size','weight','gender_id','quality_id','in_price','out_price','ekartman','glass_id','comment_id','category_id','discount','new','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
    public function brand()
    {
      return $this->belongsTo('App\Brand');
    }
    public function modek()
    {
      return $this->belongsTo('App\Modek');
    }
    public function color()
    {
      return $this->belongsTo('App\Color');
    }
    public function gender()
    {
      return $this->belongsTo('App\Gender');
    }
    public function quality()
    {
      return $this->belongsTo('App\Quality');
    }
    public function glass()
    {
      return $this->belongsTo('App\Glass');
    }
    public function comment()
    {
      return $this->belongsTo('App\Comment');
    }
    public function category()
    {
      return $this->belongsTo('App\Category');
    }
}
