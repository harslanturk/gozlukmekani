<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quality extends Model
{
    protected $table = 'quality';
    protected $fillable = [
      'user_id','name','status'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
